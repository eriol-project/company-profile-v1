<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ClaimModel extends Model
{
    protected $table = 'claim';
    protected $primaryKey = 'id';

    public $timestamps = false;

    protected $fillable = [
       'no_pr',
       'pic_name',
       'distributor_id',
       'distributor_name',
       'no_claim',
       'description',
       'periode_from',
       'periode_to',
       'periode_text',
       'dpp',
       'ppn_id',
       'ppn',
       'pph_id',
       'pph',
       'tax_expired_date',
       'amount',
       'pending_type',
       'status',
       'status_name',
       'admin_acc_date_in',
       'status_week_1',
       'status_week_2',
       'status_week_3',
       'status_week_4',
       'status_name_week_1',
       'status_name_week_2',
       'status_name_week_3',
       'status_name_week_4',
       'date_week_1',
       'date_week_2',
       'date_week_3',
       'date_week_4',
       'progress'
    ];
    // protected $hidden = [
    //     'dist_regular_password',
    // ];

    // protected $casts = [
    //    'email_verified_at' => 'datetime',
    // ];
}
