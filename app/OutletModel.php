<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class OutletModel extends Model
{
    protected $table = 'master_outlet';
    protected $primaryKey = 'id';

    public $timestamps = false;

    protected $fillable = [
       'sap','name','address','bank_no','bank_name','date','distributor_id'
    ];
}
