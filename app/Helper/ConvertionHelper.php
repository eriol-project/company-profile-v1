<?php 
  
namespace App\Helper;

class ConvertionHelper
{
	public static function password($password = '')
	{
		$password = $password==''?'unzavitalis':$password;
		return md5(sha1('system'.$password.'star'));
	}

	public static function dateformat($date = '')
	{
        $change_month = array('Jan'=>'01','Feb'=>'02','Mar'=>'03','Apr'=>'04','May'=>'05','Jun'=>'06',
                              'Jul'=>'07','Aug'=>'08','Sep'=>'09','Oct'=>'10','Nov'=>'11','Dec'=>'12');
        if(ISSET($change_month[substr($date, 3,3)]))
        {
			return substr($date, 7,4).'-'.$change_month[substr($date, 3,3)].'-'.substr($date,0,2);
        }
        else
        {
        	return NULL;
        }
	}
}