<?php 
  
namespace App\Helper;

class ExcelHelper
{
	public static function range($start = NULL, $end = NULL, $row_start = NULL, $row_end = NULL)
	{
	  $merge = 'A1:A1'; $start = $start==0?'A':$start; $end = $end==0?'A':$end;
	  if($start && $end && $row_start && $row_end){
		  $start = \PhpOffice\PhpSpreadsheet\Cell\Coordinate::stringFromColumnIndex(floatval($start));
		  $end = \PhpOffice\PhpSpreadsheet\Cell\Coordinate::stringFromColumnIndex(floatval($end));
		  $merge = "$start{$row_start}:$end{$row_end}";
	  }
	  return $merge;
	}
	public static function cell($kolom = NULL, $baris = NULL)
	{
	  $merge = 'A1:A1'; $kolom = $kolom==0?'A':$kolom;
	  if($kolom && $baris){ 
		$kolom =  \PhpOffice\PhpSpreadsheet\Cell\Coordinate::stringFromColumnIndex(floatval($kolom)); 
		$merge = "$kolom{$baris}"; 
	  }
	  return $merge;
	}
	public static function style($type = NULL, $fill = NULL, $size = NULL)
	{
		$styleArray = array();
		if($fill!=NULL)
		{
			$fill_collor = array('gray'=>'cccccc','red'=>'ff3333','orange'=>'ffe680','yellow'=>'ffff66','green'=>'5dd55d','blue'=>'1AA3FF');
			$styleArray['fill'] = [ 'fillType'   => \PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID,
									'color' => [ 'argb' => $fill_collor[$fill], ],];
		}
		if (strpos($type, '1') !== false) 
		{
			$styleArray['borders']['allBorders']['borderStyle'] = \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN;
		}
		if (strpos($type, 'L') !== false) 
		{
			$styleArray['alignment']['horizontal'] = \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_LEFT;
		}
		if (strpos($type, 'R') !== false) 
		{
			$styleArray['alignment']['horizontal'] = \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_RIGHT;
		}
		if (strpos($type, 'C') !== false) 
		{
			$styleArray['alignment']['horizontal'] = \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER;
		}
		if (strpos($type, 'M') !== false) 
		{
			$styleArray['alignment']['vertical'] = \PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER;
		}
		if (strpos($type, 'T') !== false) 
		{
			$styleArray['alignment']['vertical'] = \PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_TOP;
		}
		if (strpos($type, 'B') !== false) 
		{
			$styleArray['font']['bold'] = true;
		}
		if($size!=NULL)
		{
			$styleArray['font']['size'] = $size;
		}
		return $styleArray;
	}
}