<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use DB;
use File;

class ProcessCronJob extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'processupload:cron';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    

    public function handle()
    {
        $queue_file = DB::table('tbl_upload')->where('file_status','PENDING')->first();
        if($queue_file)
        {
            $inputFileName = public_path().'/files/'.$queue_file->file_name_alias;
            $inputFileType = \PhpOffice\PhpSpreadsheet\IOFactory::identify($inputFileName);
            $sheet = \PhpOffice\PhpSpreadsheet\IOFactory::createReader($inputFileType)->load($inputFileName)->getActiveSheet();
            $totalrow = $sheet->getHighestRow();
            $status_header = 0;
            $status_check = 0;
            $status_check_data = 1;
            $confirm_header = 0;
            $status_cancel = 0;
            $total_invoice = array();
            $text_log = '';
            DB::table('tbl_upload')->where('id', $queue_file->id)->update(['file_status' => 'PROCESS','file_process_status' => '0']);
            for ($i=0;$i<=$totalrow;++$i)
            {
                // CREATE DATA CELL AS VARIABLE
                $getCellByColumnAndRow_1 = trim($sheet->getCellByColumnAndRow(1,$i)->getCalculatedValue());
                $getCellByColumnAndRow_2 = trim($sheet->getCellByColumnAndRow(2,$i)->getCalculatedValue());
                $getCellByColumnAndRow_3 = trim($sheet->getCellByColumnAndRow(3,$i)->getCalculatedValue());
                $getCellByColumnAndRow_4 = trim($sheet->getCellByColumnAndRow(4,$i)->getCalculatedValue());
                $getCellByColumnAndRow_5 = trim($sheet->getCellByColumnAndRow(5,$i)->getCalculatedValue());
                $getCellByColumnAndRow_6 = trim($sheet->getCellByColumnAndRow(6,$i)->getCalculatedValue());
                $getCellByColumnAndRow_7 = trim($sheet->getCellByColumnAndRow(7,$i)->getCalculatedValue());
                $getCellByColumnAndRow_8 = trim($sheet->getCellByColumnAndRow(8,$i)->getCalculatedValue());
                $getCellByColumnAndRow_9 = trim($sheet->getCellByColumnAndRow(9,$i)->getCalculatedValue());
                $getCellByColumnAndRow_10 = trim($sheet->getCellByColumnAndRow(10,$i)->getCalculatedValue());
                $getCellByColumnAndRow_11 = trim($sheet->getCellByColumnAndRow(11,$i)->getCalculatedValue());
                $getCellByColumnAndRow_12 = trim($sheet->getCellByColumnAndRow(12,$i)->getCalculatedValue());
                $getCellByColumnAndRow_13 = trim($sheet->getCellByColumnAndRow(13,$i)->getCalculatedValue());
                $getCellByColumnAndRow_14 = trim($sheet->getCellByColumnAndRow(14,$i)->getCalculatedValue());
                $getCellByColumnAndRow_15 = trim($sheet->getCellByColumnAndRow(15,$i)->getCalculatedValue());
                $getCellByColumnAndRow_16 = trim($sheet->getCellByColumnAndRow(16,$i)->getCalculatedValue());
                $getCellByColumnAndRow_17 = trim($sheet->getCellByColumnAndRow(17,$i)->getCalculatedValue());
                $getCellByColumnAndRow_18 = trim($sheet->getCellByColumnAndRow(18,$i)->getCalculatedValue());
                $getCellByColumnAndRow_19 = trim($sheet->getCellByColumnAndRow(19,$i)->getCalculatedValue());
                $getCellByColumnAndRow_20 = trim($sheet->getCellByColumnAndRow(20,$i)->getCalculatedValue());
                $getCellByColumnAndRow_21 = trim($sheet->getCellByColumnAndRow(21,$i)->getCalculatedValue());
                $getCellByColumnAndRow_22 = trim($sheet->getCellByColumnAndRow(22,$i)->getCalculatedValue());
                $getCellByColumnAndRow_23 = trim($sheet->getCellByColumnAndRow(23,$i)->getCalculatedValue());
                $getCellByColumnAndRow_24 = trim($sheet->getCellByColumnAndRow(24,$i)->getCalculatedValue());
                $getCellByColumnAndRow_25 = trim($sheet->getCellByColumnAndRow(25,$i)->getCalculatedValue());
                $getCellByColumnAndRow_26 = trim($sheet->getCellByColumnAndRow(26,$i)->getCalculatedValue());
                $getCellByColumnAndRow_27 = trim($sheet->getCellByColumnAndRow(27,$i)->getCalculatedValue());
                $getCellByColumnAndRow_28 = trim($sheet->getCellByColumnAndRow(28,$i)->getCalculatedValue());
                $getCellByColumnAndRow_29 = trim($sheet->getCellByColumnAndRow(29,$i)->getCalculatedValue());
                $getCellByColumnAndRow_30 = trim($sheet->getCellByColumnAndRow(30,$i)->getCalculatedValue());

                // CHECK HEADER
                if($i <= 9 && $status_header == 0)
                {
                   $status_header = 1;
                   if(strtoupper($getCellByColumnAndRow_1) != 'SS1'){ $status_header = 0; $text_log .= '[Column 1 Row '.$i.']'; }
                   if(strtoupper($getCellByColumnAndRow_2) != 'SS'){ $status_header = 0; $text_log .= '[Column 2]'; }
                   if(strtoupper($getCellByColumnAndRow_3) != 'ORDER NO'){ $status_header = 0; $text_log .= '[Column 3]'; }
                   if(strtoupper($getCellByColumnAndRow_4) != 'ORDER DATE'){ $status_header = 0; $text_log .= '[Column 4]'; }
                   if(strtoupper($getCellByColumnAndRow_5) != 'ORDER TYPE'){ $status_header = 0; $text_log .= '[Column 5]'; }
                   if(strtoupper($getCellByColumnAndRow_6) != 'PO NO'){ $status_header = 0; $text_log .= '[Column 6]'; }
                   if(strtoupper($getCellByColumnAndRow_7) != 'CUST NO'){ $status_header = 0; $text_log .= '[Column 7]'; }
                   if(strtoupper($getCellByColumnAndRow_8) != 'CUST NAME'){ $status_header = 0; $text_log .= '[Column 8]'; }
                   if(strtoupper($getCellByColumnAndRow_9) != 'ACCOUNT'){ $status_header = 0; $text_log .= '[Column 9]'; }
                   if(strtoupper($getCellByColumnAndRow_10) != 'ACT TYPE'){ $status_header = 0; $text_log .= '[Column 10]'; }
                   if(strtoupper($getCellByColumnAndRow_11) != 'ITEM NO'){ $status_header = 0; $text_log .= '[Column 11]'; }
                   if(strtoupper($getCellByColumnAndRow_12) != 'MATNO'){ $status_header = 0; $text_log .= '[Column 12]'; }
                   if(strtoupper($getCellByColumnAndRow_13) != 'MAT DESC'){ $status_header = 0; $text_log .= '[Column 13]'; }
                   if(strtoupper($getCellByColumnAndRow_14) != 'BRAND'){ $status_header = 0; $text_log .= '[Column 14]'; }
                   if(strtoupper($getCellByColumnAndRow_15) != 'CATEGORY'){ $status_header = 0; $text_log .= '[Column 15]'; }
                   if(strtoupper($getCellByColumnAndRow_16) != 'TYPE'){ $status_header = 0; $text_log .= '[Column 16]'; }
                   if(strtoupper($getCellByColumnAndRow_17) != 'REJECTION CODE'){ $status_header = 0; $text_log .= '[Column 17]'; }
                   if(strtoupper($getCellByColumnAndRow_18) != 'ORDER QTY'){ $status_header = 0; $text_log .= '[Column 18]'; }
                   if(strtoupper($getCellByColumnAndRow_19) != 'ORDER AMOUNT'){ $status_header = 0; $text_log .= '[Column 19]'; }
                   if(strtoupper($getCellByColumnAndRow_20) != 'PLANT'){ $status_header = 0; $text_log .= '[Column 20]'; }
                   if(strtoupper($getCellByColumnAndRow_21) != 'PICK LIST'){ $status_header = 0; $text_log .= '[Column 21]'; }
                   if(strtoupper($getCellByColumnAndRow_22) != 'MAT DOCNO'){ $status_header = 0; $text_log .= '[Column 22]'; }
                   if(strtoupper($getCellByColumnAndRow_23) != 'PGI DATE'){ $status_header = 0; $text_log .= '[Column 23]'; }
                   if(strtoupper($getCellByColumnAndRow_24) != 'WEEK 2'){ $status_header = 0; $text_log .= '[Column 24]'; }
                   if(strtoupper($getCellByColumnAndRow_25) != 'INVOICE NO'){ $status_header = 0; $text_log .= '[Column 25]'; }
                   if(strtoupper($getCellByColumnAndRow_26) != 'INVOICE DATE'){ $status_header = 0; $text_log .= '[Column 26]'; }
                   if(strtoupper($getCellByColumnAndRow_27) != 'INVOICE QTY'){ $status_header = 0; $text_log .= '[Column 27]'; }
                   if(strtoupper($getCellByColumnAndRow_28) != 'INVOICE AMOUNT'){ $status_header = 0; $text_log .= '[Column 28]'; }
                   if(strtoupper($getCellByColumnAndRow_29) != 'UNFULFILLED QTY'){ $status_header = 0; $text_log .= '[Column 29]'; }
                   if(strtoupper($getCellByColumnAndRow_30) != 'UNFULFILLED AMT'){ $status_header = 0; $text_log .= '[Column 30]'; }
                   $status_header==1?($text_log=''):($text_log.='<br>');
                   $confirm_header = $i;
                }

                if(strlen($getCellByColumnAndRow_3)>0 && 
                   strlen($getCellByColumnAndRow_4)>0 && 
                   strlen($getCellByColumnAndRow_5)>0 && 
                   strlen($getCellByColumnAndRow_6)>0 && 
                   $status_header==1 && 
                   $confirm_header!=$i)
                {
                    if(strlen($getCellByColumnAndRow_4)==10)
                    {
                        $getCellByColumnAndRow_4 = substr($getCellByColumnAndRow_4,-4).'-'.substr($getCellByColumnAndRow_4,3,2).'-'.substr($getCellByColumnAndRow_4,0,2);
                    }
                    if(strlen($getCellByColumnAndRow_23)==10)
                    {
                        $getCellByColumnAndRow_23 = substr($getCellByColumnAndRow_23,-4).'-'.substr($getCellByColumnAndRow_23,3,2).'-'.substr($getCellByColumnAndRow_23,0,2);
                    }
                    if(strlen($getCellByColumnAndRow_26)==10)
                    {
                        $getCellByColumnAndRow_26 = substr($getCellByColumnAndRow_26,-4).'-'.substr($getCellByColumnAndRow_26,3,2).'-'.substr($getCellByColumnAndRow_26,0,2);
                    }
                    // CHECK DATA
                    $status_check_data = 1;
                    $costumer = DB::table('tbl_customer')->where('code',$getCellByColumnAndRow_7)->first();
                    if(!$costumer)
                    { 
                        if(strlen($getCellByColumnAndRow_9)>0 && strlen($getCellByColumnAndRow_10)>0)
                        {
                            DB::table('tbl_customer')->insert(array('code'          =>$getCellByColumnAndRow_7,
                                                                    'name'          =>$getCellByColumnAndRow_8,
                                                                    'account'       =>$getCellByColumnAndRow_9,
                                                                    'act_type'      =>$getCellByColumnAndRow_10,
                                                                    'desc'          =>'Insert at:'.date("Y-m-d H:i:s")
                                                            ));
                            $costumer = DB::table('tbl_customer')->where('code',$getCellByColumnAndRow_7)->first();
                        }
                        else
                        {
                            if(strlen($getCellByColumnAndRow_9))
                            {
                                $text_log .= '[Row '.$i.'] Unknown Account from data Costumer : '.$getCellByColumnAndRow_7.'<br>';
                            }
                            if(strlen($getCellByColumnAndRow_10)>0)
                            {
                                $text_log .= '[Row '.$i.'] Unknown Act Type from data Costumer : '.$getCellByColumnAndRow_7.'<br>';
                            }
                        }
                    }
                    if(!$costumer)
                    { 
                        $text_log .= '[Row '.$i.'] Unknown data Costumer : '.$getCellByColumnAndRow_7.'<br>';
                    }
                    $material = DB::table('tbl_material')->where('code',$getCellByColumnAndRow_12)->first(); 
                    if(!$material)
                    { 
                        $brand = DB::table('tbl_brand')->where('name',$getCellByColumnAndRow_14)->first();
                        $category = DB::table('tbl_category')->where('name',$getCellByColumnAndRow_15)->first();
                        if($brand && $category)
                        {
                            DB::table('tbl_material')->insert(array('code'          =>$getCellByColumnAndRow_12,
                                                                'name'          =>$getCellByColumnAndRow_13,
                                                                'brand_id'      =>$brand->id,
                                                                'category_id'   =>$category->id,
                                                                'desc'          =>'Insert at:'.date("Y-m-d H:i:s")
                                                            ));
                            $material = DB::table('tbl_material')->where('code',$getCellByColumnAndRow_12)->first(); 
                        }
                        else 
                        {
                            if(!$brand)
                            {
                                $text_log .= '[Row '.$i.'] Unknown Brand from data Material: '.$getCellByColumnAndRow_12.'<br>';
                            }
                            if(!$category)
                            {
                                $text_log .= '[Row '.$i.'] Unknown Category from data Material: '.$getCellByColumnAndRow_12.'<br>';
                            }
                        }
                    }
                    if($costumer && $material)
                    {
                        // LIST DATA INSERT TO ARRAY
                        if(strlen($getCellByColumnAndRow_3)>0 && strlen($getCellByColumnAndRow_20)>0)
                        {
                            if(!ISSET($total_invoice[$getCellByColumnAndRow_3])){ $total_invoice[$getCellByColumnAndRow_3] = 0; }
                            $total_invoice[$getCellByColumnAndRow_3] += 1;
                        }
                        DB::table('tbl_data')->insert(array('transaction_id'    =>$queue_file->id,
                                                            'ss'                =>$getCellByColumnAndRow_2,
                                                            'order_no'          =>$getCellByColumnAndRow_3,
                                                            'order_date'        =>$getCellByColumnAndRow_4,
                                                            'order_type'        =>$getCellByColumnAndRow_5,
                                                            'po_no'             =>$getCellByColumnAndRow_6,
                                                            'customer_id'       =>$getCellByColumnAndRow_7,
                                                            'act_type'          =>$costumer->act_type,
                                                            'item_no'           =>$getCellByColumnAndRow_11,
                                                            'material_id'       =>$material->id,
                                                            'material_no'       =>$getCellByColumnAndRow_12,
                                                            'sku_status'        =>NULL,
                                                            'type'              =>$getCellByColumnAndRow_16,
                                                            'rejection_code'    =>$getCellByColumnAndRow_17,
                                                            'order_qty'         =>$getCellByColumnAndRow_18,
                                                            'order_amount'      =>$getCellByColumnAndRow_19,
                                                            'plant'             =>$getCellByColumnAndRow_20,
                                                            'pick_list'         =>$getCellByColumnAndRow_21,
                                                            'mat_no'            =>$getCellByColumnAndRow_22,
                                                            'pgi_date'          =>$getCellByColumnAndRow_23,
                                                            'week2'             =>$getCellByColumnAndRow_24,
                                                            'invoice_no'        =>$getCellByColumnAndRow_25,
                                                            'invoice_date'      =>$getCellByColumnAndRow_26,
                                                            'invoice_qty'       =>(floatval($getCellByColumnAndRow_27)+0),
                                                            'invoice_amount'    =>(floatval($getCellByColumnAndRow_28)+0),
                                                            'unfulfilled_qty'   =>(floatval($getCellByColumnAndRow_29)+0),
                                                            'unfulfilled_amount'=>(floatval($getCellByColumnAndRow_30)+0),
                                                            'invoice_total'     =>0,
                                                            'to_be_invoice'     =>0,
                                                            'tbi_qty'           =>0,
                                                            'uff'               =>0,
                                                            'uff_qty'           =>0,
                                                            'upload_date'       =>date("Y-m-d H:i:s"),
                                                            'data_date'         =>$queue_file->file_date
                                                        ));
                        $percent = $i*100/$totalrow;
                        DB::table('tbl_upload')->where('id', $queue_file->id)->update(['file_process_status' => number_format($percent,0)]);
                    }
                }
                elseif($status_header==1 && $confirm_header!=$i)
                {
                    $text_log .= '[Row '.$i.'] Data is null<br>';
                }

                // CHECKING STATUS UPLOAD
                if($status_check==50)
                {
                    $status_check = 0;
                    $check = DB::table('tbl_upload')->where(['id'=>$queue_file->id,'file_status'=>'PROCESS'])->first();
                    if(!$check)
                    {
                        $i = $totalrow;
                        DB::table('tbl_data')->where('transaction_id', $queue_file->id)->delete();
                    }
                }
                ++$status_check;
            }
            
            if(count($total_invoice)>0)
            {
                foreach($total_invoice as $key => $value) 
                {
                    DB::table('tbl_data')->where('order_no', $key)->update(['invoice_total' => $value]);
                }
                // DB::statement('UPDATE tbl_data SET invoice_total = 0 WHERE (invoice_no IS NULL OR invoice_no = "")');
                DB::statement('UPDATE tbl_data SET to_be_invoice = CASE WHEN invoice_total>0 THEN 0 ELSE unfulfilled_amount END WHERE transaction_id = '.$queue_file->id);
                DB::statement('UPDATE tbl_data SET tbi_qty = CASE WHEN invoice_total>0 THEN 0 ELSE unfulfilled_qty END WHERE transaction_id = '.$queue_file->id);
                DB::statement('UPDATE tbl_data SET uff = unfulfilled_amount - to_be_invoice WHERE transaction_id = '.$queue_file->id);
                DB::statement('UPDATE tbl_data SET uff_qty = unfulfilled_qty - tbi_qty WHERE transaction_id = '.$queue_file->id);
            }

            $check = DB::table('tbl_upload')->where(['id'=>$queue_file->id,'file_status'=>'PROCESS'])->first();
            if(!$check)
            {
                DB::table('tbl_data')->where('transaction_id', $queue_file->id)->delete();
            }
            if($queue_file->file_type=='Overwrite Data')
            {
                DB::table('tbl_data')->where([['data_date', '=', $queue_file->file_date],['transaction_id', '<>', $queue_file->id]])->delete();
            }
            if($status_header==0)
            {
                DB::table('tbl_upload')->where('id', $queue_file->id)->update(['file_status' => 'REJECTED','text_log' => 'Document header not match '.$text_log]);
            }
            else
            {
                DB::table('tbl_upload')->where('id', $queue_file->id)->update(['file_status' => 'SUCCESS','text_log' => $text_log]);
            }
            File::delete($inputFileName);
        }
    }
}
