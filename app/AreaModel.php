<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AreaModel extends Model
{
    protected $table = 'master_area';
    protected $primaryKey = 'id';

    public $timestamps = false;

    protected $fillable = [
       'name','date'
    ];
}
