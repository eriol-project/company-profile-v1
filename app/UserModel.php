<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserModel extends Model
{
    protected $table = 'master_user';
    protected $primaryKey = 'id';

    public $timestamps = false;

    protected $fillable = [
       'name','email','password','type','status','area_id','distributor_id','position_id'
    ];
}
