<?php

namespace App\Http\Middleware;

use Route;
use Closure;
use Illuminate\Support\Facades\DB;
use App\Model\MenuModel;

class AuthMiddleware
{
    public function handle($request, Closure $next)
    {
        if(!session('is_login'))
        {
            return redirect()->to('/login');
        }
        return $next($request);
    }
}
