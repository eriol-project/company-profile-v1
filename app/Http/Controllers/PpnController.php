<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Datatables;
use DB;

class ppnController extends Controller
{
    protected function index(Request $request)
    {
      if(session('type')=='SUPER USER' || session('type')=='ACCOUNTING')
      {
        $data['title'] = 'PPN | CPS';
        $data['body'] = 'ppn';
        return view('layout',$data);
      }
      else
      {
        abort(404);
      }
    }

    public function store(Request $request)
    {
      if(DB::table('ppn')->where('name', $request->name)->first()) 
      {
        return redirect('ppn')->with('info', 'Sorry PPN name '.$request->name.' already exists.');
      }
      else
      {
        DB::table('ppn')->insert( array(   'name'   => $request->name, 'percent'   => $request->percent
                                          ) 
                                  );
        return redirect('ppn')->with('success', 'PPN '.$request->name.' has been added successfully');
      } 
    }
    
    public function show($id='')
    {
      if($id=='json')
      {
        $QUERY = DB::table('ppn');
        return Datatables::of($QUERY->get())->make();
      }
      else
      {
        $detail = DB::table('ppn')->where('id', $id)->get()->first();
        if(!empty($detail))
        {
            echo json_encode($detail);
        }
        else
        {
            abort(404);
        }
      }
    }

    public function edit($id)
    {
    }

    public function update(Request $request, $id)
    {
      DB::beginTransaction();
      if(DB::connection('mysql'))
      {
        if($request->deleteid)
        {
          // DB::table('user')->where('id',$id)->delete();
          // DB::commit();
          // return redirect('user')->with('warning', 'User '.$request->name.' has been deleted.');
        }
        else 
        {
          DB::table('ppn')->where('id',$id)->update( array( 'name'   => $request->name, 'percent'   => $request->percent ) );
          DB::commit();
          return redirect('ppn')->with('success', 'PPN '.$request->name.' has been changed successfully.');
        }
      }
      else
      {
        DB::rollback();
        return redirect('ppn')->with('info', 'PPN '.$request->name.' failed to change.');
      }
      return redirect('ppn')->with('info', 'PPN '.$request->name.' failed to change.');
    }

    public function create()
    {
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

    public function destroy($id)
    {
        //
    }

}
