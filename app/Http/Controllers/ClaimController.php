<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Datatables;
use DB;
use Mail;
use App\ClaimModel;
use App\Helper\ConvertionHelper;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;
use App\Helper\ExcelHelper;
use Fpdf;
class ClaimController extends Controller
{
    public function index()
    {
        for ($i=2018; $i <= date('Y'); $i++) {  $year[$i] = $i; }
        $data['title'] = 'Claim | CMS';
        $data['body'] = 'claim';
        $data['area'] =  DB::table('master_area')->get();
        $data['distributor'] =  DB::table('master_distributor')->get();
        $data['outlet'] =  DB::table('master_outlet')->get();
        $data['operational'] =  DB::table('master_user')->where(['status_operational'=>0])->get();
        $data['type'] =  DB::table('master_type')->get();
        $data['status'] =  DB::table('master_status')->get();
        $data['ppn'] =  DB::table('master_ppn')->get();
        $data['pph'] =  DB::table('master_pph')->get();
        $data['pending_type'] =  DB::table('pending_type')->get();
        return view('template',$data);
    }
    
    public function show($id='')
    {
        $detail = DB::table('claim AS A')->join("claim_detail AS B", "B.claim_id", "=", "A.id")
                                         ->select(DB::raw('A.*,GROUP_CONCAT(B.id SEPARATOR "**##") AS detail_id,
                                                               GROUP_CONCAT(no_claim SEPARATOR "**##") AS no_claim,
                                                               GROUP_CONCAT(description SEPARATOR "**##") AS description,
                                                               GROUP_CONCAT(dpp SEPARATOR "**##") AS dpp,
                                                               GROUP_CONCAT(periode_text SEPARATOR "**##") AS periode_text'))
                                         ->where('A.id', $id)
                                         ->groupBy('A.id')
                                         ->first();
        if(!empty($detail))
        {
            echo json_encode($detail);
        }
        else
        {
            abort(404);
        }
    }
    
    public function table(Request $request)
    {
        $data = DB::table('claim AS A')
                ->join("claim_detail AS B", "B.claim_id", "=", "A.id")
                ->select('A.id',
                         'no_pr',
                         'distributor_name',
                         DB::raw('GROUP_CONCAT(no_claim SEPARATOR "<br>") AS no_claim'),
                         'status_name',
                         'remarks',
                         'description',
                         'periode_text',
                         'total_amount',
                         'status_name');
                                  
        if($request->distributor_id!='ALL')
        {
            $data = $data->where('distributor_id',$request->distributor_id);
        }                        
        if($request->area_id!='ALL')
        {
            $list_distributor_by_area = array();    
            $distributor = DB::table('master_distributor')->where('area_id',$request->area_id)->get();
            foreach ($distributor as $value) { array_push($list_distributor_by_area,$value->id); }
            $data = $data->whereIn('distributor_id',$list_distributor_by_area);
        }
        elseif(session('area_id')!=0)
        {
            $list_distributor_by_area = array();    
            $distributor = DB::table('distributor')->where('area_id',session('area_id'))->get();
            foreach ($distributor as $value) { array_push($list_distributor_by_area,$value->id); }
            $data = $data->whereIn('distributor_id',$list_distributor_by_area);
        }
        if($request->status!='ALL')
        { 
            $data = $data->where('status_id', $request->status);
        }
        $data = $data->groupBy('A.id');
        $data = $data->orderBy('update_at','desc');


        return Datatables::of($data->get())->make(true);
    }
    
    public function table_detail(Request $request)
    {
        $data = DB::table('claim_log')->where('claim_id',$request->id)->orderBy('update_at','desc');
        return Datatables::of($data->get())->make(true);
    }

    public function store(Request $request)
    {
        $change_month = array('Jan'=>'01','Feb'=>'02','Mar'=>'03','Apr'=>'04','May'=>'05','Jun'=>'06',
                              'Jul'=>'07','Aug'=>'08','Sep'=>'09','Oct'=>'10','Nov'=>'11','Dec'=>'12');
        $today = date('Y-m-d');
        $admin_date_in = substr($request->admin_date_in, 7,4).'-'.$change_month[substr($request->admin_date_in, 3,3)].'-'.substr($request->admin_date_in,0,2);
        $periode_to = substr($request->input('period1'), 21,4).'-'.$change_month[substr($request->input('period1'), 17,3)].'-'.substr($request->input('period1'),14,2);
        $distributor = DB::table('distributor')->where('id',$request->distributor)->first();
        $ppn = DB::table('ppn')->where('id',$request->ppn)->first();
        $pph = DB::table('pph')->where('id',$request->pph)->first();
        $status = DB::table('status')->where('id',$request->status)->first();
        $dpp = floatval(str_replace(',', '', $request->dpp));
        $pending_type = '';
        $new_line = '
';
        if($request->pending_type)
        {
            foreach ($request->pending_type as $key => $value) 
            {
                $pending_type .= $pending_type==''?$value:$new_line.$value;
            }
        }
        $id = DB::table('claim')->insertGetId(array('no_pr'               => $request->no_pr,
                                                    'pic_name'            => strtoupper($request->pic),
                                                    'distributor_id'      => $request->distributor,
                                                    'distributor_name'    => $distributor->name,
                                                    'total_dpp'           => floatval(str_replace(",","",$dpp)),
                                                    'ppn_id'              => $request->ppn,
                                                    'total_ppn'           => ($dpp*$ppn->percent/100),
                                                    'pph_id'              => $request->pph,
                                                    'total_pph'           => ($dpp*$pph->percent/100),
                                                    'tax_expired_date'    => ($request->ppn==1?NULL:DB::raw('"'.$periode_to.'" + INTERVAL 3 MONTH')),
                                                    'total_amount'        => floatval(str_replace(",","",$request->amount)),
                                                    'pending_type'        => $pending_type,
                                                    'status'              => $request->status,
                                                    'status_name'         => $status->name,
                                                    'admin_acc_date_in'   => $admin_date_in,
                                                    'remarks'             => $request->remarks,
                                                    'confirm_date'        => ($pending_type==''?NULL:date('Y-m-d')),
                                                    'created_at'          => date('Y-m-d H:i:s'),
                                                    'created_by'          => session('name'),
                                                    'update_at'           => date('Y-m-d H:i:s'),
                                                    'update_by'           => session('name')));

        DB::table('claim_log')->insert(array(   'no_pr'               => $request->no_pr,
                                                'pic_name'            => strtoupper($request->pic),
                                                'distributor_id'      => $request->distributor,
                                                'distributor_name'    => $distributor->name,
                                                'total_dpp'           => floatval(str_replace(",","",$dpp)),
                                                'ppn_id'              => $request->ppn,
                                                'total_ppn'           => ($dpp*$ppn->percent/100),
                                                'pph_id'              => $request->pph,
                                                'total_pph'           => ($dpp*$pph->percent/100),
                                                'tax_expired_date'    => ($request->ppn==1?NULL:DB::raw('"'.$periode_to.'" + INTERVAL 3 MONTH')),
                                                'total_amount'        => floatval(str_replace(",","",$request->amount)),
                                                'pending_type'        => $pending_type,
                                                'status'              => $request->status,
                                                'status_name'         => $status->name,
                                                'status_date'         => $today,
                                                'week'                => DB::raw('IF('.intval(substr($today,-2)).'<8,1,IF('.intval(substr($today,-2)).'<15,2,IF('.intval(substr($today,-2)).'<22,3,4)))'),
                                                'admin_acc_date_in'   => $admin_date_in,
                                                'remarks'             => $request->remarks,
                                                'confirm_date'        => ($pending_type==''?NULL:date('Y-m-d')),
                                                'update_at'           => date('Y-m-d H:i:s'),
                                                'update_by'           => session('name'),
                                                'claim_id'            => $id ) );
        for ($i=1; $i <= 100; $i++)
        {
            if($request->input('no_claim'.$i))
            {
                $period = $request->input('period'.$i);
                $periode_from = substr($period, 7,4).'-'.$change_month[substr($period, 3,3)].'-'.substr($period,0,2);
                $periode_to = substr($period, 21,4).'-'.$change_month[substr($period, 17,3)].'-'.substr($period,14,2);
                $dpp = floatval(str_replace(',', '', $request->input('dpp_detail'.$i)));
                DB::table('claim_detail')->insert(array('no_claim'            => $request->input('no_claim'.$i),
                                                        'description'         => $request->input('description'.$i),
                                                        'periode_from'        => $periode_from,
                                                        'periode_to'          => $periode_to,
                                                        'periode_text'        => $period,
                                                        'dpp'              => $dpp,
                                                        'claim_id'            => $id ) );
            }
        }
        return redirect('claim')->with('success', 'Payment Request : '.$request->no_pr.' has been added successfully');
    }

    public function detail(Request $request)
    {
        $data = DB::table('claim')->where('id',$request->id)->first();
        $claim_detail_text = ''; $no = 1;
        $claim_detail = DB::table('claim_detail')->where('claim_id',$data->id)->get();
        foreach ($claim_detail as $val)
        {
            $claim_detail_text .=  '<tr>
                                        <td style="vertical-align:top;" align="center"><small><span class="display-block text-muted">'.$no++.'</span></small></td>
                                        <td style="vertical-align:top;"><small><span class="display-block text-muted">'.$val->no_claim.'</span></small></td>
                                        <td style="vertical-align:top;"><span class="display-block text-muted">'.$val->description.'</span></td>
                                        <td style="vertical-align:top;" align="right">
                                            <span class="display-block text-muted">'.number_format($val->dpp,0).'</span>
                                        </td>
                                    </tr>';
        }
        $admin_acc_date_progress = '<i class="icon-cross2 text-danger-400"></i>';
        if($data->admin_acc_date_in)
        {
            if(!$data->admin_acc_date_out)
            {
                $diff = strtotime(date('Y-m-d')) - (strtotime($data->admin_acc_date_in));
                $admin_acc_date_progress = floor($diff/3600/24).' <small class="display-block text-size-small no-margin">days</small>';
            }
            else
            {
                $admin_acc_date_progress = '<i class="icon-checkmark3 text-success"></i>';
            }
        }
        $spv_acc_date_progress = '<i class="icon-cross2 text-danger-400"></i>';
        if($data->spv_acc_date_in)
        {
            if(!$data->spv_acc_date_out)
            {
                $diff = strtotime(date('Y-m-d')) - (strtotime($data->spv_acc_date_in));
                $spv_acc_date_progress = floor($diff/3600/24).' <small class="display-block text-size-small no-margin">days</small>';
            }
            else
            {
                $spv_acc_date_progress = '<i class="icon-checkmark3 text-success"></i>';
            }
        }
        $dept_sales_date_progress = '<i class="icon-cross2 text-danger-400"></i>';
        if($data->dept_sales_date_in)
        {
            if(!$data->dept_sales_date_out)
            {
                $diff = strtotime(date('Y-m-d')) - (strtotime($data->dept_sales_date_in));
                $dept_sales_date_progress = floor($diff/3600/24).' <small class="display-block text-size-small no-margin">days</small>';
            }
            else
            {
                $dept_sales_date_progress = '<i class="icon-checkmark3 text-success"></i>';
            }
        }
        if($data->ap_date_in)
        {
            $ap_date_progress = '<i class="icon-checkmark3 text-success"></i>';
        }
        else
        {
            $ap_date_progress = '<i class="icon-cross2 text-danger-400"></i>';
        }
        if($data->finance_date_in)
        {
            $finance_date_progress = '<i class="icon-checkmark3 text-success"></i>';
        }
        else
        {
            $finance_date_progress = '<i class="icon-cross2 text-danger-400"></i>';
        }
        if($data->complete_date)
        {
            $complete_date_progress = '<i class="icon-checkmark3 text-success"></i>';
        }
        else
        {
            $complete_date_progress = '<i class="icon-cross2 text-danger-400"></i>';
        }
        if($data->transfer_date)
        {
            $transfer_date_progress = '<i class="icon-checkmark3 text-success"></i>';
        }
        else
        {
            $transfer_date_progress = '<i class="icon-cross2 text-danger-400"></i>';
        }
        $pending_type_array = preg_split('/\r\n|\r|\n/', $data->pending_type);
        $pending_type = '';
        foreach ($pending_type_array as $key => $value) {
           $pending_type .= '<span class="label label-default">'.$value.'</span> &nbsp;';
        }
        $html = '
        <div class="panel panel-flat no-margin">

            <div class="panel-heading">
                <div class="media-left">
                    <span class="text-muted">Payment Request No.</span>
                    <h6 class="text-semibold no-margin">'.$data->no_pr.'</h6>
                </div>
                <div class="heading-elements">
                    <button type="button" class="close position-left" data-dismiss="modal">&times;</button>
                </div>
            </div>

            <div class="tabbable tab-content-bordered">
                <ul class="nav nav-tabs nav-tabs-top nav-justified">
                    <li class="active"><a href="#tab_status" data-toggle="tab">Status Progress</a></li>
                    <li><a href="#tab_history" data-toggle="tab">Status History</a></li>
                </ul>
                <div class="tab-content">
                    <div class="tab-pane has-padding active no-padding" id="tab_status">            

                        <div class="table-responsive">
                            <table class="table table-xlg text-nowrap">
                                <tbody>
                                    <tr>
                                        <td class="col-md-3">
                                            <div class="media-left">
                                                <span class="text-muted">Distributor</span>
                                                <h6 class="text-semibold no-margin">'.$data->distributor_name.'</h6>
                                            </div>
                                        </td>
            
                                        <td class="col-md-3">
                                            <div class="media-left">
                                                <span class="text-muted">PIC</span>
                                                <h6 class="text-semibold no-margin">'.$data->pic_name.'</h6>
                                            </div>
                                        </td>
            
                                        <td class="text-right col-md-6">
                                            <a href="#" class="btn bg-slate">'.$data->status_name.'</a>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>	
                        </div>
                        <div class="table-responsive">
                            <table class="table">
                                <tbody>
                                    <tr>
                                        <td style="width: 50px" class="text-center text-nowrap">
                                            <h6 class="no-margin">'.$admin_acc_date_progress.'</h6>
                                        </td>
                                        <td style="width: 100px; padding-left: 0px;" class="text-nowrap">
                                            <div class="media-body">
                                                <a href="#" class="display-inline-block text-default text-semibold letter-icon-title">Admin Accounting</a>
                                                <div class="text-muted text-size-small"></span>'.($data->admin_acc_date_in?$data->admin_acc_date_in.' &raquo; '.
                                                                                                ($data->admin_acc_date_out?$data->admin_acc_date_out:'<i>( Not Yet )</i>'):'').'</div>
                                            </div>
                                        </td>
                                        <td rowspan="8" class="no-padding">
                                            <div class="panel panel-flat" style="margin-bottom: 0px;">
                                                <div class="panel-body">
                                                    <div class="row">
                                                        <table class="table no-margin">
                                                            <tbody>
                                                                <tr>
                                                                    <td style="border-top:none; width:2%;" align="center"><span class="text-semibold">No</span></td>
                                                                    <td style="border-top:none; width:5%;"><span class="text-semibold" width="10">Claim No</span></td>
                                                                    <td style="border-top:none"><span class="text-semibold">Description</span></td>
                                                                    <td style="border-top:none; width:5%;" align="right"><span class="text-semibold">Amount</span></td>
                                                                </tr>
                                                                '
                                                                .$claim_detail_text.
                                                                '
                                                                <tr>
                                                                    <td style="padding-top:2px;padding-bottom:2px;" align="right" colspan="3"><span class="text-semibold">PPN</span></td>
                                                                    <td style="padding-top:2px;padding-bottom:2px;" align="right">
                                                                        <span class="display-block text-muted">'.number_format($data->total_ppn,0).'</span>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td style="padding-top:2px;padding-bottom:2px;" align="right" colspan="3"><span class="text-semibold">PPH</span></td>
                                                                    <td style="padding-top:2px;padding-bottom:2px;" align="right">
                                                                        <span class="display-block text-muted">'.number_format($data->total_pph,0).'</span>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td style="padding-top:2px;padding-bottom:2px;" align="right" colspan="3"><span class="text-semibold">Total Amount</span></td>
                                                                    <td style="padding-top:2px;padding-bottom:2px;" align="right" style="width:100px">
                                                                        <span class="display-block text-muted"><b>'.number_format($data->total_amount,0).'</b></span>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td width="50%" colspan="4" style="height:50px; vertical-align:top;">
                                                                        <span class="text-semibold">Requirement</span>
                                                                        <span class="display-block text-muted">'.$pending_type.'</span>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td colspan="4" style="vertical-align:top">
                                                                        <span class="text-semibold">Remarks</span>
                                                                        <span class="display-block text-muted">'.$data->remarks.'</span>
                                                                    </td>
                                                                </tr>
                                                            </tbody>
                                                        </table>
                                                    </div>
                                                </div>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr class="text-nowrap">
                                        <td class="text-center">
                                            <h6 class="no-margin">'.$spv_acc_date_progress.'</h6>
                                        </td>
                                        <td style="padding-left: 0px;">
                                            <div class="media-body">
                                                <a href="#" class="display-inline-block text-default text-semibold letter-icon-title">Spv Accounting</a>
                                                <div class="text-muted text-size-small"></span>'.($data->spv_acc_date_in?$data->spv_acc_date_in.' &raquo; '.
                                                                                                ($data->spv_acc_date_out?$data->spv_acc_date_out:'<i>( Not Yet )</i>'):'').'</div>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr class="text-nowrap">
                                        <td class="text-center">
                                            <h6 class="no-margin">'.$dept_sales_date_progress.'</h6>
                                        </td>
                                        <td style="padding-left: 0px;">
                                            <div class="media-body">
                                                <a href="#" class="display-inline-block text-default text-semibold letter-icon-title">Department Sales</a>
                                                <div class="text-muted text-size-small"></span>'.($data->dept_sales_date_in?$data->dept_sales_date_in.' &raquo; '.
                                                                                                ($data->dept_sales_date_out?$data->dept_sales_date_out:'<i>( Not Yet )</i>'):'').'</div>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr class="text-nowrap">
                                        <td class="text-center">
                                            <h6 class="no-margin">'.$ap_date_progress.'</h6>
                                        </td>
                                        <td style="padding-left: 0px;">
                                            <div class="media-body">
                                                <a href="#" class="display-inline-block text-default text-semibold letter-icon-title">Account Payable</a>
                                                <div class="text-muted text-size-small"></span>'.$data->ap_date_in.'</div>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr class="text-nowrap">
                                        <td class="text-center">
                                            <h6 class="no-margin">'.$finance_date_progress.'</h6>
                                        </td>
                                        <td style="padding-left: 0px;">
                                            <div class="media-body">
                                                <a href="#" class="display-inline-block text-default text-semibold letter-icon-title">Finance</a>
                                                <div class="text-muted text-size-small"></span>'.$data->finance_date_in.'</div>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr class="text-nowrap">
                                        <td class="text-center">
                                            <h6 class="no-margin">'.$complete_date_progress.'</h6>
                                        </td>
                                        <td style="padding-left: 0px;">
                                            <div class="media-body">
                                                <a href="#" class="display-inline-block text-default text-semibold letter-icon-title">Complete</a>
                                                <div class="text-muted text-size-small"></span>'.$data->complete_date.'</div>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr class="text-nowrap">
                                        <td class="text-center">
                                            <h6 class="no-margin">'.$transfer_date_progress.'</h6>
                                        </td>
                                        <td style="padding-left: 0px;">
                                            <div class="media-body">
                                                <a href="#" class="display-inline-block text-default text-semibold letter-icon-title">Transfer</a>
                                                <div class="text-muted text-size-small"></span>'.$data->transfer_date.'</div>
                                            </div>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <div class="tab-pane no-padding" id="tab_history">
                      <table class="table claim-detail-table">
                        <thead>
                          <tr>
                            <th style="width:10px;">Updated At</th>
                            <th style="width:10px;">Updated By</th>
                            <th style="width:10px;">Status</th>
                            <th style="width:10px;">Requirement</th>
                            <th>Remarks</th>
                          </tr>
                        </thead>
                        <tbody>
                        </tbody>
                      </table>
                    </div>
                </div>
            </div>
        </div>
        ';
        echo $html;
    }
    public function update(Request $request, $id)
    {
      DB::beginTransaction();
      if(DB::connection('mysql'))
      {
        if($request->deleteid)
        {
          // DB::table('tbl_material')->where('id',$id)->delete();
          // DB::commit();
          // return redirect('material')->with('danger', 'Material '.$request->material.' has been deleted.');
        }
        else 
        {
            $change_month = array('Jan'=>'01','Feb'=>'02','Mar'=>'03','Apr'=>'04','May'=>'05','Jun'=>'06',
                                  'Jul'=>'07','Aug'=>'08','Sep'=>'09','Oct'=>'10','Nov'=>'11','Dec'=>'12');
            $today = date('Y-m-d');
            $admin_date_in = ConvertionHelper::dateformat($request->admin_date_in);
            $admin_date_out = ConvertionHelper::dateformat($request->admin_date_out);
            $spv_date_in = ConvertionHelper::dateformat($request->spv_date_in);
            $spv_date_out = ConvertionHelper::dateformat($request->spv_date_out);
            $sales_date_in = ConvertionHelper::dateformat($request->sales_date_in);
            $sales_date_out = ConvertionHelper::dateformat($request->sales_date_out);
            $ap_date_in = ConvertionHelper::dateformat($request->ap_date_in);
            $finance_date_in = ConvertionHelper::dateformat($request->finance_date_in);
            $complete_date = ConvertionHelper::dateformat($request->complete_date);
            $transfer_date = ConvertionHelper::dateformat($request->transfer_date);
            $tax_expired_date = ConvertionHelper::dateformat($request->tax_expired_date);
            $periode_from = substr($request->period1, 7,4).'-'.$change_month[substr($request->period1, 3,3)].'-'.substr($request->period1,0,2);
            $periode_to = substr($request->period1, 21,4).'-'.$change_month[substr($request->period1, 17,3)].'-'.substr($request->period1,14,2);
            $distributor = DB::table('distributor')->where('id',$request->distributor)->first();
            $ppn = DB::table('ppn')->where('id',$request->ppn)->first();
            $pph = DB::table('pph')->where('id',$request->pph)->first();
            $status = DB::table('status')->where('id',$request->status)->first();
            $dpp = floatval(str_replace(',', '', $request->dpp));
            $pending_type = '';
            $new_line = '
';
            if($request->pending_type)
            {
                foreach ($request->pending_type as $key => $value) 
                {
                    $pending_type .= $pending_type==''?$value:$new_line.$value;
                }
            }
            DB::table('claim')->where('id',$id)
                            ->update(array( 'no_pr'               => $request->no_pr,
                                            'pic_name'            => strtoupper($request->pic),
                                            'distributor_id'      => $request->distributor,
                                            'distributor_name'    => $distributor->name,
                                            'total_dpp'           => floatval(str_replace(",","",$dpp)),
                                            'ppn_id'              => $request->ppn,
                                            'total_ppn'           => ($dpp*$ppn->percent/100),
                                            'pph_id'              => $request->pph,
                                            'total_pph'           => ($dpp*$pph->percent/100),
                                            'total_amount'        => floatval(str_replace(",","",$request->amount)),
                                            'pending_type'        => $pending_type,
                                            'status'              => $request->status,
                                            'status_name'         => $status->name,
                                            'admin_acc_date_in'   => $admin_date_in,
                                            'admin_acc_date_out'  => $admin_date_out,
                                            'spv_acc_date_in'     => $spv_date_in,
                                            'spv_acc_date_out'    => $spv_date_out,
                                            'dept_sales_date_in'  => $sales_date_in,
                                            'dept_sales_date_out' => $sales_date_out,
                                            'ap_date_in'          => $ap_date_in,
                                            'finance_date_in'     => $finance_date_in,
                                            'complete_date'       => $complete_date,
                                            'transfer_date'       => $transfer_date,
                                            'tax_expired_date'    => $tax_expired_date,
                                            'remarks'             => $request->remarks,
                                            'confirm_date'        => ($pending_type==''?NULL:date('Y-m-d')),
                                            'update_at'           => date('Y-m-d H:i:s'),
                                            'update_by'           => session('name'), ) );

            $check_data = DB::table('claim_log')->where('claim_id',$id)
                                                ->whereRaw('week = IF('.intval(substr($today,-2)).'<8,1,IF('.intval(substr($today,-2)).'<15,2,IF('.intval(substr($today,-2)).'<22,3,4)))')
                                                ->update(array('week_status'=>0));
            DB::table('claim_log')->insert(array(   'no_pr'               => $request->no_pr,
                                                    'pic_name'            => strtoupper($request->pic),
                                                    'distributor_id'      => $request->distributor,
                                                    'distributor_name'    => $distributor->name,
                                                    'total_dpp'           => floatval(str_replace(",","",$dpp)),
                                                    'ppn_id'              => $request->ppn,
                                                    'total_ppn'           => ($dpp*$ppn->percent/100),
                                                    'pph_id'              => $request->pph,
                                                    'total_pph'           => ($dpp*$pph->percent/100),
                                                    'tax_expired_date'    => ($request->ppn==1?NULL:DB::raw('"'.$periode_to.'" + INTERVAL 3 MONTH')),
                                                    'total_amount'        => floatval(str_replace(",","",$request->amount)),
                                                    'pending_type'        => $pending_type,
                                                    'status'              => $request->status,
                                                    'status_name'         => $status->name,
                                                    'status_date'         => $today,
                                                	'week'                => DB::raw('IF('.intval(substr($today,-2)).'<8,1,IF('.intval(substr($today,-2)).'<15,2,IF('.intval(substr($today,-2)).'<22,3,4)))'),
                                                    'admin_acc_date_in'   => $admin_date_in,
                                                    'admin_acc_date_out'  => $admin_date_out,
                                                    'spv_acc_date_in'     => $spv_date_in,
                                                    'spv_acc_date_out'    => $spv_date_out,
                                                    'dept_sales_date_in'  => $sales_date_in,
                                                    'dept_sales_date_out' => $sales_date_out,
                                                    'ap_date_in'          => $ap_date_in,
                                                    'finance_date_in'     => $finance_date_in,
                                                    'complete_date'       => $complete_date,
                                                    'transfer_date'       => $transfer_date,
                                                    'tax_expired_date'    => $tax_expired_date,
                                                    'remarks'             => $request->remarks,
                                                    'confirm_date'        => ($pending_type==''?NULL:date('Y-m-d')),
                                                    'update_at'           => date('Y-m-d H:i:s'),
                                                    'update_by'           => session('name'),
                                                    'claim_id'            => $id ) );
            $list_delete_detail = array();
            for ($i=1; $i <= 100; $i++)
            {
                if($request->input('no_claim'.$i))
                {
                    $period = $request->input('period'.$i);
                    $periode_from = substr($period, 7,4).'-'.$change_month[substr($period, 3,3)].'-'.substr($period,0,2);
                    $periode_to = substr($period, 21,4).'-'.$change_month[substr($period, 17,3)].'-'.substr($period,14,2);
                    $dpp = floatval(str_replace(',', '', $request->input('dpp_detail'.$i)));
                    if($request->input('detail_id'.$i))
                    {
                        DB::table('claim_detail')->where('id',$request->input('detail_id'.$i))
                                                 ->update(array('no_claim'      => $request->input('no_claim'.$i),
                                                                'description'   => $request->input('description'.$i),
                                                                'periode_from'  => $periode_from,
                                                                'periode_to'    => $periode_to,
                                                                'periode_text'  => $period,
                                                                'dpp'           => $dpp) );
                        array_push($list_delete_detail,$request->input('detail_id'.$i));
                    }
                    else
                    {
                        $id_insert = DB::table('claim_detail')->insertGetId(array('no_claim'      => $request->input('no_claim'.$i),
                                                                                  'description'   => $request->input('description'.$i),
                                                                                  'periode_from'  => $periode_from,
                                                                                  'periode_to'    => $periode_to,
                                                                                  'periode_text'  => $period,
                                                                                  'dpp'           => $dpp,
                                                                                  'claim_id'      => $id ) );
                        array_push($list_delete_detail,$id_insert);
                    }
                }
            }
            if(COUNT($list_delete_detail)>0)
            {
                DB::table('claim_detail')->where('claim_id',$id)->whereNotIn('id',$list_delete_detail)->delete();
            }
            DB::commit();
            return redirect('claim')->with('info', 'No.PR '.$request->no_pr.' has been updated successfully.');
        }
      }
      else
      {
        DB::rollback();
        return redirect('claim')->with('info', 'No.PR '.$request->no_pr.' failed to change.');
      }
      return redirect('claim')->with('info', 'No.PR '.$request->no_pr.' failed to change.');
    }

    public function create(Request $request)
    {
        $column = 1;
        $row = 1;
        $newline = "\n";
        $data_export = DB::table('claim AS A')->join("claim_detail AS B", "B.claim_id", "=", "A.id");
        $spreadsheet = new Spreadsheet();
        $spreadsheet->getProperties()
                ->setCreator("Claim Progress System")
                ->setLastModifiedBy("PT.Unza Vitalis")
                ->setTitle("Outstanding Claim");
        $sheet = $spreadsheet->getActiveSheet();
        $sheet->setCellValueByColumnAndRow($column,$row++, 'REKAP KLAIM '.strtoupper(date('( d M Y )')));
        $header_detail_text = '';
        if($request->distributor!='ALL')
        {
            $distributor = DB::table('distributor')->where('id',$request->distributor)->first();
            $data_export = $data_export->where('distributor_id',$request->distributor);
            $header_detail_text = $distributor->name;
        }
        if($request->area!='ALL')
        {
            $list_distributor_by_area = array();    
            $distributor = DB::table('distributor')->where('area_id',$request->area)->get();
            foreach ($distributor as $value) { array_push($list_distributor_by_area,$value->id); }
            $data_export = $data_export->whereIn('distributor_id',$list_distributor_by_area);
        }
        elseif(session('area_id')!=0)
        {
            $list_distributor_by_area = array();    
            $distributor = DB::table('distributor')->where('area_id',session('area_id'))->get();
            foreach ($distributor as $value) { array_push($list_distributor_by_area,$value->id); }
            $data_export = $data_export->whereIn('distributor_id',$list_distributor_by_area);
        }
        if($request->status!='ALL')
        {
            $status = DB::table('status')->where('id',$request->status)->first();
            $data_export = $data_export->where('status',$request->status);
            $header_detail_text .= $header_detail_text==''?'STATUS : '.$status->name:' ( '.$status->name. ')';
        }
        $data_export = $data_export->orderBy('created_at','DESC');
        $data_export = $data_export->orderBy('no_pr','ASC');
        $data_export = $data_export->get();
        $sheet->setCellValueByColumnAndRow($column,$row, $header_detail_text);
        $sheet->mergeCells(ExcelHelper::range(1,26,1,1));
        $sheet->mergeCells(ExcelHelper::range(1,26,2,3));
        $sheet->getStyle(ExcelHelper::range(1,1,1,3))->applyFromArray(ExcelHelper::style('BT','',18));
        $column = 1;
        $row = 5;
        $sheet->setCellValueByColumnAndRow(1,4, 'NO.');
        $sheet->mergeCells(ExcelHelper::range(1,1,4,6));
        $sheet->setCellValueByColumnAndRow(2,4, 'TGL');
        $sheet->mergeCells(ExcelHelper::range(2,9,4,4));
        $sheet->setCellValueByColumnAndRow(10,4, 'NO. PR');
        $sheet->mergeCells(ExcelHelper::range(10,10,4,6));
        $sheet->setCellValueByColumnAndRow(11,4, 'PIC');
        $sheet->mergeCells(ExcelHelper::range(11,11,4,6));
        $sheet->setCellValueByColumnAndRow(12,4, 'NAMA DISTRIBUTOR');
        $sheet->mergeCells(ExcelHelper::range(12,12,4,6));
        $sheet->setCellValueByColumnAndRow(13,4, 'NO. KLAIM/INVOICE');
        $sheet->mergeCells(ExcelHelper::range(13,13,4,6));
        $sheet->setCellValueByColumnAndRow(14,4, 'DESCRIPTION');
        $sheet->mergeCells(ExcelHelper::range(14,14,4,6));
        $sheet->setCellValueByColumnAndRow(15,4, 'PERIODE');
        $sheet->mergeCells(ExcelHelper::range(15,15,4,6));
        $sheet->setCellValueByColumnAndRow(16,4, 'AMOUNT');
        $sheet->mergeCells(ExcelHelper::range(16,16,4,6));
        $sheet->setCellValueByColumnAndRow(17,4, 'TOTAL');
        $sheet->mergeCells(ExcelHelper::range(17,19,4,4));
        $sheet->setCellValueByColumnAndRow(20,4, 'TOTAL AMOUNT');
        $sheet->mergeCells(ExcelHelper::range(20,20,4,6));
        $sheet->setCellValueByColumnAndRow(21,4, 'EXP. TAX');
        $sheet->mergeCells(ExcelHelper::range(21,21,4,6));
        $sheet->setCellValueByColumnAndRow(22,4, 'LIST KEKURANGAN DOK');
        $sheet->mergeCells(ExcelHelper::range(22,22,4,6));
        $sheet->setCellValueByColumnAndRow(23,4, 'COMPLETE '.$newline.' DATE');
        $sheet->mergeCells(ExcelHelper::range(23,23,4,6));
        $sheet->setCellValueByColumnAndRow(24,4, 'TRANSFER '.$newline.' DATE');
        $sheet->mergeCells(ExcelHelper::range(24,24,4,6));
        $sheet->setCellValueByColumnAndRow(25,4, 'STATUS');
        $sheet->mergeCells(ExcelHelper::range(25,25,4,6));
        $sheet->setCellValueByColumnAndRow(26,4, 'KETERANGAN');
        $sheet->mergeCells(ExcelHelper::range(26,26,4,6));
        $sheet->setCellValueByColumnAndRow(2,5, 'ADMIN ACC');
        $sheet->mergeCells(ExcelHelper::range(2,3,5,5));
        $sheet->setCellValueByColumnAndRow(4,5, 'SPV ACC');
        $sheet->mergeCells(ExcelHelper::range(4,5,5,5));
        $sheet->setCellValueByColumnAndRow(6,5, 'DEPT SALES');
        $sheet->mergeCells(ExcelHelper::range(6,7,5,5));
        $sheet->setCellValueByColumnAndRow(8,5, 'A/P');
        $sheet->setCellValueByColumnAndRow(9,5, 'FINANCE');
        $sheet->setCellValueByColumnAndRow(17,5, 'DPP');
        $sheet->mergeCells(ExcelHelper::range(17,17,5,6));
        $sheet->setCellValueByColumnAndRow(18,5, 'PPN');
        $sheet->mergeCells(ExcelHelper::range(18,18,5,6));
        $sheet->setCellValueByColumnAndRow(19,5, 'PPH');
        $sheet->mergeCells(ExcelHelper::range(19,19,5,6));
        $sheet->setCellValueByColumnAndRow(2,6, 'IN');
        $sheet->setCellValueByColumnAndRow(3,6, 'OUT');
        $sheet->setCellValueByColumnAndRow(4,6, 'IN');
        $sheet->setCellValueByColumnAndRow(5,6, 'OUT');
        $sheet->setCellValueByColumnAndRow(6,6, 'IN');
        $sheet->setCellValueByColumnAndRow(7,6, 'OUT');
        $sheet->setCellValueByColumnAndRow(8,6, 'IN');
        $sheet->setCellValueByColumnAndRow(9,6, 'IN');
        $sheet->getStyle(ExcelHelper::range(1,26,4,6))->applyFromArray(ExcelHelper::style('1CMB','gray'));
        $sheet->getStyle(ExcelHelper::range(1,26,4,6))->getAlignment()->setWrapText(true);
        if($data_export)
        {
            $column = 1;
            $row = 7;
            $marge_status = 7;
            $check_data = '';
            $i = 1;
            foreach ($data_export as $value) 
            {
                $alert = 0;
                if($value->admin_acc_date_in)
                {
                    if(!$value->admin_acc_date_out)
                    {
                        $diff = strtotime(date('Y-m-d')) - (strtotime($value->admin_acc_date_in));
                        $admin_acc_date_progress = floor($diff/3600/24);
                        $admin_acc_date_progress>=30?($alert = 1):'';
                    }
                }
                if($value->spv_acc_date_in)
                {
                    if(!$value->spv_acc_date_out)
                    {
                        $diff = strtotime(date('Y-m-d')) - (strtotime($value->spv_acc_date_in));
                        $spv_acc_date_progress = floor($diff/3600/24);
                        $spv_acc_date_progress>=30?($alert = 1):'';
                    }
                }
                if($value->dept_sales_date_in)
                {
                    if(!$value->dept_sales_date_out)
                    {
                        $diff = strtotime(date('Y-m-d')) - (strtotime($value->dept_sales_date_in));
                        $dept_sales_date_progress = floor($diff/3600/24);
                        $dept_sales_date_progress>=30?($alert = 1):'';
                    }
                }
                if($value->confirm_date)
                {
                    $diff = strtotime(date('Y-m-d')) - (strtotime($value->confirm_date));
                    $confirm_date_progress = floor($diff/3600/24);
                    $confirm_date_progress>=30?($alert = 1):'';
                }
                if($check_data!=$value->claim_id)
                {
                    $check_data = $value->claim_id;
                    if($row>7)
                    {
                        $sheet->mergeCells(ExcelHelper::range(1,1,$marge_status,$row-1));
                        $sheet->mergeCells(ExcelHelper::range(2,2,$marge_status,$row-1));
                        $sheet->mergeCells(ExcelHelper::range(3,3,$marge_status,$row-1));
                        $sheet->mergeCells(ExcelHelper::range(4,4,$marge_status,$row-1));
                        $sheet->mergeCells(ExcelHelper::range(5,5,$marge_status,$row-1));
                        $sheet->mergeCells(ExcelHelper::range(6,6,$marge_status,$row-1));
                        $sheet->mergeCells(ExcelHelper::range(7,7,$marge_status,$row-1));
                        $sheet->mergeCells(ExcelHelper::range(8,8,$marge_status,$row-1));
                        $sheet->mergeCells(ExcelHelper::range(9,9,$marge_status,$row-1));
                        $sheet->mergeCells(ExcelHelper::range(10,10,$marge_status,$row-1));
                        $sheet->mergeCells(ExcelHelper::range(11,11,$marge_status,$row-1));
                        $sheet->mergeCells(ExcelHelper::range(12,12,$marge_status,$row-1));
                        $sheet->mergeCells(ExcelHelper::range(17,17,$marge_status,$row-1));
                        $sheet->mergeCells(ExcelHelper::range(18,18,$marge_status,$row-1));
                        $sheet->mergeCells(ExcelHelper::range(19,19,$marge_status,$row-1));
                        $sheet->mergeCells(ExcelHelper::range(20,20,$marge_status,$row-1));
                        $sheet->mergeCells(ExcelHelper::range(21,21,$marge_status,$row-1));
                        $sheet->mergeCells(ExcelHelper::range(22,22,$marge_status,$row-1));
                        $sheet->mergeCells(ExcelHelper::range(23,23,$marge_status,$row-1));
                        $sheet->mergeCells(ExcelHelper::range(24,24,$marge_status,$row-1));
                        $sheet->mergeCells(ExcelHelper::range(25,25,$marge_status,$row-1));
                        $sheet->mergeCells(ExcelHelper::range(26,26,$marge_status,$row-1));
                    }
                    $marge_status = $row;
                    $admin_acc_date_in = $value->admin_acc_date_in;
                    $admin_acc_date_out = $value->admin_acc_date_out;
                    $spv_acc_date_in = $value->spv_acc_date_in;
                    $spv_acc_date_out = $value->spv_acc_date_out;
                    $dept_sales_date_in = $value->dept_sales_date_in;
                    $dept_sales_date_out = $value->dept_sales_date_out;
                    $ap_date_in = $value->ap_date_in;
                    $finance_date_in = $value->finance_date_in;
                    $no_pr = $value->no_pr;
                    $pic_name = $value->pic_name;
                    $distributor_name = $value->distributor_name;
                    $total_dpp = $value->total_dpp;
                    $total_ppn = $value->total_ppn;
                    $total_pph = $value->total_pph;
                    $total_amount = $value->total_amount;
                    $tax_expired_date = $value->tax_expired_date;
                    $no = $i++;
                }
                else
                {
                    $admin_acc_date_in = '';
                    $admin_acc_date_out = '';
                    $spv_acc_date_in = '';
                    $spv_acc_date_out = '';
                    $dept_sales_date_in = '';
                    $dept_sales_date_out = '';
                    $ap_date_in = '';
                    $finance_date_in = '';
                    $no_pr = '';
                    $pic_name = '';
                    $distributor_name = '';
                    $total_dpp = '';
                    $total_ppn = '';
                    $total_pph = '';
                    $total_amount = '';
                    $tax_expired_date = '';
                    $no = '';
                }
                $sheet->setCellValueByColumnAndRow($column++, $row, $no);
                $sheet->setCellValueByColumnAndRow($column++, $row, $admin_acc_date_in);
                $sheet->setCellValueByColumnAndRow($column++, $row, $value->admin_acc_date_out);
                $sheet->setCellValueByColumnAndRow($column++, $row, $value->spv_acc_date_in);
                $sheet->setCellValueByColumnAndRow($column++, $row, $value->spv_acc_date_out);
                $sheet->setCellValueByColumnAndRow($column++, $row, $value->dept_sales_date_in);
                $sheet->setCellValueByColumnAndRow($column++, $row, $value->dept_sales_date_out);
                $sheet->setCellValueByColumnAndRow($column++, $row, $value->ap_date_in);
                $sheet->setCellValueByColumnAndRow($column++, $row, $value->finance_date_in);
                $sheet->setCellValueByColumnAndRow($column++, $row, $value->no_pr);
                $sheet->setCellValueByColumnAndRow($column++, $row, $value->pic_name);
                $sheet->setCellValueByColumnAndRow($column++, $row, $value->distributor_name);
                $sheet->setCellValueByColumnAndRow($column++, $row, $value->no_claim);
                $sheet->setCellValueByColumnAndRow($column++, $row, $value->description);
                $sheet->setCellValueByColumnAndRow($column++, $row, $value->periode_text);
                $sheet->setCellValueByColumnAndRow($column++, $row, $value->dpp);
                $sheet->setCellValueByColumnAndRow($column++, $row, $value->total_dpp);
                $sheet->setCellValueByColumnAndRow($column++, $row, $value->total_ppn);
                $sheet->setCellValueByColumnAndRow($column++, $row, $value->total_pph);
                $sheet->setCellValueByColumnAndRow($column++, $row, $value->total_amount);
                $sheet->setCellValueByColumnAndRow($column++, $row, $value->tax_expired_date);
                $pending_type_array = preg_split('/\r\n|\r|\n/', $value->pending_type);
                $pending_type = '';
                foreach ($pending_type_array as $key => $val) {
                    $pending_type .= $pending_type==''?$val:', '.$val;
                }
                $sheet->setCellValueByColumnAndRow($column++, $row, $pending_type);
                $sheet->setCellValueByColumnAndRow($column++, $row, $value->complete_date);
                $sheet->setCellValueByColumnAndRow($column++, $row, $value->transfer_date);
                $sheet->setCellValueByColumnAndRow($column++, $row, $value->status_name);
                if($alert==1)
                {
                    $sheet->getStyle(ExcelHelper::range($column-1,$column-1,$row,$row))->applyFromArray(ExcelHelper::style('','orange'));
                }
                $sheet->setCellValueByColumnAndRow($column++, $row, $value->remarks);
                ++$row;
                $column = 1;
            }
            if($marge_status!=($row-1))
            {
                $sheet->mergeCells(ExcelHelper::range(1,1,$marge_status,$row-1));
                $sheet->mergeCells(ExcelHelper::range(2,2,$marge_status,$row-1));
                $sheet->mergeCells(ExcelHelper::range(3,3,$marge_status,$row-1));
                $sheet->mergeCells(ExcelHelper::range(4,4,$marge_status,$row-1));
                $sheet->mergeCells(ExcelHelper::range(5,5,$marge_status,$row-1));
                $sheet->mergeCells(ExcelHelper::range(6,6,$marge_status,$row-1));
                $sheet->mergeCells(ExcelHelper::range(7,7,$marge_status,$row-1));
                $sheet->mergeCells(ExcelHelper::range(8,8,$marge_status,$row-1));
                $sheet->mergeCells(ExcelHelper::range(9,9,$marge_status,$row-1));
                $sheet->mergeCells(ExcelHelper::range(10,10,$marge_status,$row-1));
                $sheet->mergeCells(ExcelHelper::range(11,11,$marge_status,$row-1));
                $sheet->mergeCells(ExcelHelper::range(12,12,$marge_status,$row-1));
                $sheet->mergeCells(ExcelHelper::range(17,17,$marge_status,$row-1));
                $sheet->mergeCells(ExcelHelper::range(18,18,$marge_status,$row-1));
                $sheet->mergeCells(ExcelHelper::range(19,19,$marge_status,$row-1));
                $sheet->mergeCells(ExcelHelper::range(20,20,$marge_status,$row-1));
                $sheet->mergeCells(ExcelHelper::range(21,21,$marge_status,$row-1));
                $sheet->mergeCells(ExcelHelper::range(22,22,$marge_status,$row-1));
                $sheet->mergeCells(ExcelHelper::range(23,23,$marge_status,$row-1));
                $sheet->mergeCells(ExcelHelper::range(24,24,$marge_status,$row-1));
                $sheet->mergeCells(ExcelHelper::range(25,25,$marge_status,$row-1));
                $sheet->mergeCells(ExcelHelper::range(26,26,$marge_status,$row-1));
            }
            $sheet->getStyle(ExcelHelper::range(1,26,7,$row-1))->applyFromArray(ExcelHelper::style('1LM'));
            $sheet->getStyle(ExcelHelper::range(1,9,7,$row-1))->applyFromArray(ExcelHelper::style('1CM'));
            $sheet->getStyle(ExcelHelper::range(16,20,7,$row-1))->applyFromArray(ExcelHelper::style('1RM'));
            $sheet->getStyle(ExcelHelper::range(21,21,7,$row-1))->applyFromArray(ExcelHelper::style('1CM'));
            $sheet->setAutoFilter(ExcelHelper::range(1,26,6,$row-1));
            $sheet->freezePane(ExcelHelper::cell(1,7));
            $nCols = 26; 
            $col_default = [2=>'',3=>'',4=>'',5=>'',6=>'',7=>'',8=>'',9=>'',16=>'',17=>'',18=>'',19=>'',20=>'',21=>'',23=>'',24=>''];
            foreach (range(0, $nCols) as $col) {
                if(ISSET($col_default[$col]))
                {
                    $sheet->getColumnDimensionByColumn($col)->setWidth(12);
                }
                else
                {
                    $sheet->getColumnDimensionByColumn($col)->setAutoSize(true);                
                }
            }
        }
        $writer = new Xlsx($spreadsheet);
        header('Content-Type: application/vnd.ms-excel');
        header('Content-Disposition: attachment;filename="Outstanding-Claim.xlsx"'); 
        header('Cache-Control: max-age=0');
        $writer->save('php://output');
    }


    public function create_pdf(Request $request)
    {
        $pdf = new Fpdf();
        $pdf::AddPage('L','Legal');
        $pdf::SetTitle('CPS');
        $pdf::SetAuthor('Unza Vitalis | Claim Progress System');

        $data_export = DB::table('claim AS A')->join("claim_detail AS B", "B.claim_id", "=", "A.id");
        $header_detail_text = '';
        if($request->distributor!='ALL')
        {
            $distributor = DB::table('distributor')->where('id',$request->distributor)->first();
            $data_export = $data_export->where('distributor_id',$request->distributor);
            $header_detail_text = $distributor->name;
        }
        if($request->area!='ALL')
        {
            $list_distributor_by_area = array();    
            $distributor = DB::table('distributor')->where('area_id',$request->area)->get();
            foreach ($distributor as $value) { array_push($list_distributor_by_area,$value->id); }
            $data_export = $data_export->whereIn('distributor_id',$list_distributor_by_area);
        }
        elseif(session('area_id')!=0)
        {
            $list_distributor_by_area = array();    
            $distributor = DB::table('distributor')->where('area_id',session('area_id'))->get();
            foreach ($distributor as $value) { array_push($list_distributor_by_area,$value->id); }
            $data_export = $data_export->whereIn('distributor_id',$list_distributor_by_area);
        }
        if($request->status!='ALL')
        {
            $status = DB::table('status')->where('id',$request->status)->first();
            $data_export = $data_export->where('status',$request->status);
            $header_detail_text .= $header_detail_text==''?'STATUS : '.$status->name:' ( '.$status->name. ')';
        }
        $data_export = $data_export->orderBy('created_at','DESC');
        $data_export = $data_export->orderBy('no_pr','ASC');
        $data_export = $data_export->get();
        
        $pdf::SetFont('Times','B',18);
        $pdf::Cell(190, 10, 'REKAP KLAIM '.strtoupper(date('( d M Y )')), 0, '0', 'L', false);
        $pdf::Ln(8);
        $pdf::Cell(190, 10, $header_detail_text, 0, '0', 'L', false);
        $pdf::Ln();
        $pdf::SetFont('Times','B',8);
        $pdf::SetFillColor(204, 204, 204);
        $pdf::Cell(6, 15, 'NO.', 1, '0', 'C', false);
        $pdf::Cell(80, 5, 'TGL', 1, '0', 'C', false);
        $pdf::Cell(25, 5, 'NO.PR', 1, '0', 'C', false);
        $pdf::Cell(25, 5, 'PIC', 1, '0', 'C', false);
        $pdf::Output('I','Outstanding-Claim.pdf');
        exit();
    }
    // public function sendEmail(Request $request)
    // {
    //     $data['text_primary'] = $request->text_primary;
    //     $data['text_secondary'] = $request->text_secondary;
    //     $data['data1'] = str_replace("display: none;","",$request->data1);
    //     $data['data2'] = str_replace("display: none;","",$request->data2);
    //     $data['data11'] = str_replace("display: none;","",$request->data11);
    //     $data['data22'] = str_replace("display: none;","",$request->data22);
    //     $data['order'] = $request->order;
    //     $data['invoice'] = $request->invoice;
    //     $data['unfulfilled'] = $request->unfulfilled;
    //     $data['image1'] = $request->image1;
    //     $data['image2'] = $request->image2;
    //     $data['image3'] = $request->image3;
    //     $data['title'] = 'UFF - '.$request->text_primary;
    //     $list_email = DB::table('users')->where('active','Active')->get();
    //     $subject = 'UFF - '.$request->text_primary;
    //     $email_list = array();
    //     foreach ($list_email as $value) 
    //     {
    //         // $email_list[$value->email] = $value->fullname;
    //     }
    //     $email_list['emanuel.riolan@wipro-unza.co.id'] = 'emanuel.riolan@wipro-unza.co.id';
    //     $email_list['rizki.ramadhani@wipro-unza.co.id'] = 'rizki.ramadhani@wipro-unza.co.id';
    //     $email_list['emanuelriolan@gmail.com'] = 'emanuelriolan@gmail.com';
    //     Mail::send('email', $data, function($message) use ($email_list, $subject){
    //         $message->from('us@example.com', 'Laravel');
    //         $message->to($email_list)
    //                 ->subject($subject);
    //     });
    // }
}
