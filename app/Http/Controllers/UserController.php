<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Datatables;
use DB;

class UserController extends Controller
{
    protected function index(Request $request)
    {
      if(session('type')=='SUPER USER')
      {
        $data['title'] = 'User | CPS';
        $data['body'] = 'user';
        $data['area'] =  DB::table('area')->get();
        return view('layout',$data);
      }
      else
      {
        abort(404);
      }
    }

    public function store(Request $request)
    {
      if(DB::table('user')->where('email', $request->email)->first()) 
      {
        return redirect('user')->with('info', 'Sorry User email '.$request->email.' already exists.');
      }
      else
      {
        DB::table('user')->insert( array(   'name'    => $request->name,
                                            'email'   => $request->email,
                                            'password'=> sha1(md5('unzavitalis').'cp'),
                                            'type'    => $request->type,
                                            'status'  => $request->status,
                                            'area_id' => $request->area
                                          ) 
                                  );
        return redirect('user')->with('success', 'User '.$request->name.' has been added successfully');
      } 
    }
    
    public function show($id='')
    {
      if($id=='json')
      {
        $QUERY = DB::table('user as A');
        $QUERY = $QUERY->select('A.*','B.name AS 7');
        $QUERY = $QUERY->leftJoin("area AS B", "A.area_id", "=", "B.id");
        return Datatables::of($QUERY->get())->make();
      }
      else
      {
        $detail = DB::table('user')->where('id', $id)->get()->first();
        if(!empty($detail))
        {
            echo json_encode($detail);
        }
        else
        {
            abort(404);
        }
      }
    }

    public function edit($id)
    {
    }

    public function update(Request $request, $id)
    {
      DB::beginTransaction();
      if(DB::connection('mysql'))
      {
        if($request->deleteid)
        {
          DB::table('user')->where('id',$id)->delete();
          DB::commit();
          return redirect('user')->with('warning', 'User '.$request->name.' has been deleted.');
        }
        elseif($request->resetid)
        {
          DB::table('user')->where('id',$id)->update(array('password'=> sha1(md5('unzavitalis').'cp') ) );
          DB::commit();
          return redirect('user')->with('info', 'User password '.$request->name.' has been reset.');
        }
        else 
        {
          DB::table('user')->where('id',$id)->update( array(  'name'    => $request->name,
                                                              'email'   => $request->email,
                                                              'type'    => $request->type,
                                                              'status'  => $request->status,
                                                              'area_id' => $request->area ) );
          DB::commit();
          return redirect('user')->with('success', 'User '.$request->name.' has been changed successfully.');
        }
      }
      else
      {
        DB::rollback();
        return redirect('user')->with('info', 'User '.$request->name.' failed to change.');
      }
      return redirect('user')->with('info', 'User '.$request->name.' failed to change.');
    }

    public function create()
    {
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

    public function destroy($id)
    {
        //
    }

}
