<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Datatables;
use DB;

class AreaController extends Controller
{
    protected function index(Request $request)
    {
      if(session('type')=='SUPER USER' || session('type')=='ACCOUNTING')
      {
        $data['title'] = 'Area | CPS';
        $data['body'] = 'area';
        return view('layout',$data);
      }
      else
      {
        abort(404);
      }
    }

    public function store(Request $request)
    {
      if(DB::table('area')->where('name', $request->name)->first()) 
      {
        return redirect('area')->with('info', 'Sorry Area name '.$request->name.' already exists.');
      }
      else
      {
        DB::table('area')->insert( array(   'name'   => $request->name
                                          ) 
                                  );
        return redirect('area')->with('success', 'Area '.$request->name.' has been added successfully');
      } 
    }
    
    public function show($id='')
    {
      if($id=='json')
      {
        $QUERY = DB::table('area');
        return Datatables::of($QUERY->get())->make();
      }
      else
      {
        $detail = DB::table('area')->where('id', $id)->get()->first();
        if(!empty($detail))
        {
            echo json_encode($detail);
        }
        else
        {
            abort(404);
        }
      }
    }

    public function edit($id)
    {
    }

    public function update(Request $request, $id)
    {
      DB::beginTransaction();
      if(DB::connection('mysql'))
      {
        if($request->deleteid)
        {
          // DB::table('user')->where('id',$id)->delete();
          // DB::commit();
          // return redirect('user')->with('warning', 'User '.$request->name.' has been deleted.');
        }
        else 
        {
          DB::table('area')->where('id',$id)->update( array( 'name'   => $request->name ) );
          DB::commit();
          return redirect('area')->with('success', 'Area '.$request->name.' has been changed successfully.');
        }
      }
      else
      {
        DB::rollback();
        return redirect('area')->with('info', 'Area '.$request->name.' failed to change.');
      }
      return redirect('area')->with('info', 'Area '.$request->name.' failed to change.');
    }

    public function create()
    {
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

    public function destroy($id)
    {
        //
    }

}
