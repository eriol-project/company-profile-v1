<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Datatables;
use DB;
use File;

class UploadController extends Controller
{
    public function index()
    {
        if(session('status')=='Admin')
        {
            for ($i=2018; $i <= date('Y'); $i++) {  $year[$i] = $i; }
            $data['title'] = 'Upload - UFF';
            $data['body'] = 'upload';
            $data['year'] = $year;
            $data['upload'] = DB::table('tbl_upload')->orderBy('id', 'desc')->limit(20)->get();
            $data['process'] = DB::table('tbl_upload')->whereIn('file_status', ['PENDING', 'PROCESS'])->first();
            $data['current'] = DB::table('tbl_data')->where('invoice_date','<>', '0000-00-00')
                                                    ->select(DB::raw("MIN(invoice_date) AS min"),
                                                             DB::raw("MAX(invoice_date) AS max"))->first();
            return view('layout',$data);
        }
        else
        {
            abort(404);
        }
    }
    
    public function fileUploadPost(Request $request)
    {
        $request->validate(['file' => 'required'],
                           ['year' => 'required'],
                           ['month' => 'required'],
                           ['type' => 'required']);
        $file_name = request()->file->getClientOriginalName();
        $file_name_alias = time().mt_rand(10000000, 99999999).'.'.request()->file->getClientOriginalExtension();
        
        DB::table('tbl_upload')->insert( array( 'file_name'          => $file_name, 
                                                'file_name_alias'    => $file_name_alias,
                                                'file_type'          => $request->type,
                                                'file_status'        => 'PENDING',
                                                'file_process_status'=> 0,
                                                'file_size'          => request()->file->getSize(),
                                                'file_date'          => $request->year.'-'.$request->month.'-01',
                                                'upload_by'          => session('fullname')
                                                )
                                        );
        request()->file->move(public_path('files'), $file_name_alias);
        session()->flash('success', $file_name.' uploaded.');
 
        return response()->json(['success'=>'You have successfully upload file.']);
    }
    
    
    public function show($id='')
    {
        $detail = DB::table('tbl_upload')->where('id', $id)->get()->first();
        if(!empty($detail))
        {
            echo json_encode($detail->text_log);
        }
        else
        {
            abort(404);
        }
    }

    public function check_process()
    {
        $data = DB::table('tbl_upload')->where('file_status', 'PROCESS')->first();
        if(!$data)
        {
            $data = 100;
        }
        else
        {
            $data = $data->file_process_status;
        }
        echo json_encode($data);
    }

    public function check_process_calculate()
    {
        $data = DB::table('tbl_upload')->where('file_status', 'PROCESS')->first();
        if(!$data)
        {
            $show = '-';
        }
        else
        {
            $data_count = DB::table('tbl_data')
                            ->select(DB::raw('COUNT(*) AS count_now, ( SELECT COUNT(*) FROM tbl_data WHERE transaction_id = '.$data->id.' ) AS count_from'))
                            ->where('invoice_total', 0)
                            ->where('transaction_id',$data->id)
                            ->first();
            $show = '['.($data_count->count_from-$data_count->count_now).'/'.$data_count->count_from.']';
        }
        echo $show;
    }

    public function process_cancel()
    {
        DB::beginTransaction();
        if(DB::connection())
        {
            DB::table('tbl_upload')->whereIn('file_status', ['PENDING', 'PROCESS'])->update( array(  'file_status' => 'CANCELED' ) );
            DB::commit();
            session()->flash('info', 'Process has been canceled.');
            return redirect('upload');
        }
        else
        {
            DB::rollback();
            session()->flash('info', 'Failed to cancel process.');
            return redirect('upload');
        }
        session()->flash('info', 'Failed to cancel process.');
        return redirect('upload');
    }
}
