<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use App\UserModel;
class LoginController extends Controller
{
    public function index()
    {
        if(session('is_login'))
        {
            return redirect()->to('/');
        }
        else
        {
            return view('login');
        }
    }

    public function editpassword(Request $request)
    {
        // DB::table('user')->where('id',session('id'))->update(array('password'=> sha1(md5($request->newpassword).'cp')) );
        // DB::commit();
        // $email = session('email');
        // $request->session()->flush();
        // $request->session()->regenerate();
        // session()->flash('success',' Your password has been changed successfully');
        // return redirect()->to('/login')->with('email', $email);
    }

    public function store(Request $request)
    {
        $email = $request->email;
        $password = sha1(md5($request->password).'cp');
        $data = UserModel::where(['email'=>$email,'password' => $password])->get()->first();
        $exist = UserModel::where(['email'=>$email])->exists();
        if($exist)
        {
            if($data)
            {
                if($data->status=='ACTIVE')
                {
                    session([
                        'is_login'       => TRUE,
                        'id'             => $data->id,
                        'name'           => $data->name,
                        'email'          => $data->email,
                        'type'           => $data->type,
                        'status'         => $data->status,
                        'area_id'        => $data->area_id,
                        'distributor_id' => $data->distributor_id
                    ]);
                }
                else
                {
                    session()->flash('warning',' Your account is "'.ucwords(strtolower($data->status)).'"');
                }
            }
            else 
            {                
                session()->flash('info',' Your password is incorrect');
            }
        }
        else
        {
            session()->flash('danger',' Email users are not registered');
        }
        return redirect()->to('/login')->withInput($request->only('email'));
    }

    protected function logout(Request $request)
    {
        $request->session()->flush();
        $request->session()->regenerate();
        return redirect('/login');
    }
}
