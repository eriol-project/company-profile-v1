<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Datatables;
use DB;
use Mail;

class HomeController extends Controller
{
    public function index()
    {
        $data['title'] = 'Dashboard | DCMS';
        $data['body'] = 'dashboard';
        $data['area'] =  DB::table('master_area')->get();
        $data['distributor'] =  DB::table('master_distributor')->get();
        $data['status'] =  DB::table('master_status')->get();
        return view('template',$data);
    }

    public function dashboard(Request $request)
    {
        // CREATE VARIABLE DATA
        $dashboard = '';
        $distributor_array = array();

        // QUERY
        $QUERY = DB::table('claim_log');
        $QUERY = $QUERY->whereRaw('year(update_at) = "'.$request->year.'"');
        $QUERY = $QUERY->whereRaw('month(update_at) = "'.$request->month.'"');

        $list_distributor_by_area = array();    
        $list_distributor = array();    
        $list_distributor_status = array();    
        $distributor = DB::table('master_distributor')->get();
        foreach ($distributor as $value) { $list_distributor_status[$value->id]=$value->status; }
        if($request->area!='ALL')
        {        
            $distributor = DB::table('master_distributor')->where('area_id',$request->area)->get();
            foreach ($distributor as $value) { array_push($list_distributor_by_area,$value->id); }
        }
        if($request->distributor!='ALL')
        {        
            $distributor = DB::table('master_distributor')->where('id',$request->distributor)->get();            
            foreach ($distributor as $value) { array_push($list_distributor,$value->id); }
        }
        if($request->area!='ALL')
        { 
            $QUERY = $QUERY->whereIn('distributor_id', $list_distributor_by_area);
        }
        if($request->distributor!='ALL')
        { 
            $QUERY = $QUERY->whereIn('distributor_id', $list_distributor);
        }
        if($request->status!='ALL')
        { 
            $QUERY = $QUERY->where('status_id', $request->status);
        }
        $QUERY = $QUERY->where('week', 1);
        $QUERY = $QUERY->orderBy('distributor_name', 'desc')->get();

        // GET DATA FROM QUERY
        foreach ($QUERY as $val) 
        {
            if(!ISSET($distributor_array[$val->distributor_name]))
            {
                $distributor_array[$val->distributor_name] = array();
                $distributor_array[$val->distributor_name]['status'] = $list_distributor_status[$val->distributor_id];
            }
            if(!ISSET($distributor_array[$val->distributor_name][$val->status_name]))
            {
                $distributor_array[$val->distributor_name][$val->status_name] = array(1=>array('sum'=>0,'total'=>0),
                                                                                      2=>array('sum'=>0,'total'=>0),
                                                                                      3=>array('sum'=>0,'total'=>0),
                                                                                      4=>array('sum'=>0,'total'=>0) );
            }
            $distributor_array[$val->distributor_name][$val->status_name][$val->week]['sum'] += 1;
            $distributor_array[$val->distributor_name][$val->status_name][$val->week]['total'] += $val->total_amount;
        }

        foreach ($distributor_array as $name => $data_1)
        {
            $type = $data_1['status']=='ACTIVE'?'success':'danger';
            $dashboard .= '
            <div class="col-xl-6 col-lg-12">
                <div class="panel panel-flat">
                    <div class="table-responsive">
                        <table class="table text-nowrap">
                        <thead>
                            <tr>
                                <th width="52%" style="vertical-align: middle;"><span class="font-weight-bold mb-0"> &nbsp;'.$name.'</span><br><span class="font-weight-bold badge badge-'.$type.'">'.$data_1['status'].'</span></th>
                                <th width="12%" class="text-center"><div class="text-muted font-weight-bold">Week 1</div></th>
                                <th width="12%" class="text-center"><div class="text-muted font-weight-bold">Week 2</div></th>
                                <th width="12%" class="text-center"><div class="text-muted font-weight-bold">Week 3</div></th>
                                <th width="12%" class="text-center"><div class="text-muted font-weight-bold">Week 4</div></th>
                            </tr>
                        </thead>
                        <tbody>';
                        foreach ($data_1 as $status => $data_2)
                        {
                            if($status!='status')
                            {
                                $dashboard .='
                                <tr>
                                    <td style="vertical-align: middle;"><span class="font-weight-bold mb-0">'.$status.'</span></td>
                                    <td class="text-center"><div><h4 class="font-weight-bold mb-0">'.number_format($data_2[1]['sum'],0).'</h4><label class="text-muted fs-sm mb-0">'.number_format($data_2[1]['total'],0).'</label></div></td>
                                    <td class="text-center"><div><h4 class="font-weight-bold mb-0">'.number_format($data_2[2]['sum'],0).'</h4><label class="text-muted fs-sm mb-0">'.number_format($data_2[2]['total'],0).'</label></div></td>
                                    <td class="text-center"><div><h4 class="font-weight-bold mb-0">'.number_format($data_2[3]['sum'],0).'</h4><label class="text-muted fs-sm mb-0">'.number_format($data_2[3]['total'],0).'</label></div></td>
                                    <td class="text-center"><div><h4 class="font-weight-bold mb-0">'.number_format($data_2[4]['sum'],0).'</h4><label class="text-muted fs-sm mb-0">'.number_format($data_2[4]['total'],0).'</label></div></td>
                                </tr>
                                ';
                            }
                        }
            $dashboard .= '
                        </tbody>
                    </table>
                    </div>
                </div>
            </div>
            ';
        }
        echo json_encode($dashboard);
    }
}
