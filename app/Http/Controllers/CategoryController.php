<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Datatables;
use DB;

class CategoryController extends Controller
{
    protected function index(Request $request)
    {
      if(session('status')=='Admin')
      {
          $data['title'] = 'Category - UFF';
          $data['body'] = 'category';
          return view('layout',$data);
      }
      else
      {
          abort(404);
      }
    }

    public function store(Request $request)
    {
      if(DB::table('tbl_category')->where('name', $request->category)->first()) 
      {
        return redirect('category')->with('info', 'Sorry category '.$request->category.' already exists.');
      }
      else
      {
        DB::table('tbl_category')->insert( array( 'name' => $request->category) );
        return redirect('category')->with('success', 'Category '.$request->category.' has been added successfully');
      } 
    }
    
    public function show($id='')
    {
      if($id=='json')
      {
        return Datatables::of(DB::table('tbl_category')->get())->make();
      }
      else
      {
        $detail = DB::table('tbl_category')->where('id', $id)->get()->first();
        if(!empty($detail))
        {
            echo json_encode(array(	$detail->name));
        }
        else
        {
            abort(404);
        }
      }
    }

    public function edit($id)
    {
    }

    public function update(Request $request, $id)
    {
      DB::beginTransaction();
      if(DB::connection('mysql'))
      {
        if($request->deleteid)
        {
          DB::table('tbl_category')->where('id',$id)->delete();
          DB::commit();
          return redirect('category')->with('danger', 'Category '.$request->category.' has been deleted.');
        }
        else 
        {
          DB::table('tbl_category')->where('id',$id)->update( array(  'name' => $request->category ) );
          DB::commit();
          return redirect('category')->with('success', 'Category '.$request->category.' has been changed successfully.');
        }
      }
      else
      {
        DB::rollback();
        return redirect('category')->with('info', 'Category '.$request->category.' failed to change.');
      }
      return redirect('category')->with('info', 'Category '.$request->category.' failed to change.');
    }

    public function create()
    {
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

    public function destroy($id)
    {
        //
    }

}
