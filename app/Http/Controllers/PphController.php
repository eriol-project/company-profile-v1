<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Datatables;
use DB;

class pphController extends Controller
{
    protected function index(Request $request)
    {
      if(session('type')=='SUPER USER' || session('type')=='ACCOUNTING')
      {
        $data['title'] = 'PPH | CPS';
        $data['body'] = 'pph';
        return view('layout',$data);
      }
      else
      {
        abort(404);
      }
    }

    public function store(Request $request)
    {
      if(DB::table('pph')->where('name', $request->name)->first()) 
      {
        return redirect('pph')->with('info', 'Sorry PPH name '.$request->name.' already exists.');
      }
      else
      {
        DB::table('pph')->insert( array(   'name'      => $request->name,
                                           'percent'   => $request->percent
                                       ) 
                                  );
        return redirect('pph')->with('success', 'PPH '.$request->name.' has been added successfully');
      } 
    }
    
    public function show($id='')
    {
      if($id=='json')
      {
        $QUERY = DB::table('pph');
        return Datatables::of($QUERY->get())->make();
      }
      else
      {
        $detail = DB::table('pph')->where('id', $id)->get()->first();
        if(!empty($detail))
        {
            echo json_encode($detail);
        }
        else
        {
            abort(404);
        }
      }
    }

    public function edit($id)
    {
    }

    public function update(Request $request, $id)
    {
      DB::beginTransaction();
      if(DB::connection('mysql'))
      {
        if($request->deleteid)
        {
          // DB::table('user')->where('id',$id)->delete();
          // DB::commit();
          // return redirect('user')->with('warning', 'User '.$request->name.' has been deleted.');
        }
        else 
        {
          DB::table('pph')->where('id',$id)->update( array( 'name'   => $request->name, 'percent'   => $request->percent ) );
          DB::commit();
          return redirect('pph')->with('success', 'PPH '.$request->name.' has been changed successfully.');
        }
      }
      else
      {
        DB::rollback();
        return redirect('pph')->with('info', 'PPH '.$request->name.' failed to change.');
      }
      return redirect('pph')->with('info', 'PPH '.$request->name.' failed to change.');
    }

    public function create()
    {
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

    public function destroy($id)
    {
        //
    }

}
