<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Datatables;
use DB;

class PendingController extends Controller
{
    protected function index(Request $request)
    {
      if(session('type')=='SUPER USER' || session('type')=='ACCOUNTING')
      {
        $data['title'] = 'Pending Type | CPS';
        $data['body'] = 'pending';
        return view('layout',$data);
      }
      else
      {
        abort(404);
      }
    }

    public function store(Request $request)
    {
      if(DB::table('pending_type')->where('name', $request->name)->first()) 
      {
        return redirect('pending')->with('info', 'Sorry Pending name '.$request->name.' already exists.');
      }
      else
      {
        DB::table('pending_type')->insert( array(   'name'   => $request->name ) );
        return redirect('pending')->with('success', 'Pending '.$request->name.' has been added successfully');
      } 
    }
    
    public function show($id='')
    {
      if($id=='json')
      {
        $QUERY = DB::table('pending_type');
        return Datatables::of($QUERY->get())->make();
      }
      else
      {
        $detail = DB::table('pending_type')->where('id', $id)->get()->first();
        if(!empty($detail))
        {
            echo json_encode($detail);
        }
        else
        {
            abort(404);
        }
      }
    }

    public function edit($id)
    {
    }

    public function update(Request $request, $id)
    {
      DB::beginTransaction();
      if(DB::connection('mysql'))
      {
        if($request->deleteid)
        {
          // DB::table('user')->where('id',$id)->delete();
          // DB::commit();
          // return redirect('user')->with('warning', 'User '.$request->name.' has been deleted.');
        }
        else 
        {
          DB::table('pending_type')->where('id',$id)->update( array( 'name'   => $request->name ) );
          DB::commit();
          return redirect('pending')->with('success', 'Pending '.$request->name.' has been changed successfully.');
        }
      }
      else
      {
        DB::rollback();
        return redirect('pending')->with('info', 'Pending '.$request->name.' failed to change.');
      }
      return redirect('pending')->with('info', 'Pending '.$request->name.' failed to change.');
    }

    public function create()
    {
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

    public function destroy($id)
    {
        //
    }

}
