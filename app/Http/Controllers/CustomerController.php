<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Datatables;
use DB;

class CustomerController extends Controller
{
    protected function index(Request $request)
    {
      if(session('status')=='Admin')
      {
          $data['title'] = 'Customer - UFF';
          $data['account'] =  DB::table('tbl_customer')->groupby('account')->get();
          $data['account_type'] =  DB::table('tbl_customer')->groupby('act_type')->get();
          $data['body'] = 'customer';
          return view('layout',$data);
      }
      else
      {
          abort(404);
      }
    }

    public function store(Request $request)
    {
      if(DB::table('tbl_customer')->where('code', $request->code)->first()) 
      {
        return redirect('customer')->with('info', 'Sorry customer code '.$request->code.' already exists.');
      }
      else
      {
        DB::table('tbl_customer')->insert( array( 'code' => $request->code,
                                                  'name' => $request->name,
                                                  'account' => $request->account,
                                                  'act_type'=> $request->act_type,
                                                ) 
                                         );
        return redirect('customer')->with('success', 'Customer '.$request->name.' has been added successfully');
      } 
    }
    
    public function show($id='')
    {
      if($id=='json')
      {
        return Datatables::of(DB::table('tbl_customer')->get())->make();
      }
      else
      {
        $detail = DB::table('tbl_customer')->where('id', $id)->get()->first();
        if(!empty($detail))
        {
            echo json_encode(array(	$detail->code,$detail->name,$detail->account,$detail->act_type ));
        }
        else
        {
            abort(404);
        }
      }
    }

    public function edit($id)
    {
    }

    public function update(Request $request, $id)
    {
      DB::beginTransaction();
      if(DB::connection('mysql'))
      {
        if($request->deleteid)
        {
          DB::table('tbl_customer')->where('id',$id)->delete();
          DB::commit();
          return redirect('customer')->with('danger', 'Customer '.$request->customer.' has been deleted.');
        }
        else 
        {
          DB::table('tbl_customer')->where('id',$id)->update( array(  'code' => $request->code,
                                                                      'name' => $request->name,
                                                                      'account' => $request->account,
                                                                      'act_type'=> $request->act_type ) );
          DB::commit();
          return redirect('customer')->with('success', 'Customer '.$request->name.' has been changed successfully.');
        }
      }
      else
      {
        DB::rollback();
        return redirect('customer')->with('info', 'Customer '.$request->name.' failed to change.');
      }
      return redirect('customer')->with('info', 'Customer '.$request->name.' failed to change.');
    }

    public function create()
    {
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

    public function destroy($id)
    {
        //
    }

}
