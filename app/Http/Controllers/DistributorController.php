<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Datatables;
use DB;

class DistributorController extends Controller
{
    protected function index(Request $request)
    {
      if(session('type')=='SUPER USER' || session('type')=='ACCOUNTING')
      {
        $data['title'] = 'Distributor | CPS';
        $data['body'] = 'distributor';
        $data['area'] =  DB::table('area')->get();
        return view('layout',$data);
      }
      else
      {
        abort(404);
      }
    }

    public function store(Request $request)
    {
      if(DB::table('distributor')->where('name', $request->name)->first()) 
      {
        return redirect('distributor')->with('info', 'Sorry Distributor name '.$request->name.' already exists.');
      }
      elseif(DB::table('distributor')->where('sap', $request->sap)->first()) 
      {
        return redirect('distributor')->with('info', 'Sorry SAP '.$request->sap.' already exists.');
      }
      else
      {
        DB::table('distributor')->insert( array(  'sap'     => $request->sap,
                                                  'name'    => $request->name,
                                                  'city'    => $request->city,
                                                  'area_id' => $request->area,
                                                  'status'  => $request->status
                                                ) 
                                        );
        return redirect('distributor')->with('success', 'Distributor '.$request->name.' has been added successfully');
      } 
    }
    
    public function show($id='')
    {
      if($id=='json')
      {
        $QUERY = DB::table('distributor as A');
        $QUERY = $QUERY->select('A.*','B.name AS 7');
        $QUERY = $QUERY->join("area AS B", "A.area_id", "=", "B.id");
        return Datatables::of($QUERY->get())->make();
      }
      else
      {
        $detail = DB::table('distributor')->where('id', $id)->get()->first();
        if(!empty($detail))
        {
            echo json_encode($detail);
        }
        else
        {
            abort(404);
        }
      }
    }

    public function edit($id)
    {
    }

    public function update(Request $request, $id)
    {
      DB::beginTransaction();
      if(DB::connection('mysql'))
      {
        if($request->deleteid)
        {
          // DB::table('user')->where('id',$id)->delete();
          // DB::commit();
          // return redirect('user')->with('warning', 'User '.$request->name.' has been deleted.');
        }
        else 
        {
          DB::table('distributor')->where('id',$id)->update( array( 'sap'    => $request->sap,
                                                                    'name'   => $request->name,
                                                                    'city'   => $request->city,
                                                                    'area_id'=> $request->area,
                                                                    'status' => $request->status) );
          DB::commit();
          return redirect('distributor')->with('success', 'Distributor '.$request->name.' has been changed successfully.');
        }
      }
      else
      {
        DB::rollback();
        return redirect('distributor')->with('info', 'Distributor '.$request->name.' failed to change.');
      }
      return redirect('distributor')->with('info', 'Distributor '.$request->name.' failed to change.');
    }

    public function create()
    {
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

    public function destroy($id)
    {
        //
    }

}
