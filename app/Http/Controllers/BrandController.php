<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Datatables;
use DB;

class BrandController extends Controller
{
    protected function index(Request $request)
    {
      if(session('status')=='Admin')
      {
          $data['title'] = 'Brand - UFF';
          $data['body'] = 'brand';
          return view('layout',$data);
      }
      else
      {
          abort(404);
      }
    }

    public function store(Request $request)
    {
      if(DB::table('tbl_brand')->where('name', $request->brand)->first()) 
      {
        return redirect('brand')->with('info', 'Sorry brand '.$request->brand.' already exists.');
      }
      else
      {
        DB::table('tbl_brand')->insert( array( 'name' => $request->brand) );
        return redirect('brand')->with('success', 'Brand '.$request->brand.' has been added successfully');
      } 
    }
    
    public function show($id='')
    {
      if($id=='json')
      {
        return Datatables::of(DB::table('tbl_brand')->get())->make();
      }
      else
      {
        $detail = DB::table('tbl_brand')->where('id', $id)->get()->first();
        if(!empty($detail))
        {
            echo json_encode(array(	$detail->name));
        }
        else
        {
            abort(404);
        }
      }
    }

    public function edit($id)
    {
    }

    public function update(Request $request, $id)
    {
      DB::beginTransaction();
      if(DB::connection('mysql'))
      {
        if($request->deleteid)
        {
          DB::table('tbl_brand')->where('id',$id)->delete();
          DB::commit();
          return redirect('brand')->with('danger', 'Brand '.$request->brand.' has been deleted.');
        }
        else 
        {
          DB::table('tbl_brand')->where('id',$id)->update( array(  'name' => $request->brand ) );
          DB::commit();
          return redirect('brand')->with('success', 'Brand '.$request->brand.' has been changed successfully.');
        }
      }
      else
      {
        DB::rollback();
        return redirect('brand')->with('info', 'Brand '.$request->brand.' failed to change.');
      }
      return redirect('brand')->with('info', 'Brand '.$request->brand.' failed to change.');
    }

    public function create()
    {
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

    public function destroy($id)
    {
        //
    }

}
