<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Datatables;
use DB;

class MaterialController extends Controller
{
    protected function index(Request $request)
    {
      if(session('status')=='Admin')
      {
          $data['title'] = 'Material - UFF';
          $data['brand'] =  DB::table('tbl_brand')->get();
          $data['category'] =  DB::table('tbl_category')->get();
          $data['body'] = 'material';
          return view('layout',$data);
      }
      else
      {
          abort(404);
      }
    }

    public function store(Request $request)
    {
      if(DB::table('tbl_material')->where('code', $request->code)->first()) 
      {
        return redirect('material')->with('info', 'Sorry material code '.$request->code.' already exists.');
      }
      else
      {
        DB::table('tbl_material')->insert( array( 'code' => $request->code,
                                                  'name' => $request->name,
                                                  'brand_id' => $request->brand,
                                                  'category_id'=> $request->category,
                                                ) 
                                         );
        return redirect('material')->with('success', 'Material '.$request->name.' has been added successfully');
      } 
    }
    
    public function show($id='')
    {
      if($id=='json')
      {
        return Datatables::of(DB::table('tbl_material AS A')
                                  ->join("tbl_brand AS B", "A.brand_id", "=", "B.id")
                                  ->join("tbl_category AS C", "A.category_id", "=", "C.id")
                                  ->select("A.id","A.code", "A.name", "B.name AS 2", "C.name AS 3")
                                  ->get())->make();
      }
      else
      {
        $detail = DB::table('tbl_material')->where('id', $id)->get()->first();
        if(!empty($detail))
        {
            echo json_encode(array(	$detail->code,$detail->name,$detail->brand_id,$detail->category_id ));
        }
        else
        {
            abort(404);
        }
      }
    }

    public function edit($id)
    {
    }

    public function update(Request $request, $id)
    {
      DB::beginTransaction();
      if(DB::connection('mysql'))
      {
        if($request->deleteid)
        {
          DB::table('tbl_material')->where('id',$id)->delete();
          DB::commit();
          return redirect('material')->with('danger', 'Material '.$request->material.' has been deleted.');
        }
        else 
        {
          DB::table('tbl_material')->where('id',$id)->update( array(  'code' => $request->code,
                                                                      'name' => $request->name,
                                                                      'brand_id' => $request->brand,
                                                                      'category_id'=> $request->category ) );
          DB::commit();
          return redirect('material')->with('success', 'Material '.$request->name.' has been changed successfully.');
        }
      }
      else
      {
        DB::rollback();
        return redirect('material')->with('info', 'Material '.$request->name.' failed to change.');
      }
      return redirect('material')->with('info', 'Material '.$request->name.' failed to change.');
    }

    public function create()
    {
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

    public function destroy($id)
    {
        //
    }

}
