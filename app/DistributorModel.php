<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DistributorModel extends Model
{
    protected $table = 'master_distributor';
    protected $primaryKey = 'id';

    public $timestamps = false;

    protected $fillable = [
       'sap','name','address','bank_no','bank_name','city_id','area_id','status','date'
    ];

    public function area()
    {
        return $this->belongsTo('App\AreaModel');
    }

    public function outlet()
    {
        return $this->belongsToMany('App\AreaModel');
    }
}
