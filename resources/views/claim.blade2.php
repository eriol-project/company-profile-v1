  <script type="text/javascript">
    document.addEventListener('DOMContentLoaded', function() {
        $('.daterange-basic').daterangepicker({
            applyClass: 'bg-slate-600',
            cancelClass: 'btn-default',
            locale: {
                format: 'DD/MMM/YYYY'
            }
        });
        $('.pickadate').pickadate({
            format: 'dd/mmm/yyyy',
            selectYears: 4,
            selectMonths: true,
        });
        var oTable = $('.claim-table').DataTable(
        {
          processing: true,
          serverSide: true,
          autoWidth: false,
          responsive: true,
          // order: [[ 3, "desc" ]],
          columnDefs: [{ 
              orderable: false,
              width: '50px',
              targets: [ 0 ]
          }],
          dom: '<"datatable-header"fl><"datatable-scroll-wrap"t><"datatable-footer"ip>',
          language: {
              search: '<span>Filter:</span> _INPUT_',
              searchPlaceholder: 'Type to filter...',
              lengthMenu: '<span>Show:</span> _MENU_',
              paginate: { 'first': 'First', 'last': 'Last', 'next': $('html').attr('dir') == 'rtl' ? '&larr;' : '&rarr;', 'previous': $('html').attr('dir') == 'rtl' ? '&rarr;' : '&larr;' }
          },
          drawCallback: function () {
              $(this).find('tbody tr').slice(-3).find('.dropdown, .btn-group').addClass('dropup');
          },
          preDrawCallback: function() {
              $(this).find('tbody tr').slice(-3).find('.dropdown, .btn-group').removeClass('dropup');
          },
          "ajax": {
              url: '{{ url("claim-table") }}',
              data: function (d) {
                  d.area_id = $('#area option:selected').val();
                  d.distributor_id = $('#distributor option:selected').val();
                  d.status = $('#status').val();
              }
          },
          "createdRow": function( row, data, dataIndex ) {
              $(row).addClass( 'detail_row' );
              $(row).attr('data-id',data['id']);
          },
          "columns": [
            {"data": "id", className: "text-center", render: function (val, type, row){
                                  return "<ul class='icons-list'>"+
                                            "<li><a href='#' class='update_modal' data-id='"+val+"'><i class='icon-pencil7'></i></a></li>"+
                                            "</li>"+
                                          "</ul>";
                                  }
            },
            {"data": "no_pr"},
            {"data": "distributor_name"},
            {"data": "no_claim", className:"text-nowrap text-left", render: function (val, type, row){
                                  return val+(row['progress']>1?" <span class='badge badge-default'>"+row['progress']+"</span>":"");
                               }
            },
            {"data": "pending_type", className:"text-left", render: function (val, type, row){
                                    var values = "";
                                    $.each(val.split("\n"), function(i,e){
                                        values = values + "<span class='badge badge-default' style='margin-top: 2px;'>"+e+"</span> "
                                    });
                                    return values;
                               }
            },
            {"data": "remarks"},
            {"data": "periode_text", className:"text-nowrap"},
            {"data": "total_amount", className:"text-right", render: function (val, type, row){
                                  return parseInt(val).toLocaleString('en');
                               }
            },
            {"data": "status_name"}
          ]
        });

        @if(session('type')!='SUPER USER' && session('type')!='ACCOUNTING')
        var column = oTable.column( $(this).attr('id') );
        column.visible( ! column.visible() );
        @endif

        function change_date(date)
        {
          if (!date) {
              return "";
          }
          else
          {
            return moment(new Date(date)).format("DD/MMM/YYYY");
          }
        }

        // MODAL VIEW
        $(document).on("click", ".detail_row td:not(:first-child)", function () {
          var id = $(this).parent().data('id');
          $.ajax({
            type:"GET",
            url:"{{ url('claim-detail') }}/"+id,
            success:function(a)
            {              
              $('#modal_detail').modal('show');
              $('#modal_detail .modal-content').html(a);
              $('.claim-detail-table').DataTable({
                processing: true,
                serverSide: true,
                autoWidth: false,
                responsive: true,
                order: [[ 0, "desc" ]],
                dom: '<"datatable-header"fl><"datatable-scroll-wrap"t><"datatable-footer"ip>',
                language: {
                    search: '<span>Filter:</span> _INPUT_',
                    searchPlaceholder: 'Type to filter...',
                    lengthMenu: '<span>Show:</span> _MENU_',
                    paginate: { 'first': 'First', 'last': 'Last', 'next': $('html').attr('dir') == 'rtl' ? '&larr;' : '&rarr;', 'previous': $('html').attr('dir') == 'rtl' ? '&rarr;' : '&larr;' }
                },
                drawCallback: function () {
                    $(this).find('tbody tr').slice(-3).find('.dropdown, .btn-group').addClass('dropup');
                },
                preDrawCallback: function() {
                    $(this).find('tbody tr').slice(-3).find('.dropdown, .btn-group').removeClass('dropup');
                },
                "ajax": {
                    url: '{{ url("claim-detail-table") }}',
                    data: function (d) {
                        d.id = id;
                    }
                },
                "columns": [
                  {"data": "update_at", className:"text-nowrap"},
                  {"data": "update_by", className:"text-nowrap"},
                  {"data": "status_name", className:"text-nowrap"},
                  {"data": "pending_type", className:"text-left", render: function (val, type, row){
                                          var values = "";
                                          $.each(val.split("\n"), function(i,e){
                                              values = values + "<span class='badge badge-default' style='margin-top: 2px;'>"+e+"</span> "
                                          });
                                          return values;
                                     }
                  },
                  {"data": "remarks"},
                ]
              });
            }
          });
        });

        // MODAL EDIT
        $(document).on("click", ".update_modal", function () {
          var id = $(this).data('id');
          var action_url = "{{ url('claim')}}/"+id;
          $.ajax({
                  type:"GET",
                  url:"{{ url('claim') }}/"+id,
            dataType: 'json',
            success:function(a){
              $('#edit_form').attr('action', action_url);
              $('#edit_form input[name=admin_date_in]').val(change_date(a['admin_acc_date_in']));
              $('#edit_form input[name=admin_date_out]').val(change_date(a['admin_acc_date_out']));
              $('#edit_form input[name=spv_date_in]').val(change_date(a['spv_acc_date_in']));
              $('#edit_form input[name=spv_date_out]').val(change_date(a['spv_acc_date_out']));
              $('#edit_form input[name=sales_date_in]').val(change_date(a['dept_sales_date_in']));
              $('#edit_form input[name=sales_date_out]').val(change_date(a['dept_sales_date_out']));
              $('#edit_form input[name=ap_date_in]').val(change_date(a['ap_date_in']));
              $('#edit_form input[name=finance_date_in]').val(change_date(a['finance_date_in']));
              $('#edit_form input[name=complete_date]').val(change_date(a['complete_date']));
              $('#edit_form input[name=transfer_date]').val(change_date(a['transfer_date']));
              $('#edit_form input[name=return_date]').val(change_date(a['return_date']));
              $('#edit_form input[name=tax_expired_date]').val(change_date(a['tax_expired_date']));
              $('#edit_form input[name=no_pr]').val(a['no_pr']);
              $('#edit_form input[name=pic]').val(a['pic_name']);
              $('#edit_form select[name=distributor] option[value="'+a['distributor_id']+'"]').prop('selected', true);
              for (var i = 100; i > 1; i--) {
                 $('.claim_list_update .claim_list'+i).remove();
              }
              $('#edit_form input[name=no_claim1]').val(a['no_claim']);
              $('#edit_form input[name=description1]').val(a['description']);
              $('#edit_form input[name=period1]').val(a['periode_text']);
              $('#edit_form input[name=dpp1]').val(a['total_dpp']);
              var detail_id = a['detail_id'].split('**##');
              var no_claim = a['no_claim'].split('**##');
              var description = a['description'].split('**##');
              var periode_text = a['periode_text'].split('**##');
              var dpp = a['dpp'].split('**##');
              $('#edit_form input[name=detail_id1]').val(detail_id[0]);
              $('#edit_form input[name=no_claim1]').val(no_claim[0]);
              $('#edit_form input[name=description1]').val(description[0]);
              $('#edit_form input[name=period1]').val(periode_text[0]);
              $('#edit_form input[name=dpp_detail1]').val(dpp[0]);
              for(i=1;i<no_claim.length;i++){
                var x = i+1;
                var fieldSet = "<div class='row claim_list"+x+"'>"+
                                  "<div class='col-md-3 col-xs-3'>"+
                                    "<div class='form-group'>"+
                                      "<input name='detail_id"+x+"' type='hidden' value='"+detail_id[i]+"' required>"+
                                      "<input name='no_claim"+x+"' type='text' value='"+no_claim[i]+"' placeholder='No. Invoice' class='form-control' required>"+
                                    "</div>"+
                                  "</div>"+
                                  "<div class='col-md-3 col-xs-3'>"+
                                    "<div class='form-group'>"+
                                      "<input name='period"+x+"' type='text' value='"+periode_text[i]+"' class='form-control daterange-basic' value='{{date("d/M/Y - d/M/Y")}}' readonly required>"+
                                    "</div>"+
                                  "</div>"+
                                  "<div class='col-md-4 col-xs-4'>"+
                                    "<div class='form-group'>"+
                                      "<input name='description"+x+"' type='text' value='"+description[i]+"' placeholder='Description' class='form-control' required>"+
                                    "</div>"+
                                  "</div>"+
                                  "<div class='col-md-2 col-xs-2'>"+
                                    "<div class='form-group'>"+
                                      "<div class='input-group'>"+
                                        "<input name='dpp_detail"+x+"' id='dpp_detail_update"+x+"' type='text' value='"+dpp[i]+"' placeholder='DPP Amount' class='form-control dpp_detail' required oninput='amount_total_update()'>"+
                                        "<div class='input-group-btn'>"+
                                          "<button type='button' class='btn btn-danger btn-icon' onclick='delete_claim_update("+x+")'><i class='icon-minus3'></i></button>"+
                                        "</div>"+
                                      "</div>"+
                                    "</div>"+
                                  "</div>"+
                                "</div>";
                $(".claim_list_update").append(fieldSet);
              }
              $('#edit_form input[name=dpp]').val(a['total_dpp']);
              $('#edit_form select[name=ppn] option[value="'+a['ppn_id']+'"]').prop('selected', true);
              $('#edit_form select[name=pph] option[value="'+a['pph_id']+'"]').prop('selected', true);
              $('#edit_form input[name=amount]').val(a['total_amount']);
              var values = a['pending_type'];
              $.each(values.split("\n"), function(i,e){
                  $('#edit_form select[id=pending_type] option[value="'+e+'"]').prop('selected', true);
              });
              $('#edit_form input[name=pending_type]').val(a['pending_type']);
              $('#edit_form select[name=status] option[value="'+a['status']+'"]').prop('selected', true);
              $('#edit_form input[name=remarks]').val(a['remarks']);
              $('#edit_form textarea[name=remarks]').val(a['remarks']);
              $('.select_valid_update').select2();
              $('#modal_edit').modal('show');
            }
          });
        });

        $('#period').daterangepicker({
            applyClass: 'bg-slate-600',
            cancelClass: 'btn-default',
            locale: { format: 'DD/MMM/YYYY' }
        });
        // $('#period').on('apply.daterangepicker', function(ev, picker) {
        //   oTable.draw();
        //   ev.preventDefault();
        // });
        $('#status').on('change', function (e) {
          $(".export_excel").attr('href',"claim-export-excel/"+$('#distributor').val()+"/"+$('#status').val()+"/"+$('#area').val());
          $(".export_pdf").attr('href',"claim-export-pdf/"+$('#distributor').val()+"/"+$('#status').val()+"/"+$('#area').val());
          oTable.draw();
          e.preventDefault();
        });
        $('#distributor').on('change', function (e) {
          $(".export_excel").attr('href',"claim-export-excel/"+$('#distributor').val()+"/"+$('#status').val()+"/"+$('#area').val());
          $(".export_pdf").attr('href',"claim-export-pdf/"+$('#distributor').val()+"/"+$('#status').val()+"/"+$('#area').val());
          oTable.draw();
          e.preventDefault();
        });
        $('#area').on('change', function (e) {
          $(".export_excel").attr('href',"claim-export-excel/"+$('#distributor').val()+"/"+$('#status').val()+"/"+$('#area').val());
          $(".export_pdf").attr('href',"claim-export-pdf/"+$('#distributor').val()+"/"+$('#status').val()+"/"+$('#area').val());
          oTable.draw();
          e.preventDefault();
        });

				$("#add_form,#edit_form").each(function(){ 

          $(this).validate({
            ignore: 'input[type=hidden], .select2-search__field', // ignore hidden fields
            errorClass: 'validation-error-label',
            successClass: 'validation-valid-label',
            highlight: function(element, errorClass) {
                $(element).removeClass(errorClass);
            },
            unhighlight: function(element, errorClass) {
                $(element).removeClass(errorClass);
            },

            // Different components require proper error label placement
            errorPlacement: function(error, element) {

                // Styled checkboxes, radios, bootstrap switch
                if (element.parents('div').hasClass("checker") || element.parents('div').hasClass("choice") || element.parent().hasClass('bootstrap-switch-container') ) {
                    if(element.parents('label').hasClass('checkbox-inline') || element.parents('label').hasClass('radio-inline')) {
                        error.appendTo( element.parent().parent().parent().parent() );
                    }
                     else {
                        error.appendTo( element.parent().parent().parent().parent().parent() );
                    }
                }

                // Unstyled checkboxes, radios
                else if (element.parents('div').hasClass('checkbox') || element.parents('div').hasClass('radio')) {
                    error.appendTo( element.parent().parent().parent() );
                }

                // Input with icons and Select2
                else if (element.parents('div').hasClass('has-feedback') || element.hasClass('select2-hidden-accessible')) {
                    error.appendTo( element.parent() );
                }

                // Inline checkboxes, radios
                else if (element.parents('label').hasClass('checkbox-inline') || element.parents('label').hasClass('radio-inline')) {
                    error.appendTo( element.parent().parent() );
                }

                // Input group, styled file input
                else if (element.parent().hasClass('uploader') || element.parents().hasClass('input-group')) {
                    error.appendTo( element.parent().parent() );
                }

                else {
                    error.insertAfter(element);
                }
            },
            validClass: "validation-valid-label",
            rules: {
                no_pr: {
                    required: true
                },
                admin_date_in: {
                    required: true
                },
                pic: {
                    required: true
                },
                distributor: {
                    required: true
                },
                no_invoice: {
                    required: true
                },
                period: {
                    required: true
                },
                desciption: {
                    required: true
                },
                dpp_detail1: {number: true},
                dpp_detail2: {number: true},
                dpp_detail3: {number: true},
                dpp_detail4: {number: true},
                dpp_detail5: {number: true},
                dpp_detail6: {number: true},
                dpp_detail7: {number: true},
                dpp_detail8: {number: true},
                dpp_detail9: {number: true},
                dpp_detail20: {number: true},
                dpp_detail21: {number: true},
                dpp_detail22: {number: true},
                dpp_detail23: {number: true},
                dpp_detail24: {number: true},
                dpp_detail25: {number: true},
                dpp_detail26: {number: true},
                dpp_detail27: {number: true},
                dpp_detail28: {number: true},
                dpp_detail29: {number: true},
                dpp_detail20: {number: true},
                dpp_detail21: {number: true},
                dpp_detail22: {number: true},
                dpp_detail23: {number: true},
                dpp_detail24: {number: true},
                dpp_detail25: {number: true},
                dpp_detail26: {number: true},
                dpp_detail27: {number: true},
                dpp_detail28: {number: true},
                dpp_detail29: {number: true},
                dpp_detail30: {number: true},
                dpp_detail31: {number: true},
                dpp_detail32: {number: true},
                dpp_detail33: {number: true},
                dpp_detail34: {number: true},
                dpp_detail35: {number: true},
                dpp_detail36: {number: true},
                dpp_detail37: {number: true},
                dpp_detail38: {number: true},
                dpp_detail39: {number: true},
                dpp_detail40: {number: true},
                dpp_detail41: {number: true},
                dpp_detail42: {number: true},
                dpp_detail43: {number: true},
                dpp_detail44: {number: true},
                dpp_detail45: {number: true},
                dpp_detail46: {number: true},
                dpp_detail47: {number: true},
                dpp_detail48: {number: true},
                dpp_detail49: {number: true},
                dpp_detail50: {number: true},
                dpp_detail51: {number: true},
                dpp_detail52: {number: true},
                dpp_detail53: {number: true},
                dpp_detail54: {number: true},
                dpp_detail55: {number: true},
                dpp_detail56: {number: true},
                dpp_detail57: {number: true},
                dpp_detail58: {number: true},
                dpp_detail59: {number: true},
                dpp_detail60: {number: true},
                dpp_detail61: {number: true},
                dpp_detail62: {number: true},
                dpp_detail63: {number: true},
                dpp_detail64: {number: true},
                dpp_detail65: {number: true},
                dpp_detail66: {number: true},
                dpp_detail67: {number: true},
                dpp_detail68: {number: true},
                dpp_detail69: {number: true},
                dpp_detail70: {number: true},
                dpp_detail71: {number: true},
                dpp_detail72: {number: true},
                dpp_detail73: {number: true},
                dpp_detail74: {number: true},
                dpp_detail75: {number: true},
                dpp_detail76: {number: true},
                dpp_detail77: {number: true},
                dpp_detail78: {number: true},
                dpp_detail79: {number: true},
                dpp_detail80: {number: true},
                dpp_detail81: {number: true},
                dpp_detail82: {number: true},
                dpp_detail83: {number: true},
                dpp_detail84: {number: true},
                dpp_detail85: {number: true},
                dpp_detail86: {number: true},
                dpp_detail87: {number: true},
                dpp_detail88: {number: true},
                dpp_detail89: {number: true},
                dpp_detail90: {number: true},
                dpp_detail91: {number: true},
                dpp_detail92: {number: true},
                dpp_detail93: {number: true},
                dpp_detail94: {number: true},
                dpp_detail95: {number: true},
                dpp_detail96: {number: true},
                dpp_detail97: {number: true},
                dpp_detail98: {number: true},
                dpp_detail99: {number: true},
                dpp_detail100: {number: true},
                dpp: {
                    required: true,
                    number: true
                },
                ppn: {
                    required: true
                },
                pph: {
                    required: true
                },
                amount: {
                    required: true,
                    number: true
                },
                status: {
                    required: true
                },
                requirement: {
                    required: true
                }
            },
            messages: {
                // email: {
                //     required: 'Please insert your email'
                // }
            }
          });
        });
        var intVal = function ( i ) {
            return typeof i === 'string' ?
                i.replace(',', '').replace(/[^0-9.\d-]/g, '')*1 :
                typeof i === 'number' ?
                    i : 0;
        };
        $('.select_valid').on('change', function () {
          $(this).valid();
          var dpp = intVal($('#dpp').val());
          var ppn = dpp*intVal($('#ppn option:selected').data('percent'))/100;
          var pph = dpp*intVal($('#pph option:selected').data('percent'))/100;
          $('#amount').val((Math.round(dpp+ppn-pph)).toLocaleString('en')).valid();
        });
        $('.select_valid_update').on('change', function () {
          $(this).valid();
          var dpp = intVal($('#dpp_update').val());
          var ppn = dpp*intVal($('#ppn_update option:selected').data('percent'))/100;
          var pph = dpp*intVal($('#pph_update option:selected').data('percent'))/100;
          $('#amount_update').val((Math.round(dpp+ppn-pph)).toLocaleString('en')).valid();
        });
        $('#dpp').on('input', function () {
          $(this).valid();
          var dpp = intVal($('#dpp').val());
          var ppn = dpp*intVal($('#ppn option:selected').data('percent'))/100;
          var pph = dpp*intVal($('#pph option:selected').data('percent'))/100;
          $('#amount').val((Math.round(dpp+ppn-pph)).toLocaleString('en')).valid();
        });
        $('#dpp_update').on('input', function () {
          $(this).valid();
          var dpp = intVal($('#dpp_update').val());
          var ppn = dpp*intVal($('#ppn_update option:selected').data('percent'))/100;
          var pph = dpp*intVal($('#pph_update option:selected').data('percent'))/100;
          $('#amount_update').val((Math.round(dpp+ppn-pph)).toLocaleString('en')).valid();
        });
        $('.add_claim').on('click', function () {
          if ( $('#add_claim').css('display') == 'none' || $('#add_claim').css("visibility") == "hidden"){
            $('#add_claim').slideDown();
          }
          else
          {
            $('#add_claim').slideUp();
          }
        });

        $('#edit_form select[name=status]').on('change', function(){
          if($('#edit_form select[name=status]').val()==7)
            {
              $('.return_date').show();
              $('#edit_form input[name=return_date]').attr('required','');
            }
            else
            {
              $('.return_date').hide();
              $('#edit_form input[name=return_date]').removeAttr('required');
            }
        });
    });
    function amount_total_update()
    {
      var intVal = function ( i ) {
          return typeof i === 'string' ?
              i.replace(',', '').replace(/[^0-9.\d-]/g, '')*1 :
              typeof i === 'number' ?
                  i : 0;
      };
      var x = 0;
      for (var i = 100; i > 0; i--) { if($('#dpp_detail_update'+i).length) { x += intVal($('#dpp_detail_update'+i).val()); } }
      $('.total_dpp_update').val(x.toLocaleString('en')).trigger("input");
    }
    function amount_total_insert()
    {
      var intVal = function ( i ) {
          return typeof i === 'string' ?
              i.replace(',', '').replace(/[^0-9.\d-]/g, '')*1 :
              typeof i === 'number' ?
                  i : 0;
      };
      var x = 0;
      for (var i = 100; i > 0; i--) { if($('#dpp_detail_insert'+i).length) { x += intVal($('#dpp_detail_insert'+i).val()); } }
      $('.total_dpp_insert').val(x.toLocaleString('en')).trigger("input");
    }
    function add_claim_update()
    {
      var x = 2;
      for (var i = 100; i >= 2; i--) { if(!$('.claim_list'+i).length) { x = i; } }
      var fieldSet = "<div class='row claim_list"+x+"'>"+
                        "<div class='col-md-3 col-xs-3'>"+
                          "<div class='form-group'>"+
                            "<input name='no_claim"+x+"' type='text' placeholder='No. Invoice' class='form-control' required>"+
                          "</div>"+
                        "</div>"+
                        "<div class='col-md-3 col-xs-3'>"+
                          "<div class='form-group'>"+
                            "<input name='period"+x+"' type='text' class='form-control daterange-basic' value='{{date("d/M/Y - d/M/Y")}}' readonly required>"+
                          "</div>"+
                        "</div>"+
                        "<div class='col-md-4 col-xs-4'>"+
                          "<div class='form-group'>"+
                            "<input name='description"+x+"' type='text' placeholder='Description' class='form-control' required>"+
                          "</div>"+
                        "</div>"+
                        "<div class='col-md-2 col-xs-2'>"+
                          "<div class='form-group'>"+
                            "<div class='input-group'>"+
                              "<input name='dpp_detail"+x+"' id='dpp_detail_update"+x+"' type='text' placeholder='DPP Amount' class='form-control dpp_detail' required oninput='amount_total_update()'>"+
                              "<div class='input-group-btn'>"+
                                "<button type='button' class='btn btn-danger btn-icon' onclick='delete_claim_update("+x+")'><i class='icon-minus3'></i></button>"+
                              "</div>"+
                            "</div>"+
                          "</div>"+
                        "</div>"+
                      "</div>";
      $(".claim_list_update").append(fieldSet);
      $('.daterange-basic').daterangepicker({
          applyClass: 'bg-slate-600',
          cancelClass: 'btn-default',
          locale: {
              format: 'DD/MMM/YYYY'
          }
      });
    }
    function add_claim_insert()
    {
      var x = 2;
      for (var i = 100; i >= 2; i--) { if(!$('.claim_list'+i).length) { x = i; } }
      var fieldSet = "<div class='row claim_list"+x+"'>"+
                        "<div class='col-md-3 col-xs-3'>"+
                          "<div class='form-group'>"+
                            "<input name='no_claim"+x+"' type='text' placeholder='No. Invoice' class='form-control' required>"+
                          "</div>"+
                        "</div>"+
                        "<div class='col-md-3 col-xs-3'>"+
                          "<div class='form-group'>"+
                            "<input name='period"+x+"' type='text' class='form-control daterange-basic' value='{{date("d/M/Y - d/M/Y")}}' readonly required>"+
                          "</div>"+
                        "</div>"+
                        "<div class='col-md-4 col-xs-4'>"+
                          "<div class='form-group'>"+
                            "<input name='description"+x+"' type='text' placeholder='Description' class='form-control' required>"+
                          "</div>"+
                        "</div>"+
                        "<div class='col-md-2 col-xs-2'>"+
                          "<div class='form-group'>"+
                            "<div class='input-group'>"+
                              "<input name='dpp_detail"+x+"' id='dpp_detail_insert"+x+"' type='text' placeholder='DPP Amount' class='form-control dpp_detail' required oninput='amount_total_insert()'>"+
                              "<div class='input-group-btn'>"+
                                "<button type='button' class='btn btn-danger btn-icon' onclick='delete_claim_insert("+x+")'><i class='icon-minus3'></i></button>"+
                              "</div>"+
                            "</div>"+
                          "</div>"+
                        "</div>"+
                      "</div>";
      $(".claim_list_insert").append(fieldSet);
      $('.daterange-basic').daterangepicker({
          applyClass: 'bg-slate-600',
          cancelClass: 'btn-default',
          locale: {
              format: 'DD/MMM/YYYY'
          }
      });
    }
    function delete_claim_update(x)
    {
      $('.claim_list_update .claim_list'+x).remove();
      amount_total_update();
    }
    function delete_claim_insert(x)
    {
      $('.claim_list_insert .claim_list'+x).remove();
      amount_total_insert();
    }
  </script>

  @if(session('warning'))
      <script type="text/javascript">
          document.addEventListener('DOMContentLoaded', function() {
              new Noty({
                  theme: 'limitless',
                  layout: 'topCenter',
                  text: '{{session("warning")}}',
                  type: 'warning',
                  timeout: 2500
              }).show();
          });
      </script>
  @endif
  @if(session('success'))
      <script type="text/javascript">
          document.addEventListener('DOMContentLoaded', function() {
              new Noty({
                  theme: 'limitless',
                  layout: 'topCenter',
                  text: '{{session("success")}}',
                  type: 'success',
                  timeout: 2500
              }).show();
          });
      </script>
  @endif
  @if(session('info'))
      <script type="text/javascript">
          document.addEventListener('DOMContentLoaded', function() {
              new Noty({
                  theme: 'limitless',
                  layout: 'topCenter',
                  text: '{{session("info")}}',
                  type: 'info',
                  timeout: 2500
              }).show();
          });
      </script>
  @endif
  
  <!-- Page header -->
  <div class="page-header">
    <div class="page-header-content">
      <div class="page-title">
        <h4>
          <i class="icon-arrow-left52 position-left"></i>
          <span class="text-semibold">Claim</span>
        </h4>
      </div>
    </div>
  </div>
  <!-- /page header -->


  <!-- Page container -->
  <div class="page-container">

    <!-- Page content -->
    <div class="page-content">

      <!-- Main content -->
      <div class="content-wrapper">

        <!-- Detail modal -->
        <div id="modal_detail" class="modal fade" >
          <div class="modal-dialog modal-lg">
            <div class="modal-content">
            </div>
          </div>
        </div>
        <!-- /Detail modal -->

        @if(session('type')=='SUPER USER' || session('type')=='ACCOUNTING')
        <!-- Top right menu -->
        <ul class="fab-menu fab-menu-absolute fab-menu-top-right" data-fab-toggle="hover">
          <li class="add_claim">
            <a class="fab-menu-btn btn bg-blue btn-float btn-rounded btn-icon">
              <i class="fab-icon-open icon-plus3"></i>
              <i class="fab-icon-close icon-plus3"></i>
            </a>
          </li>
        </ul>
        <!-- /top right menu -->

        <!-- Update modal -->
        <div id="modal_edit" class="modal fade" >
          <div class="modal-dialog modal-lg">
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h5 class="modal-title">Update Claim</h5>
              </div>
              <legend></legend>
              <div class="modal-body">
                <form method="POST" id="edit_form">
                  @csrf
                  @method('PUT')
                  <fieldset>
                    <div class="row">
                      <div class="col-md-6">
                        <div class="form-group">
                          <label>Admin Accounting</label>
                          <div class="input-group">
                            <input name="admin_date_in" type="text" class="pickadate form-control" readonly required> 
                            <span class="input-group-addon"><i class="icon-chevron-right"></i></span>
                            <input name="admin_date_out" type="text" class="pickadate form-control" readonly> 
                          </div>
                        </div>
                      </div>
                      <div class="col-md-6">
                        <div class="form-group">
                          <label>Supervisor Accounting</label>
                          <div class="input-group">
                            <input name="spv_date_in" type="text" class="pickadate form-control" readonly> 
                            <span class="input-group-addon"><i class="icon-chevron-right"></i></span>
                            <input name="spv_date_out" type="text" class="pickadate form-control" readonly> 
                          </div>
                        </div>
                      </div>
                      <div class="col-md-6">
                        <div class="form-group">
                          <label>Department Sales</label>
                          <div class="input-group">
                            <input name="sales_date_in" type="text" class="pickadate form-control" readonly> 
                            <span class="input-group-addon"><i class="icon-chevron-right"></i></span>
                            <input name="sales_date_out" type="text" class="pickadate form-control" readonly> 
                          </div>
                        </div>
                      </div>
                      <div class="col-md-3 col-xs-6">
                        <div class="form-group">
                          <label>Account Payable</label>
                          <input name="ap_date_in" type="text" class="pickadate form-control" readonly> 
                        </div>
                      </div>
                      <div class="col-md-3 col-xs-6">
                        <div class="form-group">
                          <label>Finance</label>
                          <input name="finance_date_in" type="text" class="pickadate form-control" readonly> 
                        </div>
                      </div>
                      <div class="col-md-3 col-xs-6">
                        <div class="form-group">
                          <label>Complete</label>
                          <input name="complete_date" type="text" class="pickadate form-control" readonly> 
                        </div>
                      </div>
                      <div class="col-md-3 col-xs-6">
                        <div class="form-group">
                          <label>Transfer</label>
                          <input name="transfer_date" type="text" class="pickadate form-control" readonly> 
                        </div>
                      </div>
                      <div class="col-md-3 col-xs-6">
                        <div class="form-group">
                          <label>Expired Tax</label>
                          <input name="tax_expired_date" type="text" class="pickadate form-control" readonly> 
                        </div>
                      </div>
                      <div class="col-md-3 col-xs-6 return_date" style="display: none;">
                        <div class="form-group">
                          <label>Return</label>
                          <input name="return_date" type="text" class="pickadate form-control" readonly> 
                        </div>
                      </div>
                    </div>
                  </fieldset>
                  <legend></legend>
                  <fieldset>
                    <div class="row">
                      <div class="col-md-3">
                        <div class="form-group">
                          <label>No. PR</label>
                          <input name="no_pr" type="text" placeholder="No. PR" class="form-control" required>
                        </div>
                      </div>

                      <div class="col-md-3">
                        <div class="form-group">
                          <label>PIC</label>
                          <input name="pic" type="text" placeholder="PIC Name" class="form-control" style="text-transform:uppercase" required>
                        </div>
                      </div>

                      <div class="col-md-6">
                        <div class="form-group">
                          <label>Distributor</label>
                          <select name="distributor" data-placeholder="Select Distributor..." class="select-search select_valid_update" required>
                            <option></option>
                            @if(session('area_id')==0)
                              @foreach($distributor as $val)
                              <option value="{{$val->id}}">{{$val->name}}</option>
                              @endforeach
                            @else
                              @foreach($distributor as $val)
                                @if($val->area_id==session('area_id'))
                                  <option value="{{$val->id}}">{{$val->name}}</option>
                                @endif
                              @endforeach
                            @endif
                          </select>
                        </div>
                      </div>

                    </div>


                    <div class="claim_list_update">

                      <div class="row">

                        <div class="col-md-3 col-xs-3">
                          <div class="form-group">
                            <label>No. Claim</label>
                            <input name="detail_id1" type="hidden" required>
                            <input name="no_claim1" type="text" placeholder="No. Invoice" class="form-control" required>
                          </div>
                        </div>
                        
                        <div class="col-md-3 col-xs-3">
                          <div class="form-group">
                            <label>Period</label>
                            <input name="period1" type="text" class="form-control daterange-basic" value="{{date('d/M/Y - d/M/Y')}}" readonly required> 
                          </div>
                        </div>

                        <div class="col-md-4 col-xs-4">
                          <div class="form-group">
                            <label>Description</label>
                            <input name="description1" type="text" placeholder="Description" class="form-control" required>
                          </div>
                        </div>

                        <div class="col-md-2 col-xs-2">
                          <div class="form-group">
                            <label>Amount</label>
                            <div class="input-group">
                              <input name="dpp_detail1" id="dpp_detail_update1" type="text" placeholder="DPP Amount" class="form-control" required oninput="amount_total_update()">
                              <span class="input-group-btn">
                                <button type="button" class="btn btn-primary btn-icon" onclick="add_claim_update()"><i class="icon-plus3"></i></button>
                              </span>
                            </div>
                          </div>
                        </div>

                      </div>
                      
                    </div>

                    <div class="row">

                      <div class="col-md-2">
                        <div class="form-group">
                          <label>DPP</label>
                          <input name="dpp" id="dpp_update" type="text" placeholder="DPP Amount" class="form-control total_dpp_update" required>
                        </div>
                      </div>

                      <div class="col-md-2">
                        <div class="form-group">
                          <label>PPN</label>
                          <select name="ppn" id="ppn_update" data-placeholder="Select PPN..." class="select select_valid_update" required>
                            <option></option>
                            @foreach($ppn as $val)
                            <option value="{{$val->id}}" data-percent="{{$val->percent}}">{{$val->name}}</option>
                            @endforeach
                          </select>
                        </div>
                      </div>

                      <div class="col-md-2">
                        <div class="form-group">
                          <label>PPH</label>
                          <select name="pph" id="pph_update" data-placeholder="Select PPH..." class="select-search select_valid_update" required>
                            <option></option>
                            @foreach($pph as $val)
                            <option value="{{$val->id}}" data-percent="{{$val->percent}}">{{$val->name}}</option>
                            @endforeach
                          </select>
                        </div>
                      </div>

                      <div class="col-md-3">
                        <div class="form-group">
                          <label>Amount</label>
                          <input name="amount" id="amount_update" type="text" placeholder="Total Amount" class="form-control" required>
                        </div>
                      </div>

                      <div class="col-md-3">
                        <div class="form-group">
                          <label>Status</label>
                          <select name="status" data-placeholder="Select Status..." class="select-search select_valid_update" required>
                            <option></option>
                            @foreach($status as $val)
                            <option value="{{$val->id}}">{{$val->name}}</option>
                            @endforeach
                          </select>
                        </div>
                      </div>
                    </div>
                    <div class="row">
                      <div class="col-md-6">
                        <div class="form-group">
                          <label>Requirement</label>
                          <select name="pending_type[]" id="pending_type" data-placeholder="Select requirement..." class="select select2-hidden-accessible select_valid_update" multiple>
                            <option></option>
                            @foreach($pending_type as $val)
                            <option value="{{$val->name}}">{{$val->name}}</option>
                            @endforeach
                          </select>
                        </div>
                      </div>
                      <div class="col-md-6">
                        <div class="form-group">
                          <label>Remarks</label>
                          <textarea name="remarks" rows="3" cols="5" class="form-control" placeholder="Enter your message here"></textarea>
                        </div>
                      </div>
                    </div>
                  </fieldset>
                  <legend></legend>
                  <fieldset>
                    <div class="row">
                      <div class="col-md-12">
                        <div class="text-right">
                          <button type="submit" class="btn btn-primary">Submit form <i class="icon-arrow-right14 position-right"></i></button>
                        </div>
                      </div>
                    </div>
                  </fieldset>
                </form>
              </div>
            </div>
          </div>
        </div>
        <!-- /Update modal -->

        <!-- Insert modal -->
        <div class="row">
          <div class="col-lg-12" id="add_claim" style="display: none;">

            <div class="panel panel-flat">
              <div class="panel-body">

                <form action="{{ url('claim') }}" method="POST" id="add_form">
                @csrf

                  <fieldset>
                    <legend class="text-semibold"> Add Claim</legend>
                    <div class="row">

                      <div class="col-md-3">
                        <div class="form-group">
                          <label>No. PR</label>
                          <input name="no_pr" type="text" placeholder="No. PR" class="form-control" required>
                        </div>
                      </div>

                      <div class="col-md-3">
                        <div class="form-group">
                          <label>Admin Recieved</label>
                          <input name="admin_date_in" type="text" class="pickadate form-control" value="{{date('d/M/Y')}}" readonly required> 
                        </div>
                      </div>

                      <div class="col-md-3">
                        <div class="form-group">
                          <label>PIC</label>
                          <input name="pic" type="text" placeholder="PIC Name" class="form-control" style="text-transform:uppercase" required>
                        </div>
                      </div>

                      <div class="col-md-3">
                        <div class="form-group">
                          <label>Distributor</label>
                          <select name="distributor" data-placeholder="Select Distributor..." class="select-search select_valid" required>
                            <option></option>
                            @if(session('area_id')==0)
                              @foreach($distributor as $val)
                              <option value="{{$val->id}}">{{$val->name}}</option>
                              @endforeach
                            @else
                              @foreach($distributor as $val)
                                @if($val->area_id==session('area_id'))
                                  <option value="{{$val->id}}">{{$val->name}}</option>
                                @endif
                              @endforeach
                            @endif
                          </select>
                        </div>
                      </div>

                    </div>

                    <div class="claim_list_insert">

                      <div class="row">

                        <div class="col-md-3 col-xs-3">
                          <div class="form-group">
                            <label>No. Claim</label>
                            <input name="no_claim1" type="text" placeholder="No. Invoice" class="form-control" required>
                          </div>
                        </div>
                        
                        <div class="col-md-3 col-xs-3">
                          <div class="form-group">
                            <label>Period</label>
                            <input name="period1" type="text" class="form-control daterange-basic" value="{{date('d/M/Y - d/M/Y')}}" readonly required> 
                          </div>
                        </div>

                        <div class="col-md-4 col-xs-4">
                          <div class="form-group">
                            <label>Description</label>
                            <input name="description1" type="text" placeholder="Description" class="form-control" required>
                          </div>
                        </div>

                        <div class="col-md-2 col-xs-2">
                          <div class="form-group">
                            <label>Amount</label>
                            <div class="input-group">
                              <input name="dpp_detail1" id="dpp_detail_insert1" type="text" placeholder="DPP Amount" class="form-control" required oninput="amount_total_insert()">
                              <span class="input-group-btn">
                                <button type="button" class="btn btn-primary btn-icon" onclick="add_claim_insert()"><i class="icon-plus3"></i></button>
                              </span>
                            </div>
                          </div>
                        </div>

                      </div>
                      
                    </div>

                    <div class="row">

                      <div class="col-md-2">
                        <div class="form-group">
                          <label>DPP</label>
                          <input name="dpp" id="dpp" type="text" placeholder="DPP Amount" class="form-control total_dpp_insert" required>
                        </div>
                      </div>

                      <div class="col-md-2">
                        <div class="form-group">
                          <label>PPN</label>
                          <select name="ppn" id="ppn" data-placeholder="Select PPN..." class="select select_valid" required>
                            <option></option>
                            @foreach($ppn as $val)
                            <option value="{{$val->id}}" data-percent="{{$val->percent}}">{{$val->name}}</option>
                            @endforeach
                          </select>
                        </div>
                      </div>


                      <div class="col-md-2">
                        <div class="form-group">
                          <label>PPH</label>
                          <select name="pph" id="pph" data-placeholder="Select PPH..." class="select-search select_valid" required>
                            <option></option>
                            @foreach($pph as $val)
                            <option value="{{$val->id}}" data-percent="{{$val->percent}}">{{$val->name}}</option>
                            @endforeach
                          </select>
                        </div>
                      </div>

                      <div class="col-md-3">
                        <div class="form-group">
                          <label>Total Amount</label>
                          <input name="amount" id="amount" type="text" placeholder="Total Amount" class="form-control" required>
                        </div>
                      </div>

                      <div class="col-md-3">
                        <div class="form-group">
                          <label>Status</label>
                          <select name="status" data-placeholder="Select Status..." class="select-search select_valid" required>
                            <option></option>
                            @foreach($status as $val)
                            <option value="{{$val->id}}">{{$val->name}}</option>
                            @endforeach
                          </select>
                        </div>
                      </div>

                    </div>
                    <div class="row">

                      <div class="col-md-6">
                        <div class="form-group">
                          <label>Requirement</label>
                          <select name="pending_type[]" data-placeholder="Select requirement..." class="select select2-hidden-accessible select_valid" multiple>
                            <option></option>
                            @foreach($pending_type as $val)
                            <option value="{{$val->name}}">{{$val->name}}</option>
                            @endforeach
                          </select>
                        </div>
                      </div>
                      <div class="col-md-6">
                        <div class="form-group">
                          <label>Remarks</label>
                          <textarea name="remarks" rows="3" cols="5" class="form-control" placeholder="Enter your message here"></textarea>
                        </div>
                      </div>
                    </div>
                    <legend></legend>
                    <div class="text-right">
                      <button type="submit" class="btn btn-primary">Submit form <i class="icon-arrow-right14 position-right"></i></button>
                    </div>
                  </fieldset>
                </form>
              </div>
            </div>
          </div>
        </div>
        <!-- Insert modal -->
        @endif  

        <div class="row">
          <div class="col-lg-12">
            <div class="panel panel-flat">
              <div class="panel-body">
                <div class="col-lg-3">
                  <div class="form-group">
                    <label>Area</label>
                    <select id="area" data-placeholder="Select Area..." class="select-search">
                      <option></option>
                      <option value="ALL" selected>ALL</option>
                      @if(session('area_id')==0)
                        @foreach($area as $val)
                        <option value="{{$val->id}}">{{$val->name}}</option>
                        @endforeach
                      @else
                        @foreach($area as $val)
                          @if($val->area_id==session('area_id'))
                            <option value="{{$val->id}}">{{$val->name}}</option>
                          @endif
                        @endforeach
                      @endif
                    </select>
                  </div>
                </div>
                <div class="col-lg-3">
                  <div class="form-group">
                    <label>Distributor</label>
                    <select id="distributor" data-placeholder="Select Distributor..." class="select-search">
                      <option></option>
                      <option value="ALL" selected>ALL</option>
                      @if(session('area_id')==0)
                        @foreach($distributor as $val)
                        <option value="{{$val->id}}">{{$val->name}}</option>
                        @endforeach
                      @else
                        @foreach($distributor as $val)
                          @if($val->area_id==session('area_id'))
                            <option value="{{$val->id}}">{{$val->name}}</option>
                          @endif
                        @endforeach
                      @endif
                    </select>
                  </div>
                </div>
                <div class="col-lg-3">
                  <div class="form-group">
                    <label>Status</label>
                    <select id="status" data-placeholder="Select Status..." class="select-search">
                      <option></option>
                      <option value="ALL" selected>ALL</option>
                      @foreach($status as $val)
                      <option value="{{$val->id}}">{{$val->name}}</option>
                      @endforeach
                    </select>
                  </div>
                </div>
                <div class="col-lg-3">
                  <div class="form-group">
                    <a style="margin-top: 27px;" class="btn bg-teal-400 btn-labeled btn-md export_excel" href="{{ url('claim-export-excel/ALL/ALL/ALL') }}">
                      <b><i class="icon-file-excel"></i></b> Export Excel
                    </a>
                    <a style="margin-top: 27px;" class="btn bg-slate btn-labeled btn-md export_pdf" href="{{ url('claim-export-pdf/ALL/ALL/ALL') }}" target="_blank">
                      <b><i class="icon-file-pdf"></i></b> Export PDF
                    </a>
                  </div>
                </div>
                <div class="col-lg-1">
                  <div class="form-group">
                  </div>
                </div>
<?php 

?>
                <!-- <div class="col-lg-3">
                  <div class="form-group">
                    <label>Period</label>
                    <div class="input-group">
                      <span class="input-group-addon"><i class="icon-calendar22"></i></span>
                      <input id="period" type="text" class="form-control daterange-basic" value="{{date('01/M/Y - t/M/Y')}}" readonly> 
                    </div>
                  </div>
                </div> -->
              </div>
              <table class="table claim-table table-hover">
                <thead>
                  <tr>
                    <th></th>
                    <th>No.PR</th>
                    <th>Distributor</th>
                    <th>No.Claim</th>
                    <th>Requirement</th>
                    <th>Remarks</th>
                    <th>Period</th>
                    <th>Amount</th>
                    <th>Status</th>
                  </tr>
                </thead>
                <tbody>
                </tbody>
              </table>
            </div>
          </div>
        </div>
        <!-- /dashboard content -->
      </div>
      <!-- /main content -->
    </div>
    <!-- /page content -->
  </div>
  <!-- /page container -->