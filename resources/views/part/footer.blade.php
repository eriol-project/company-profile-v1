
                    <!-- BEGIN Page Content -->
                    <!-- this  is activated only when mobile menu is triggered -->
                    <div class="page-content-overlay" data-action="toggle" data-class="mobile-nav-on"></div> <!-- END Page Content -->
                    <!-- BEGIN Page Footer -->
                    <footer class="page-footer" role="contentinfo">
                        <div class="d-flex align-items-right flex-1 text-muted">
                            
                        </div>
                        <div>
                            <ul class="list-table m-0">
                                <li><span class="hidden-md-down fw-700 text-secondary">{{date('Y')}} © <a href='https://www.wipro-unza.com/indonesia-id/' class='text-primary fw-500' title='Wipro Unza' target='_blank'>Wipro Unza</a> | Claim Management System</span></li>
                            </ul>
                        </div>
                    </footer>
                    <!-- END Page Footer -->
                </div>
            </div>
        </div>
        <!-- END Page Wrapper -->
        <script src="js/vendors.bundle.js"></script>
        <script src="js/app.bundle.js"></script>
        <script src="js/formplugins/select2/select2.bundle.js"></script>
        <script src="js/dependency/moment/moment.js"></script>
        <script src="js/formplugins/bootstrap-daterangepicker/bootstrap-daterangepicker.js"></script>
        <script src="js/formplugins/bootstrap-datepicker/bootstrap-datepicker.js"></script>
        <script src="js/datagrid/datatables/datatables.bundle.js"></script>
</body>
</html>