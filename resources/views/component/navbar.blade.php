
  <!-- RESET MODAL -->
  <div id="modalchange" class="modal">
    <div class="modal-dialog" role="document">
      <div class="modal-content modal-content-demo">
        <div class="modal-header">
          <h6 class="modal-title">Change Password</h6>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <form class="row-fluid" method="POST" action="{{ url('editpassword') }}" >
        <div class="modal-body">
          @csrf
          <div class="form-group mg-b-10">
            <label class="form-label">New Password <span class="tx-danger">*</span></label>
            <input type="password" name="newpassword" class="form-control wd-450" placeholder="Enter new password" required>
          </div><!-- form-group -->
        </div>
        <div class="modal-footer">
          <button type="submit" class="btn btn-warning">Change Password</button>
        </div>
        </form>
      </div>
    </div><!-- modal-dialog -->
  </div><!-- modal -->

  <!-- Main navbar -->
  <div class="navbar navbar-inverse">
    <div class="navbar-header">
      <a class="navbar-brand" href="{{URL::to('/')}}"><img src="{{asset('img/dcms-mini.png')}}" alt="" style="margin-top: -16px;height: 51px;"></a>

      <ul class="nav navbar-nav pull-right visible-xs-block">
        <li><a data-toggle="collapse" data-target="#navbar-mobile"><i class="icon-tree5"></i></a></li>
      </ul>
    </div>

    <div class="navbar-collapse collapse" id="navbar-mobile">
      <ul class="nav navbar-nav navbar-right">
        <li class="dropdown dropdown-user">
          <a class="dropdown-toggle" data-toggle="dropdown">
            <img src="{{asset('global/images/placeholders/placeholder.jpg')}}" alt="">
            <span>{{session('name')}}</span>
            <i class="caret"></i>
          </a>

          <ul class="dropdown-menu dropdown-menu-right">
            <li><a href="#" data-toggle='modal' data-target='#modalchange'><i class="icon-lock"></i> Change Password</a></li>
            <li class="divider"></li>
            <li><a href="{{URL::to('logout')}}"><i class="icon-switch2"></i> Logout</a></li>
          </ul>
        </li>
      </ul>
    </div>
  </div>
  <!-- /main navbar -->

  <!-- Second navbar -->
  <div class="navbar navbar-default" id="navbar-second">
    <ul class="nav navbar-nav no-border visible-xs-block">
      <li><a class="text-center collapsed" data-toggle="collapse" data-target="#navbar-second-toggle"><i class="icon-menu7"></i></a></li>
    </ul>

    <div class="navbar-collapse collapse" id="navbar-second-toggle">
      <ul class="nav navbar-nav">
        <li class="{{$body=='dashboard'?'active':''}}"><a href="{{URL::to('/')}}"><i class="icon-display4 position-left"></i> Dashboard</a></li>
        <li class="{{$body=='claim'?'active':''}}"><a href="{{URL::to('/claim')}}"><i class="icon-cash3 position-left"></i> Claim</a></li>
        @if(session('type')=='SUPER USER' || session('type')=='ACCOUNTING')
        <li class="dropdown">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown">
            <i class="icon-browser position-left"></i> Data Master <span class="caret"></span>
          </a>
          <ul class="dropdown-menu width-200">
            @if(session('type')=='SUPER USER')
            <li class="dropdown-header">Basic</li>
            <li class="dropdown-submenu dropdown-submenu-hover">
              <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="icon-coin-dollar"></i> Tax</a>
              <ul class="dropdown-menu">
                <li class="{{$body=='ppn'?'active':''}}"><a href="{{URL::to('/ppn')}}">PPN</a></li>
                <li class="{{$body=='pph'?'active':''}}"><a href="{{URL::to('/pph')}}">PPH</a></li>
              </ul>
            </li>
            <li class="dropdown-submenu dropdown-submenu-hover">
              <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="icon-paragraph-justify3"></i> Text</a>
              <ul class="dropdown-menu">
                <li class="{{$body=='status'?'active':''}}"><a href="{{URL::to('/status')}}">Status Claim</a></li>
                <li class="{{$body=='pending'?'active':''}}"><a href="{{URL::to('/pending')}}">Pending Type</a></li>
              </ul>
            </li>
            <li class="dropdown-header">Advance</li>
            <li class="{{$body=='user'?'active':''}}"><a href="{{URL::to('/user')}}"><i class="icon-user"></i> User</a></li>
            @endif
            <li class="{{$body=='distributor'?'active':''}}"><a href="{{URL::to('/distributor')}}"><i class="icon-profile"></i> Distributor</a></li>
            <li class="{{$body=='area'?'active':''}}"><a href="{{URL::to('/area')}}"><i class="icon-map5"></i> Area</a></li>
          </ul>
        </li>
        @endif
      </ul>
    </div>

  </div>
  <!-- /second navbar -->