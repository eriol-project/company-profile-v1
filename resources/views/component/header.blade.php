<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>{{$title}}</title>

    <!-- FAVICONS -->
    <link rel="shortcut icon" href="{{asset('OLD/img/wipro.ico')}}" type="image/x-icon">
    <link rel="icon" href="{{asset('OLD/img/wipro.ico')}}" type="image/x-icon">

    <!-- Global stylesheets -->
    <link href="https://fonts.googleapis.com/css?family=Roboto:400,300,100,500,700,900" rel="stylesheet" type="text/css">
    <link href="{{asset('OLD/global/css/icons/icomoon/styles.css')}}" rel="stylesheet" type="text/css">
    <link href="{{asset('OLD/css/bootstrap.min.css')}}" rel="stylesheet" type="text/css">
    <link href="{{asset('OLD/css/core.min.css')}}" rel="stylesheet" type="text/css">
    <link href="{{asset('OLD/css/components.min.css')}}" rel="stylesheet" type="text/css">
    <link href="{{asset('OLD/css/colors.min.css')}}" rel="stylesheet" type="text/css">
    <!-- /global stylesheets -->

    <!-- Core JS files -->
    <script src="{{asset('OLD/global/js/plugins/loaders/pace.min.js')}}"></script>
    <script src="{{asset('OLD/global/js/core/libraries/jquery.min.js')}}"></script>
    <script src="{{asset('OLD/global/js/core/libraries/bootstrap.min.js')}}"></script>
    <script src="{{asset('OLD/global/js/plugins/loaders/blockui.min.js')}}"></script>
    <script src="{{asset('OLD/global/js/plugins/ui/nicescroll.min.js')}}"></script>
    <script src="{{asset('OLD/global/js/plugins/ui/drilldown.js')}}"></script>
    <!-- /core JS files -->

    <!-- Theme JS files -->
    <!-- <script src="{{asset('OLD/global/js/plugins/forms/selects/select2.min.js')}}"></script> -->
    <script src="{{asset('OLD/global/js/plugins/tables/datatables/datatables.min.js')}}"></script>
    <script src="{{asset('OLD/global/js/plugins/tables/datatables/extensions/responsive.min.js')}}"></script>
    <script src="{{asset('OLD/global/js/demo_pages/form_select2-new.js')}}"></script>
    <script src="{{asset('OLD/global/js/plugins/visualization/d3/d3.min.js')}}"></script>
    <script src="{{asset('OLD/global/js/plugins/visualization/d3/d3_tooltip.js')}}"></script>
    <script src="{{asset('OLD/global/js/plugins/forms/selects/bootstrap_multiselect.js')}}"></script>
    <script src="{{asset('OLD/global/js/plugins/ui/moment/moment.min.js')}}"></script>
    <script src="{{asset('OLD/global/js/plugins/ui/headroom/headroom.min.js')}}"></script>
    <script src="{{asset('OLD/global/js/plugins/ui/headroom/headroom_jquery.min.js')}}"></script>
    <script src="{{asset('OLD/global/js/demo_pages/layout_navbar_secondary_fixed.js')}}"></script>
    <script src="{{asset('OLD/global/js/plugins/notifications/noty.min.js')}}"></script>
    <script src="{{asset('OLD/global/js/plugins/notifications/jgrowl.min.js')}}"></script>
    <script src="{{asset('OLD/global/js/plugins/forms/validation/validate.min.js')}}"></script>
    <script src="{{asset('OLD/global/js/plugins/forms/inputs/touchspin.min.js')}}"></script>
    <script src="{{asset('OLD/global/js/plugins/forms/selects/select2.min.js')}}"></script>
    <script src="{{asset('OLD/global/js/plugins/forms/styling/switch.min.js')}}"></script>
    <script src="{{asset('OLD/global/js/plugins/forms/styling/switchery.min.js')}}"></script>
    <script src="{{asset('OLD/global/js/plugins/forms/styling/uniform.min.js')}}"></script>
    <script src="{{asset('OLD/global/js/plugins/pickers/daterangepicker.js')}}"></script>
    <script src="{{asset('OLD/global/js/plugins/pickers/pickadate/picker.js')}}"></script>
    <script src="{{asset('OLD/global/js/plugins/pickers/pickadate/picker.date.js')}}"></script>
    <!-- <script src="{{asset('OLD/global/js/plugins/pickers/pickadate/legacy.js')}}"></script> -->
    <!-- <script src="{{asset('OLD/global/js/demo_pages/datatables_responsive.js')}}"></script> -->
    <!-- <script src="{{asset('OLD/global/js/demo_pages/picker_date.js')}}"></script> -->
    <!-- <script src="{{asset('OLD/global/js/demo_pages/form_validation.js')}}"></script> -->
    <script src="{{asset('OLD/js/app.js')}}"></script>
    <!-- /theme JS files -->
    <style type="text/css">
    .picker{
        width: 300px;
    }
    .table-hover > tbody tr:hover td{
        background-color: #87ece354 !important;
        cursor:pointer;
    }
    </style>
</head>

<body>
