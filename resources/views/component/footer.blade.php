

    <!-- Footer -->
    <div class="footer text-muted text-center">
        &copy; {{date('Y')}} <a href="https://www.wipro-unza.com/indonesia-id/">Wipro Unza</a>
    </div>
    <!-- /footer -->

</body>
</html>