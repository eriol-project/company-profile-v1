
  <script>
    var classHolder = document.getElementsByTagName("BODY")[0],
        themeSettings = (localStorage.getItem('themeSettings')) ? JSON.parse(localStorage.getItem('themeSettings')) : {},
        themeURL = themeSettings.themeURL || '',
        themeOptions = themeSettings.themeOptions || '';
    if (themeSettings.themeOptions){classHolder.className = themeSettings.themeOptions;}
    if (themeSettings.themeURL && !document.getElementById('mytheme'))
    {
        var cssfile = document.createElement('link'); cssfile.id = 'mytheme'; cssfile.rel = 'stylesheet'; cssfile.href = themeURL; document.getElementsByTagName('head')[0].appendChild(cssfile);
    }
    var saveSettings = function()
    {
        themeSettings.themeOptions = String(classHolder.className).split(/[^\w-]+/).filter(function(item){ return /^(nav|header|mod|display)-/i.test(item);}).join(' ');
        if (document.getElementById('mytheme')){ themeSettings.themeURL = document.getElementById('mytheme').getAttribute("href"); };
        localStorage.setItem('themeSettings', JSON.stringify(themeSettings));
    }
    var resetSettings = function() { localStorage.setItem("themeSettings", ""); }
    
    document.addEventListener('DOMContentLoaded', function()
    {
      $('.select2').select2();
      
      $(".js-hide-search").select2({minimumResultsForSearch: 1 / 0 });

      function dashboard()
      {
        $.ajax({
          type:"GET",
          url:"{{ url('dashboard') }}",
          dataType: "json",
          data:{
            area : $('#area').val(),
            distributor : $('#distributor').val(),
            year : $('#year').val(),
            month : $('#month').val(),
            status : $('#status').val()
          },
          success: function(result) {
            $('#dashboard').html(result);
          }
        });
      }
      dashboard();
      $('select').on('change', function (e) {
        dashboard();
      });
    });
  </script>
  <main id="js-page-content" role="main" class="page-content">
      <ol class="breadcrumb page-breadcrumb">
          <li class="breadcrumb-item"><a href="javascript:void(0);">DCMS</a></li>
          <li class="breadcrumb-item">Dashboard</li>
      </ol>
      <div class="subheader">
          <h1 class="subheader-title">
              <i class='fal fa-home'></i> Dashboard
          </h1>
      </div>
      <div class="card border mb-4">
        <div class="card-body row mb-2">
          <div class="col-lg-3 col-md-4 col-ms-4 mb-3">
            <div class="form-group">
              <label class="form-label" for="single-default">Area</label>
              <select id="area" class="select2 form-control">
                @if(session('area_id')==0)
                  <option value="ALL" selected>ALL</option>
                  @foreach($area as $val)
                  <option value="{{$val->id}}">{{$val->name}}</option>
                  @endforeach
                @else
                  @foreach($area as $val)
                    @if($val->id==session('area_id'))
                    <option value="{{$val->id}}" selected>{{$val->name}}</option>
                    @endif
                  @endforeach
                @endif
              </select>
            </div>
          </div>
          <div class="col-lg-3 col-md-8 col-ms-8 mb-3">
            <div class="form-group">
              <label class="form-label" for="single-default">Distributor</label>
              <select id="distributor" class="select2 form-control">
                <option value="ALL" selected>ALL</option>
                @if(session('area_id')==0)
                  @foreach($distributor as $val)
                  <option value="{{$val->id}}">{{$val->name}}</option>
                  @endforeach
                @else
                  @foreach($distributor as $val)
                    @if($val->area_id==session('area_id'))
                      <option value="{{$val->id}}">{{$val->name}}</option>
                    @endif
                  @endforeach
                @endif
              </select>
            </div>
          </div>
          <div class="col-lg-2 col-md-6 col-ms-6 mb-3">
            <label class="form-label" for="single-default">Year</label>
              <div class="input-group">
                <select id="year" class="js-hide-search form-control">
                  @for($x=2018;$x<=date('Y');$x++)
                    <option value="{{$x}}" {{ $x==date('Y')?'selected':'' }}>{{$x}}</option>
                  @endfor
                </select>
            </div>
          </div>
          <div class="col-lg-2 col-md-6 col-ms-6 mb-3">
            <label class="form-label" for="single-default">Month</label>
              <div class="input-group">
                <select id="month" class="js-hide-search form-control">
                    <option value="01" {{ '01'==date('m')?'selected':'' }}>January</option>
                    <option value="02" {{ '02'==date('m')?'selected':'' }}>Febuary</option>
                    <option value="03" {{ '03'==date('m')?'selected':'' }}>March</option>
                    <option value="04" {{ '04'==date('m')?'selected':'' }}>April</option>
                    <option value="05" {{ '05'==date('m')?'selected':'' }}>May</option>
                    <option value="06" {{ '06'==date('m')?'selected':'' }}>June</option>
                    <option value="07" {{ '07'==date('m')?'selected':'' }}>July</option>
                    <option value="08" {{ '08'==date('m')?'selected':'' }}>August</option>
                    <option value="09" {{ '09'==date('m')?'selected':'' }}>September</option>
                    <option value="10" {{ '10'==date('m')?'selected':'' }}>October</option>
                    <option value="11" {{ '11'==date('m')?'selected':'' }}>November</option>
                    <option value="12" {{ '12'==date('m')?'selected':'' }}>December</option>
                </select>
            </div>
          </div>
          <div class="col-lg-2">
            <div class="form-group">
              <label class="form-label" for="single-default">Status</label>
              <select id="status" class="select2 form-control">
                <option></option>
                <option value="ALL" selected>ALL</option>
                @foreach($status as $val)
                <option value="{{$val->id}}">{{$val->name}}</option>
                @endforeach
              </select>
            </div>
          </div>
        </div>
      </div>
      <div class="row" id="dashboard">
      </div>
  </main>