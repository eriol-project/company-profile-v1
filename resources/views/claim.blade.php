  <script type="text/javascript">
    var classHolder = document.getElementsByTagName("BODY")[0],
        themeSettings = (localStorage.getItem('themeSettings')) ? JSON.parse(localStorage.getItem('themeSettings')) : {},
        themeURL = themeSettings.themeURL || '',
        themeOptions = themeSettings.themeOptions || '';
    if (themeSettings.themeOptions){classHolder.className = themeSettings.themeOptions;}
    if (themeSettings.themeURL && !document.getElementById('mytheme'))
    {
        var cssfile = document.createElement('link'); cssfile.id = 'mytheme'; cssfile.rel = 'stylesheet'; cssfile.href = themeURL; document.getElementsByTagName('head')[0].appendChild(cssfile);
    }
    var saveSettings = function()
    {
        themeSettings.themeOptions = String(classHolder.className).split(/[^\w-]+/).filter(function(item){ return /^(nav|header|mod|display)-/i.test(item);}).join(' ');
        if (document.getElementById('mytheme')){ themeSettings.themeURL = document.getElementById('mytheme').getAttribute("href"); };
        localStorage.setItem('themeSettings', JSON.stringify(themeSettings));
    }
    var resetSettings = function() { localStorage.setItem("themeSettings", ""); }

    var intVal = function ( i ) {
        return typeof i === 'string' ?
            i.replace(',', '').replace(/[^0-9.\d-]/g, '')*1 :
            typeof i === 'number' ?
                i : 0;
    };
    function amount_total(t)
    {
      var x = 0;
      for (var i = 100; i > 0; i--) { if($('input[name=dpp_detail_'+t+'_'+i+']').length) { x += intVal($('input[name=dpp_detail_'+t+'_'+i+']').val()); } }
      $('input[name=total_dpp_'+t+']').val(x.toLocaleString('en')).trigger("input");
    }
    function add_claim_insert()
    {
      var x = 2;
      var y = "'insert'";
      for (var i = 100; i >= 2; i--) { if(!$('.claim_list'+i).length) { x = i; } }
      var fieldSet = "<div class='form-row claim_list"+x+"'>"+
                        '<div class="col-xs-3 col-md-3 mb-2">'+
                          '<div class="input-group">'+
                              '<input type="text" name="no_claim'+x+'" class="form-control" placeholder="Claim No" required>'+
                          '</div>'+
                        '</div>'+
                        '<div class="col-xs-2 col-md-2 mb-2">'+
                          '<input type="text" name="period'+x+'" value="{{date('d/M/Y - d/M/Y')}}" class="form-control daterange-basic" placeholder="Select Period" required readonly>'+
                        '</div>'+
                        '<div class="col-xs-2 col-md-2 mb-2">'+
                          '<div class="input-group">'+
                              '<input type="text" name="dpp_detail_insert_'+x+'" oninput="amount_total('+y+')" class="form-control" placeholder="DPP Amount" required>'+
                          '</div>'+
                        '</div>'+
                        '<div class="col-xs-5 col-md-5 mb-2">'+
                          '<div class="input-group">'+
                              '<input type="text" name="description'+x+'" class="form-control" placeholder="Description" required>'+
                              '<div class="input-group-append">'+
                                  '<button class="btn btn-danger" type="button" onclick="delete_claim_insert('+x+')"><i class="fal fa-trash"></i></button>'+
                              '</div>'+
                          '</div>'+
                        '</div>'+
                      "</div>";
      $(".claim_list_insert").append(fieldSet);
      $('.daterange-basic').daterangepicker({
          applyClass: 'bg-slate-600',
          cancelClass: 'btn-default',
          locale: {
              format: 'DD/MMM/YYYY'
          }
      });
    }
    function delete_claim_insert(x)
    {
      $('.claim_list_insert .claim_list'+x).remove();
      amount_total('insert');
    }
    document.addEventListener('DOMContentLoaded', function()
    {
        (function()
        {
            'use strict';
            window.addEventListener('load', function()
            {
                // Fetch all the forms we want to apply custom Bootstrap validation styles to
                var forms = document.getElementsByClassName('needs-validation');
                // Loop over them and prevent submission
                var validation = Array.prototype.filter.call(forms, function(form)
                {
                    form.addEventListener('submit', function(event)
                    {
                        if (form.checkValidity() === false)
                        {
                            event.preventDefault();
                            event.stopPropagation();
                        }
                        form.classList.add('was-validated');
                    }, false);
                });
            }, false);
        })();
        $('.select2').select2({placeholder: "Please select"});
        $(".js-hide-search").select2({minimumResultsForSearch: 1 / 0,placeholder: "Please select"});
        $("#pr_no").change(function() { if(this.checked){$("input[name=pr_no]").removeAttr('readonly');}else{$("input[name=pr_no]").attr('readonly','');}});
        $(".form-control[name=category]").change(function(){
          $('.category-claim').hide();
          if($("#category").val()==1)
          {
            $('.category-distributor').show();
          }
          else
          {
            if($("#category").val()==2)
            {
              $('.category-outlet').show();
            }
            else
            {
              $('.category-personal').show();
            }
          }
          $(".form-control[name=distributor]").val('').change();
          $(".form-control[name=outlet]").val('').change();
          $(".form-control[name=personal]").val('').change();
        });
        $(".form-control[name=distributor]").change(function(){
          $("input[name=bank_no]").val($(".form-control[name=distributor]").find(":selected").data("no"));
          $("input[name=bank_name]").val($(".form-control[name=distributor]").find(":selected").data("name"));
        });
        $(".form-control[name=outlet]").change(function(){
          $("input[name=bank_no]").val($(".form-control[name=outlet]").find(":selected").data("no"));
          $("input[name=bank_name]").val($(".form-control[name=outlet]").find(":selected").data("name"));
        });
        $(".form-control[name=personal]").change(function(){
          $("input[name=bank_no]").val($(".form-control[name=personal]").find(":selected").data("no"));
          $("input[name=bank_name]").val($(".form-control[name=personal]").find(":selected").data("name"));
        });
        $('.daterange-basic').daterangepicker({ locale: { format: 'DD/MMM/YYYY' } });
        $('input[name=total_dpp_insert]').on('input', function () {
          var dpp = intVal($('input[name=total_dpp_insert]').val());
          var ppn = dpp*intVal($('.form-control[name=ppn_insert]').find(":selected").data('percent'))/100;
          var pph = dpp*intVal($('.form-control[name=pph_insert]').find(":selected").data('percent'))/100;
          $('input[name=total_amount_insert]').val((Math.round(dpp+ppn-pph)).toLocaleString('en'));
        });
        $('.form-control[name=ppn_insert],.form-control[name=pph_insert]').on('change', function () {
          $('input[name=total_dpp_insert]').trigger("input");
        });
        $('.form-control[name=ppn_update],.form-control[name=pph_update]').on('change', function () {
          $('input[name=total_dpp_update]').trigger("input");
        });
        $('.insert_select').on('change', function () {
          var dpp = intVal($('input[name=total_dpp_insert]').val());
          var ppn = dpp*intVal($('form-control[name=ppn]').find(":selected").data('percent'))/100;
          var pph = dpp*intVal($('form-control[name=pph]').find(":selected").data('percent'))/100;
          $('input[name=amount]').val((Math.round(dpp+ppn-pph)).toLocaleString('en'));
        });
        $('input[name=dpp]').on('input', function () {
          var dpp = intVal($('input[name=dpp]').val());
          var ppn = dpp*intVal($('form-control[name=ppn]').find(":selected").data('percent'))/100;
          var pph = dpp*intVal($('form-control[name=pph]').find(":selected").data('percent'))/100;
          $('input[name=amount]').val((Math.round(dpp+ppn-pph)).toLocaleString('en'));
        });
        var oTable = $('.claim-table').DataTable(
        {
          processing: true,
          serverSide: true,
          autoWidth: false,
          responsive: true,
          columnDefs: [{ 
              orderable: false,
              targets: [ 0 ]
          }],
          "order": [[1, 'asc']],
          drawCallback: function () {
              $(this).find('tbody tr').slice(-3).find('.dropdown, .btn-group').addClass('dropup');
          },
          preDrawCallback: function() {
              $(this).find('tbody tr').slice(-3).find('.dropdown, .btn-group').removeClass('dropup');
          },
          "ajax": {
              url: '{{ url("claim-table") }}',
              data: function (d) {
                  d.area_id = $('#area option:selected').val();
                  d.distributor_id = $('#distributor option:selected').val();
                  d.status = $('#status').val();
              }
          },
          "createdRow": function( row, data, dataIndex ) {
              $(row).addClass( 'detail_row' );
              $(row).attr('data-id',data['id']);
          },
          "columns": [
            {"data": "id", className: "text-center", render: function (val, type, row){
                                  return "<div class='dropdown d-inline-block'>"+
                                            "<a href='#' class='btn btn-sm btn-icon btn-outline-primary rounded-circle shadow-0' data-toggle='dropdown' aria-expanded='true' title='More options'>"+
                                              "<i class='fal fa-ellipsis-v'></i>"+
                                            "</a>"+
                                            "<div class='dropdown-menu' x-placement='bottom-start' style='position: absolute; will-change: top, left; top: 26px; left: -76px;'>"+
                                              "<a class='dropdown-item update_modal' href='javascript:void(0);' data-id='"+val+"'>Update</a>"+
                                              "<a class='dropdown-item update_modal' href='javascript:void(0);'>Generate Report</a>"+
                                            "</div>"+
                                          "</div>";
                                          
                                  }
            },
            {"data": "no_pr"},
            {"data": "distributor_name"},
            {"data": "no_claim", className:"text-nowrap text-left", render: function (val, type, row){
                                  return val+(row['progress']>1?" <span class='badge badge-default'>"+row['progress']+"</span>":"");
                               }
            },
            {"data": "status_name"},
            {"data": "remarks"},
            {"data": "periode_text", className:"text-nowrap"},
            {"data": "total_amount", className:"text-right", render: function (val, type, row){
                                  return parseInt(val).toLocaleString('en');
                               }
            },
            {"data": "status_name"}
          ]
        });

        @if(session('type')!='SUPER USER' && session('type')!='ACCOUNTING')
        var column = oTable.column( $(this).attr('id') );
        column.visible( ! column.visible() );
        @endif

        function change_date(date)
        {
          if (!date) {
              return "";
          }
          else
          {
            return moment(new Date(date)).format("DD/MMM/YYYY");
          }
        }

        // MODAL VIEW
        $(document).on("click", ".detail_row td:not(:first-child)", function () {
          var id = $(this).parent().data('id');
          $.ajax({
            type:"GET",
            url:"{{ url('claim-detail') }}/"+id,
            success:function(a)
            {              
              $('#modal_detail').modal('show');
              $('#modal_detail .modal-content').html(a);
              $('.claim-detail-table').DataTable({
                processing: true,
                serverSide: true,
                autoWidth: false,
                responsive: true,
                order: [[ 0, "desc" ]],
                dom: '<"datatable-header"fl><"datatable-scroll-wrap"t><"datatable-footer"ip>',
                language: {
                    search: '<span>Filter:</span> _INPUT_',
                    searchPlaceholder: 'Type to filter...',
                    lengthMenu: '<span>Show:</span> _MENU_',
                    paginate: { 'first': 'First', 'last': 'Last', 'next': $('html').attr('dir') == 'rtl' ? '&larr;' : '&rarr;', 'previous': $('html').attr('dir') == 'rtl' ? '&rarr;' : '&larr;' }
                },
                drawCallback: function () {
                    $(this).find('tbody tr').slice(-3).find('.dropdown, .btn-group').addClass('dropup');
                },
                preDrawCallback: function() {
                    $(this).find('tbody tr').slice(-3).find('.dropdown, .btn-group').removeClass('dropup');
                },
                "ajax": {
                    url: '{{ url("claim-detail-table") }}',
                    data: function (d) {
                        d.id = id;
                    }
                },
                "columns": [
                  {"data": "update_at", className:"text-nowrap"},
                  {"data": "update_by", className:"text-nowrap"},
                  {"data": "status_name", className:"text-nowrap"},
                  {"data": "pending_type", className:"text-left", render: function (val, type, row){
                                          var values = "";
                                          $.each(val.split("\n"), function(i,e){
                                              values = values + "<span class='badge badge-default' style='margin-top: 2px;'>"+e+"</span> "
                                          });
                                          return values;
                                     }
                  },
                  {"data": "remarks"},
                ]
              });
            }
          });
        });

        // MODAL EDIT
        $(document).on("click", ".update_modal", function () {
          var id = $(this).data('id');
          var action_url = "{{ url('claim')}}/"+id;
          $.ajax({
                  type:"GET",
                  url:"{{ url('claim') }}/"+id,
            dataType: 'json',
            success:function(a){
              $('#edit_form').attr('action', action_url);
              $('#edit_form input[name=admin_date_in]').val(change_date(a['admin_acc_date_in']));
              $('#edit_form input[name=admin_date_out]').val(change_date(a['admin_acc_date_out']));
              $('#edit_form input[name=spv_date_in]').val(change_date(a['spv_acc_date_in']));
              $('#edit_form input[name=spv_date_out]').val(change_date(a['spv_acc_date_out']));
              $('#edit_form input[name=sales_date_in]').val(change_date(a['dept_sales_date_in']));
              $('#edit_form input[name=sales_date_out]').val(change_date(a['dept_sales_date_out']));
              $('#edit_form input[name=ap_date_in]').val(change_date(a['ap_date_in']));
              $('#edit_form input[name=finance_date_in]').val(change_date(a['finance_date_in']));
              $('#edit_form input[name=complete_date]').val(change_date(a['complete_date']));
              $('#edit_form input[name=transfer_date]').val(change_date(a['transfer_date']));
              $('#edit_form input[name=return_date]').val(change_date(a['return_date']));
              $('#edit_form input[name=tax_expired_date]').val(change_date(a['tax_expired_date']));
              $('#edit_form input[name=no_pr]').val(a['no_pr']);
              $('#edit_form input[name=pic]').val(a['pic_name']);
              $('#edit_form select[name=distributor] option[value="'+a['distributor_id']+'"]').prop('selected', true);
              for (var i = 100; i > 1; i--) {
                 $('.claim_list_update .claim_list'+i).remove();
              }
              $('#edit_form input[name=no_claim1]').val(a['no_claim']);
              $('#edit_form input[name=description1]').val(a['description']);
              $('#edit_form input[name=period1]').val(a['periode_text']);
              $('#edit_form input[name=dpp1]').val(a['total_dpp']);
              var detail_id = a['detail_id'].split('**##');
              var no_claim = a['no_claim'].split('**##');
              var description = a['description'].split('**##');
              var periode_text = a['periode_text'].split('**##');
              var dpp = a['dpp'].split('**##');
              $('#edit_form input[name=detail_id1]').val(detail_id[0]);
              $('#edit_form input[name=no_claim1]').val(no_claim[0]);
              $('#edit_form input[name=description1]').val(description[0]);
              $('#edit_form input[name=period1]').val(periode_text[0]);
              $('#edit_form input[name=dpp_detail1]').val(dpp[0]);
              for(i=1;i<no_claim.length;i++){
                var x = i+1;
                var fieldSet = "<div class='row claim_list"+x+"'>"+
                                  "<div class='col-md-3 col-xs-3'>"+
                                    "<div class='form-group'>"+
                                      "<input name='detail_id"+x+"' type='hidden' value='"+detail_id[i]+"' required>"+
                                      "<input name='no_claim"+x+"' type='text' value='"+no_claim[i]+"' placeholder='No. Invoice' class='form-control' required>"+
                                    "</div>"+
                                  "</div>"+
                                  "<div class='col-md-3 col-xs-3'>"+
                                    "<div class='form-group'>"+
                                      "<input name='period"+x+"' type='text' value='"+periode_text[i]+"' class='form-control daterange-basic' value='{{date("d/M/Y - d/M/Y")}}' readonly required>"+
                                    "</div>"+
                                  "</div>"+
                                  "<div class='col-md-4 col-xs-4'>"+
                                    "<div class='form-group'>"+
                                      "<input name='description"+x+"' type='text' value='"+description[i]+"' placeholder='Description' class='form-control' required>"+
                                    "</div>"+
                                  "</div>"+
                                  "<div class='col-md-2 col-xs-2'>"+
                                    "<div class='form-group'>"+
                                      "<div class='input-group'>"+
                                        "<input name='dpp_detail"+x+"' id='dpp_detail_update"+x+"' type='text' value='"+dpp[i]+"' placeholder='DPP Amount' class='form-control dpp_detail' required oninput='amount_total_update()'>"+
                                        "<div class='input-group-btn'>"+
                                          "<button type='button' class='btn btn-danger btn-icon' onclick='delete_claim_update("+x+")'><i class='icon-minus3'></i></button>"+
                                        "</div>"+
                                      "</div>"+
                                    "</div>"+
                                  "</div>"+
                                "</div>";
                $(".claim_list_update").append(fieldSet);
              }
              $('#edit_form input[name=dpp]').val(a['total_dpp']);
              $('#edit_form select[name=ppn] option[value="'+a['ppn_id']+'"]').prop('selected', true);
              $('#edit_form select[name=pph] option[value="'+a['pph_id']+'"]').prop('selected', true);
              $('#edit_form input[name=amount]').val(a['total_amount']);
              var values = a['pending_type'];
              $.each(values.split("\n"), function(i,e){
                  $('#edit_form select[id=pending_type] option[value="'+e+'"]').prop('selected', true);
              });
              $('#edit_form input[name=pending_type]').val(a['pending_type']);
              $('#edit_form select[name=status] option[value="'+a['status']+'"]').prop('selected', true);
              $('#edit_form input[name=remarks]').val(a['remarks']);
              $('#edit_form textarea[name=remarks]').val(a['remarks']);
              $('.select_valid_update').select2();
              $('#modal_edit').modal('show');
            }
          });
        });
        $('#status').on('change', function (e) {
          $(".export_excel").attr('href',"claim-export-excel/"+$('#distributor').val()+"/"+$('#status').val()+"/"+$('#area').val());
          $(".export_pdf").attr('href',"claim-export-pdf/"+$('#distributor').val()+"/"+$('#status').val()+"/"+$('#area').val());
          oTable.draw();
          e.preventDefault();
        });
        $('#distributor').on('change', function (e) {
          $(".export_excel").attr('href',"claim-export-excel/"+$('#distributor').val()+"/"+$('#status').val()+"/"+$('#area').val());
          $(".export_pdf").attr('href',"claim-export-pdf/"+$('#distributor').val()+"/"+$('#status').val()+"/"+$('#area').val());
          oTable.draw();
          e.preventDefault();
        });
        $('#area').on('change', function (e) {
          $(".export_excel").attr('href',"claim-export-excel/"+$('#distributor').val()+"/"+$('#status').val()+"/"+$('#area').val());
          $(".export_pdf").attr('href',"claim-export-pdf/"+$('#distributor').val()+"/"+$('#status').val()+"/"+$('#area').val());
          oTable.draw();
          e.preventDefault();
        });
        $('.select_valid_update').on('change', function () {
          $(this).valid();
          var dpp = intVal($('#dpp_update').val());
          var ppn = dpp*intVal($('#ppn_update option:selected').data('percent'))/100;
          var pph = dpp*intVal($('#pph_update option:selected').data('percent'))/100;
          $('#amount_update').val((Math.round(dpp+ppn-pph)).toLocaleString('en')).valid();
        });
        $('#dpp_update').on('input', function () {
          $(this).valid();
          var dpp = intVal($('#dpp_update').val());
          var ppn = dpp*intVal($('#ppn_update option:selected').data('percent'))/100;
          var pph = dpp*intVal($('#pph_update option:selected').data('percent'))/100;
          $('#amount_update').val((Math.round(dpp+ppn-pph)).toLocaleString('en')).valid();
        });
    });
    function amount_total_update()
    {
      var intVal = function ( i ) {
          return typeof i === 'string' ?
              i.replace(',', '').replace(/[^0-9.\d-]/g, '')*1 :
              typeof i === 'number' ?
                  i : 0;
      };
      var x = 0;
      for (var i = 100; i > 0; i--) { if($('#dpp_detail_update'+i).length) { x += intVal($('#dpp_detail_update'+i).val()); } }
      $('.total_dpp_update').val(x.toLocaleString('en')).trigger("input");
    }
    function add_claim_update()
    {
      var x = 2;
      for (var i = 100; i >= 2; i--) { if(!$('.claim_list'+i).length) { x = i; } }
      var fieldSet = "<div class='row claim_list"+x+"'>"+
                        "<div class='col-md-3 col-xs-3'>"+
                          "<div class='form-group'>"+
                            "<input name='no_claim"+x+"' type='text' placeholder='No. Invoice' class='form-control' required>"+
                          "</div>"+
                        "</div>"+
                        "<div class='col-md-3 col-xs-3'>"+
                          "<div class='form-group'>"+
                            "<input name='period"+x+"' type='text' class='form-control daterange-basic' value='{{date("d/M/Y - d/M/Y")}}' readonly required>"+
                          "</div>"+
                        "</div>"+
                        "<div class='col-md-4 col-xs-4'>"+
                          "<div class='form-group'>"+
                            "<input name='description"+x+"' type='text' placeholder='Description' class='form-control' required>"+
                          "</div>"+
                        "</div>"+
                        "<div class='col-md-2 col-xs-2'>"+
                          "<div class='form-group'>"+
                            "<div class='input-group'>"+
                              "<input name='dpp_detail"+x+"' id='dpp_detail_update"+x+"' type='text' placeholder='DPP Amount' class='form-control dpp_detail' required oninput='amount_total_update()'>"+
                              "<div class='input-group-btn'>"+
                                "<button type='button' class='btn btn-danger btn-icon' onclick='delete_claim_update("+x+")'><i class='icon-minus3'></i></button>"+
                              "</div>"+
                            "</div>"+
                          "</div>"+
                        "</div>"+
                      "</div>";
      $(".claim_list_update").append(fieldSet);
      $('.daterange-basic').daterangepicker({
          applyClass: 'bg-slate-600',
          cancelClass: 'btn-default',
          locale: {
              format: 'DD/MMM/YYYY'
          }
      });
    }
    function delete_claim_update(x)
    {
      $('.claim_list_update .claim_list'+x).remove();
      amount_total_update();
    }
  </script>

  @if(session('warning'))
      <script type="text/javascript">
          document.addEventListener('DOMContentLoaded', function() {
              new Noty({
                  theme: 'limitless',
                  layout: 'topCenter',
                  text: '{{session("warning")}}',
                  type: 'warning',
                  timeout: 2500
              }).show();
          });
      </script>
  @endif
  @if(session('success'))
      <script type="text/javascript">
          document.addEventListener('DOMContentLoaded', function() {
              new Noty({
                  theme: 'limitless',
                  layout: 'topCenter',
                  text: '{{session("success")}}',
                  type: 'success',
                  timeout: 2500
              }).show();
          });
      </script>
  @endif
  @if(session('info'))
      <script type="text/javascript">
          document.addEventListener('DOMContentLoaded', function() {
              new Noty({
                  theme: 'limitless',
                  layout: 'topCenter',
                  text: '{{session("info")}}',
                  type: 'info',
                  timeout: 2500
              }).show();
          });
      </script>
  @endif
  
  <main id="js-page-content" role="main" class="page-content">
      <ol class="breadcrumb page-breadcrumb">
          <li class="breadcrumb-item"><a href="javascript:void(0);">DCMS</a></li>
          <li class="breadcrumb-item">Claim</li>
      </ol>
      <div class="subheader">
          <h1 class="subheader-title">
              <i class='fal fa-file-alt'></i> Claim
          </h1>
      </div>
      <div class="panel panel-collapsed">
        <div class="panel-hdr">
            <h2>
                Add <span class="fw-300"><i>Claim</i></span>
            </h2>
            <div class="panel-toolbar">
                <button class="btn btn-panel" data-action="panel-collapse" data-toggle="tooltip" data-offset="0,10" data-original-title="Collapse"></button>
            </div>
        </div>
        <div class="panel-container collapse">
            <div class="panel-content p-0">
                <form action="{{ url('claim') }}" method="POST" id="add_form" class="needs-validation" novalidate>
                @csrf
                    <div class="panel-content">
                        <div class="form-row">
                            <div class="col-xs-3 col-md-3 mb-3">
                              <label class="form-label" for="checkbox-group-4">PR No</label>
                              <div class="input-group">
                                  <div class="input-group-append">
                                      <div class="input-group-text" data-toggle="popover" 
                                                                    data-trigger="hover" 
                                                                    data-placement="top" 
                                                                    data-content="Check to enter the old payment number, or uncheck for generated by system." 
                                                                    data-original-title="Payment Request">
                                          <div class="custom-control d-flex custom-checkbox" style="margin-right: -9px;">
                                            <input type="checkbox" id="pr_no" class="custom-control-input" id="checkbox-group-4">
                                            <label class="custom-control-label fw-500" for="pr_no"></label>
                                          </div>
                                      </div>
                                  </div>
                                  <input type="text" class="form-control" name="pr_no" placeholder="PR No" readonly required>
                              </div>
                            </div>
                            <div class="col-xs-6 col-md-6 mb-3">
                              <label class="form-label" for="single-default">Claim Type</label>
                              <div class="input-group">
                                <select name="tu[e" class="custom-select select2 form-control" required>
                                    <option></option>
                                    @foreach($type as $val)
                                      <option value="{{$val->id}}">{{$val->name}}</option>
                                    @endforeach
                                </select>
                              </div>
                            </div>
                            <div class="col-xs-3 col-md-3 mb-3">
                              <label class="form-label" for="single-default">Claim For</label>
                              <div class="input-group">
                                <select name="category" id="category" class="custom-select js-hide-search form-control">
                                    <option value="1" selected>Distributor</option>
                                    <option value="2">Outlet</option>
                                    <option value="3">Operational</option>
                                </select>
                              </div>
                            </div>
                            <div class="col-xs-6 col-md-6 mb-3">
                              <div class="category-claim category-distributor">
                                <label class="form-label" for="single-default">Distributor</label>
                                <div class="input-group">
                                  <select name="distributor" class="custom-select select2 form-control" required>
                                    <option></option>
                                    @if(session('area_id')==0)
                                      @foreach($distributor as $val)
                                      <option value="{{$val->id}}" data-no="{{$val->bank_no}}" data-name="{{$val->bank_name}}">{{$val->name}}</option>
                                      @endforeach
                                    @else
                                      @foreach($distributor as $val)
                                        @if($val->area_id==session('area_id'))
                                          <option value="{{$val->id}}" data-no="{{$val->bank_no}}" data-name="{{$val->bank_name}}">{{$val->name}}</option>
                                        @endif
                                      @endforeach
                                    @endif
                                  </select>
                                </div>
                              </div>
                              <div class="category-claim category-outlet"  style="display: none;">
                                <label class="form-label" for="single-default">Outlet</label>
                                <div class="input-group">
                                  <select name="outlet" class="custom-select select2 form-control">
                                    <option></option>
                                    @if(session('distributor_id')==0)
                                      @foreach($outlet as $val)
                                      <option value="{{$val->id}}" data-no="{{$val->bank_no}}" data-name="{{$val->bank_name}}">{{$val->name}}</option>
                                      @endforeach
                                    @else
                                      @foreach($outlet as $val)
                                        @if($val->distributor_id==session('distributor_id'))
                                          <option value="{{$val->id}}" data-no="{{$val->bank_no}}" data-name="{{$val->bank_name}}">{{$val->name}}</option>
                                        @endif
                                      @endforeach
                                    @endif
                                  </select>
                                </div>
                              </div>
                              <div class="category-claim category-personal"  style="display: none;">
                                <label class="form-label" for="single-default">Operational</label>
                                <div class="input-group">
                                  <select name="personal" class="custom-select select2 form-control">
                                    <option></option>
                                    @foreach($operational as $val)
                                    <option value="{{$val->id}}" data-no="{{$val->bank_no}}" data-name="{{$val->bank_name}}">{{$val->name}}</option>
                                    @endforeach
                                  </select>
                                </div>
                              </div>
                            </div>
                            <div class="col-xs-3 col-md-3 mb-3">
                                <label class="form-label">Bank No</label>
                                <input type="text" class="form-control" name="bank_no" placeholder="Bank No" required>
                            </div>
                            <div class="col-xs-3 col-md-3 mb-3">
                                <label class="form-label">Bank Name</label>
                                <input type="text" class="form-control" name="bank_name" placeholder="Bank Name" required>
                            </div>
                        </div>
                    </div>
                    <div class="panel-content border-faded border-left-0 border-right-0 border-bottom-0 claim_list_insert">
                      <div class="form-row">
                        <div class="col-xs-3 col-md-3 mb-2">
                          <label class="form-label">Claim No</label>
                          <div class="input-group">
                              <input type="text" name="no_claim1" class="form-control" placeholder="Claim No" required>
                          </div>
                        </div>
                        <div class="col-xs-2 col-md-2 mb-2">
                          <label class="form-label">Period</label>
                          <input type="text" name="period1" class="form-control daterange-basic" placeholder="Select Period" required readonly>
                        </div>
                        <div class="col-xs-2 col-md-2 mb-2">
                          <label class="form-label">DPP Amount</label>
                          <div class="input-group">
                              <input type="text" name="dpp_detail_insert_1" oninput="amount_total('insert')" class="form-control" placeholder="DPP Amount" required>
                          </div>
                        </div>
                        <div class="col-xs-5 col-md-5 mb-2">
                          <label class="form-label">Description</label>
                          <div class="input-group">
                              <input type="text" name="description1" class="form-control" placeholder="Description" required>
                              <div class="input-group-append">
                                  <button class="btn btn-primary" type="button" onclick="add_claim_insert()"><i class="fal fa-plus"></i></button>
                              </div>
                          </div>
                        </div>
                      </div>
                    </div>
                    <div class="panel-content border-faded border-left-0 border-right-0 border-bottom-0">
                      <div class="form-row">
                        <div class="col-xs-3 col-md-3 mb-3">
                          <label class="form-label" for="single-default">PPN</label>
                          <select name="ppn_insert" data-placeholder="Select PPN..." class="custom-select js-hide-search form-control insert_select" required>
                            <option></option>
                            @foreach($ppn as $val)
                            <option value="{{$val->id}}" data-percent="{{$val->percent}}">{{$val->name}}</option>
                            @endforeach
                          </select>
                        </div>
                        <div class="col-xs-3 col-md-3 mb-3">
                          <label class="form-label" for="single-default">PPH</label>
                          <select name="pph_insert" data-placeholder="Select PPH..." class="custom-select select2 form-control insert_select" required>
                            <option></option>
                            @foreach($pph as $val)
                            <option value="{{$val->id}}" data-percent="{{$val->percent}}">{{$val->name}}</option>
                            @endforeach
                          </select>
                        </div>
                        <div class="col-xs-3 col-md-3 mb-3">
                          <label class="form-label">Total DPP</label>
                          <div class="input-group">
                              <input type="text" name="total_dpp_insert" class="form-control" placeholder="DPP Amount" required>
                          </div>
                        </div>
                        <div class="col-xs-3 col-md-3 mb-3">
                          <label class="form-label">Total Amount</label>
                          <div class="input-group">
                              <input type="text" name="total_amount_insert" class="form-control" placeholder="Total Amount" required>
                          </div>
                        </div>
                      </div>
                    </div>
                    <div class="panel-content border-faded border-left-0 border-right-0 border-bottom-0">
                      <div class="form-row">
                      </div>
                    </div>
                    <div class="panel-content border-faded border-left-0 border-right-0 border-bottom-0 d-flex flex-row">
                        <button class="btn btn-primary ml-auto" type="submit">Submit form</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <div class="panel">
        <div class="panel-container show">
            <div class="panel-content">
              <div class="form-row">
                <div class="col-xs-2 col-md-2 mb-2">
                  <label class="form-label" for="single-default">Area</label>
                  <select id="area" data-placeholder="Select Area..." class="custom-select js-hide-search form-control ">
                    <option></option>
                    <option value="ALL" selected>ALL</option>
                    @if(session('area_id')==0)
                      @foreach($area as $val)
                      <option value="{{$val->id}}">{{$val->name}}</option>
                      @endforeach
                    @else
                      @foreach($area as $val)
                        @if($val->area_id==session('area_id'))
                          <option value="{{$val->id}}">{{$val->name}}</option>
                        @endif
                      @endforeach
                    @endif
                  </select>
                </div>
                <div class="col-xs-2 col-md-2 mb-2">
                  <label class="form-label" for="single-default">Distributor</label>
                  <select id="distributor" data-placeholder="Select Distributor..." class="custom-select select2 form-control">
                    <option></option>
                    <option value="ALL" selected>ALL</option>
                    @if(session('area_id')==0)
                      @foreach($distributor as $val)
                      <option value="{{$val->id}}">{{$val->name}}</option>
                      @endforeach
                    @else
                      @foreach($distributor as $val)
                        @if($val->area_id==session('area_id'))
                          <option value="{{$val->id}}">{{$val->name}}</option>
                        @endif
                      @endforeach
                    @endif
                  </select>
                </div>
                <div class="col-xs-2 col-md-2 mb-2">
                  <label class="form-label" for="single-default">Status</label>
                  <select id="status" data-placeholder="Select Status..." class="custom-select select2 form-control">
                    <option></option>
                    <option value="ALL" selected>ALL</option>
                    @foreach($status as $val)
                    <option value="{{$val->id}}">{{$val->name}}</option>
                    @endforeach
                  </select>
                </div>
                <div class="col-xs-4 col-md-4 mb-4">
                  <div class="btn-group">
                    <a style="margin-top: 24px;" href="{{ url('claim-export-excel/ALL/ALL/ALL') }}" class="btn btn-success">
                        <span class="fal fa-file-excel mr-1"></span>Export Excel
                    </a>
                    <a style="margin-top: 24px;" href="{{ url('claim-export-pdf/ALL/ALL/ALL') }}" class="btn btn-secondary">
                        <span class="fal fa-file-pdf mr-1"></span>Export PDF
                    </a>
                  </div>
                </div>
              </div>
            </div>
            <div class="panel-content border-faded border-left-0 border-right-0 border-bottom-0">
                <!-- datatable start -->
                <table id="dt-basic-example" class="table claim-table table-bordered table-hover table-striped w-100">
                    <thead>
                        <tr>
                          <th></th>
                          <th>No.PR</th>
                          <th>Distributor</th>
                          <th>No.Claim</th>
                          <th>Requirement</th>
                          <th>Remarks</th>
                          <th>Period</th>
                          <th>Amount</th>
                          <th>Status</th>
                        </tr>
                    </thead>
                    <tbody>
                    </tbody>
                    <tfoot>
                        <tr>
                          <th></th>
                          <th>No.PR</th>
                          <th>Distributor</th>
                          <th>No.Claim</th>
                          <th>Requirement</th>
                          <th>Remarks</th>
                          <th>Period</th>
                          <th>Amount</th>
                          <th>Status</th>
                        </tr>
                    </tfoot>
                </table>
                <!-- datatable end -->
            </div>
        </div>
    </div>
  </main>