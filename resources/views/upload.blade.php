    <div class="az-content">
      <div class="container">
        <div class="az-content az-content-dashboard-four">
          <div class="media media-dashboard">
            <div class="media-body">
              <div class="az-content-header">
                <div>
                  <h6 class="az-content-title tx-18 mg-b-5">Data Upload</h6>
                  <p class="az-content-text tx-13 mg-b-0">Update data monthly</p>
                </div>
                <div class="az-content-header-right">
                  <div class="media">
                    <div class="media-body">
                      <label>Current data</label>
                    </div><!-- media-body -->
                  </div><!-- media -->
                  <div class="media">
                    <div class="media-body">
                      <label>From Date</label>
                      <h6>{{date('d M Y',strtotime($current->min))}}</h6>
                    </div><!-- media-body -->
                  </div><!-- media -->
                  <div class="media">
                    <div class="media-body">
                      <label>To Date</label>
                      <h6>{{date('d M Y',strtotime($current->max))}}</h6>
                    </div><!-- media-body -->
                  </div><!-- media -->
                </div>
              </div><!-- az-content-header -->

              <div class="card card-dashboard-twelve mg-b-20">
                <form class="row-fluid" method="POST" action="{{ url('file-upload') }}" enctype="multipart/form-data" data-parsley-validate>
                @csrf
                <div class="card-header">
                  <div class="">
                    <div class="row row-sm">
                      <!-- NOTIFICATION -->
                      @if(session('success'))
                        <div class="col-sm-12">
                          <div class="alert alert-outline-success" role="alert" style="">
                              <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                              <span aria-hidden="true">×</span>
                              </button>
                              <strong>Success</strong> {!!session('success')!!}
                          </div> 
                        </div>
                      @endif
                      @if(session('info'))
                        <div class="col-sm-12">
                          <div class="alert alert-outline-info" role="alert" style="">
                              <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                              <span aria-hidden="true">×</span>
                              </button>
                              <strong>Info</strong> {!!session('info')!!}
                          </div> 
                        </div>
                      @endif
                      @if($process)
                      <div class="col-sm-12">
                        <h6 class="az-content-title tx-18 mg-b-5"><span id="progressed"></span>
                        {{ $process->file_status=='PENDING'?'Waiting for process '.$process->file_name.' data...':'Process '.$process->file_name.' data...' }}
                        </h6>
                      </div>
                      <div class="col-sm-12 mg-t-20 mg-b-10">
                        <div class="progress mg-b-10">
                          @if($process->file_status=='PROCESS' && $process->file_process_status==100)
                          <div class="progress-bar progress-bar-lg progressed" style="width:{{$process->file_process_status}}%;">Waiting for calculate...</div>
                          @else
                          <div class="progress-bar progress-bar-lg progressing" style="width:{{$process->file_process_status}}%;">{{$process->file_process_status}}%</div>
                          @endif
                        </div>
                      </div>
                      <div class="col-sm-12">
                        @if($process->file_status=='PROCESS' && $process->file_process_status==100)
                        <a href="#" class="btn btn-danger btn-block disabled">Cancel</a>
                        @else
                        <a href="{{url('cancel-process')}}" class="btn btn-danger btn-block">Cancel</a>
                        @endif
                      </div>
                      @else
                      <div class="col-sm-12 col-md-6 col-lg-4">
                      <label>File Upload</label>
                        <div class="custom-file">
                          <input type="file" class="custom-file-input" name="file" id="customFile" required accept=".xls,.xlsx,.csv">
                          <label class="custom-file-label" for="customFile" id="customFileplaceholder">Choose file</label>
                        </div>
                      </div><!-- col -->
                      <div class="col-sm-12 col-md-6 col-lg-3">
                        <p class="mg-b-10">Year</p>
                        <select name="year" class="form-control select2-no-search">
                          @foreach ($year as $val)
                          <option value="{{ $val }}" {{ $val==date('Y')?'selected':'' }}>{{ $val }}</option>
                          @endforeach
                        </select>
                      </div>
                      <div class="col-sm-12 col-md-6 col-lg-3">
                        <p class="mg-b-10">Month</p>
                        <select name="month" class="form-control select2-no-search">
                          <option value="01" {{ date('m')=='1'?'selected':'' }}>January</option>
                          <option value="02" {{ date('m')=='2'?'selected':'' }}>Febuary</option>
                          <option value="03" {{ date('m')=='3'?'selected':'' }}>March</option>
                          <option value="04" {{ date('m')=='4'?'selected':'' }}>April</option>
                          <option value="05" {{ date('m')=='5'?'selected':'' }}>May</option>
                          <option value="06" {{ date('m')=='6'?'selected':'' }}>June</option>
                          <option value="07" {{ date('m')=='7'?'selected':'' }}>July</option>
                          <option value="08" {{ date('m')=='8'?'selected':'' }}>August</option>
                          <option value="09" {{ date('m')=='9'?'selected':'' }}>September</option>
                          <option value="10" {{ date('m')=='10'?'selected':'' }}>October</option>
                          <option value="11" {{ date('m')=='11'?'selected':'' }}>November</option>
                          <option value="12" {{ date('m')=='12'?'selected':'' }}>December</option>
                        </select>
                      </div>
                      <div class="col-sm-12 col-md-6 col-lg-2">
                        <p class="mg-b-10">Upload type</p>
                        <select id="type" name="type" class="form-control select2-no-search" required>
                          <option value="Overwrite Data" selected>Overwrite Data</option>
                          <option value="Add Data">Add Data</option>
                        </select>
                      </div>
                      <div class="col-sm-12 mg-t-20">
                        <input type="submit" name="upload" value="Upload now" class="btn btn-az-primary btn-block" />
                      </div>
                      <div class="col-sm-12 mg-t-20 mg-b-20 progress-div" style="display:none;">
                        <div class="progress mg-b-10">
                          <div class="progress-bar progress-bar-lg" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100">0%</div>
                        </div>
                      </div>
                      @endif
                    </div><!-- row -->
                  </div>
                </div><!-- card-header -->
                
                <div class="card-body">
                  <div class="table-responsive">
                    <table class="table table-hover mg-b-0">
                      <thead>
                        <tr>
                          <th>Name File</th>
                          <th>Type</th>
                          <th>Size</th>
                          <th>Monthly</th>
                          <th>Upload Date</th>
                          <th>Status</th>
                        </tr>
                      </thead>
                      <tbody>
                        @foreach ($upload as $val)
                        <tr>
                          <th class="log-data" data-toggle='modal' data-target='#logmodal' data-id='{{$val->id}}'><a href="#">{{$val->file_name}}</a></th>
                          <td>{{$val->file_type}}</td>
                          <td>{{number_format(($val->file_size/1024000),2)}} MB</td>
                          <td>{{substr($val->file_date,0,7)}}</td>
                          <td>{{$val->upload_date}}</td>
                          <td>{{$val->file_status}}</td>
                        </tr>
                        @endforeach
                        <tr>
                          <th>...</th>
                          <th>...</th>
                          <th>...</th>
                          <th>...</th>
                          <th>...</th>
                          <th>...</th>
                        </tr>
                      </tbody>
                    </table>
                  </div>
                </div><!-- card-body -->
                </form>
              </div><!-- card -->

            </div><!-- media-body -->

          </div><!-- media -->

        </div><!-- az-content -->
      </div>
    </div><!-- az-content -->

    
    <!-- LOG MODAL -->
    <div id="logmodal" class="modal">
      <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content modal-content-demo">
          <div class="modal-header">
            <h6 class="modal-title">Log Detail</h6>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div class="modal-body">
            <pre id="logpre">
            </pre>
          </div>
        </div>
      </div><!-- modal-dialog -->
    </div><!-- modal -->