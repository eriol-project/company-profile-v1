  <script type="text/javascript">
    document.addEventListener('DOMContentLoaded', function() {
        var oTable = $('.table').DataTable(
        {
          processing: true,
          serverSide: true,
          autoWidth: false,
          responsive: true,
          // order: [[ 3, "desc" ]],
          columnDefs: [{ 
              orderable: false,
              width: '50px',
              targets: [ 0 ]
          }],
          dom: '<"datatable-header"fl><"datatable-scroll-wrap"t><"datatable-footer"ip>',
          language: {
              search: '<span>Filter:</span> _INPUT_',
              searchPlaceholder: 'Type to filter...',
              lengthMenu: '<span>Show:</span> _MENU_',
              paginate: { 'first': 'First', 'last': 'Last', 'next': $('html').attr('dir') == 'rtl' ? '&larr;' : '&rarr;', 'previous': $('html').attr('dir') == 'rtl' ? '&rarr;' : '&larr;' }
          },
          drawCallback: function () {
              $(this).find('tbody tr').slice(-3).find('.dropdown, .btn-group').addClass('dropup');
          },
          preDrawCallback: function() {
              $(this).find('tbody tr').slice(-3).find('.dropdown, .btn-group').removeClass('dropup');
          },
          "ajax": {
              url: '{{ url("user/json") }}',
              data: function (d) {
                  d.distributor_id = $('#distributor option:selected').val();
                  d.period = $('#period').val();
              }
          },
          "columns": [
            {"data": "0", className: "text-center", render: function (val, type, row){
                                  return "<ul class='icons-list'>"+
                                            "<li class='dropdown'><a href='#' class='dropdown-toggle' data-toggle='dropdown'><i class='icon-menu9'></i></a>"+
                                              "<ul class='dropdown-menu dropdown-menu-left'>"+
                                                "<li><a href='#' class='insert_modal' data-id='"+val+"'><i class='icon-file-check'></i> Edit User</a></li>"+
                                                "<li><a href='#' class='reset_modal' data-id='"+val+"'><i class='icon-file-eye'></i> Reset Password</a></li>"+
                                              "</ul>"+
                                            "</li>"+
                                          "</ul>";
                                  }
            },
            {"data": "1"},
            {"data": "2"},
            {"data": "4"},
            {"data": "5"},
            {"data": "7", className: "text-center", render: function (val, type, row){ return !val?'ALL':val; }},
          ]
        });
        
        $('.select_valid').on('change', function () {
          $(this).valid();
        });

        // MODAL ADD
        $(document).on("click", ".update_modal", function () {
            $('#modal_add').modal('show');
        });
        
        // MODAL EDIT
        $(document).on("click", ".insert_modal", function () {
          var id = $(this).data('id');
          var action_url = "{{ url('user')}}/"+id;
          $.ajax({
                  type:"GET",
                  url:"{{ url('user') }}/"+id,
            dataType: 'json',
            success:function(a){
              $('#edit_form').attr('action', action_url);
              $('#edit_form input[name=name]').val(a['name']);
              $('#edit_form input[name=email]').val(a['email']);
              $('#edit_form select[name=type] option[value="'+a['type']+'"]').prop('selected', true);
              $('#edit_form select[name=status] option[value="'+a['status']+'"]').prop('selected', true);
              $('#edit_form select[name=area] option[value="'+a['area_id']+'"]').prop('selected', true);
              $('.select_valid').select2();
              $('#modal_edit').modal('show');
            }
          });
        });

        // MODAL RESET
        $(document).on("click", ".reset_modal", function () {
          var id = $(this).data('id');
          var action_url = "{{ url('user')}}/"+id;
          $.ajax({
                  type:"GET",
                  url:"{{ url('user') }}/"+id,
            dataType: 'json',
            success:function(a){
              $('#reset_form').attr('action', action_url);
              $('#reset_form .desc').html("Reset password "+a['name']);
              $('#modal_reset').modal('show');
            }
          });
        });

        $("#add_form,#edit_form").each(function(){ 

          $(this).validate({
            ignore: 'input[type=hidden], .select2-search__field', // ignore hidden fields
            errorClass: 'validation-error-label',
            successClass: 'validation-valid-label',
            highlight: function(element, errorClass) {
                $(element).removeClass(errorClass);
            },
            unhighlight: function(element, errorClass) {
                $(element).removeClass(errorClass);
            },

            // Different components require proper error label placement
            errorPlacement: function(error, element) {

                // Styled checkboxes, radios, bootstrap switch
                if (element.parents('div').hasClass("checker") || element.parents('div').hasClass("choice") || element.parent().hasClass('bootstrap-switch-container') ) {
                    if(element.parents('label').hasClass('checkbox-inline') || element.parents('label').hasClass('radio-inline')) {
                        error.appendTo( element.parent().parent().parent().parent() );
                    }
                     else {
                        error.appendTo( element.parent().parent().parent().parent().parent() );
                    }
                }

                // Unstyled checkboxes, radios
                else if (element.parents('div').hasClass('checkbox') || element.parents('div').hasClass('radio')) {
                    error.appendTo( element.parent().parent().parent() );
                }

                // Input with icons and Select2
                else if (element.parents('div').hasClass('has-feedback') || element.hasClass('select2-hidden-accessible')) {
                    error.appendTo( element.parent() );
                }

                // Inline checkboxes, radios
                else if (element.parents('label').hasClass('checkbox-inline') || element.parents('label').hasClass('radio-inline')) {
                    error.appendTo( element.parent().parent() );
                }

                // Input group, styled file input
                else if (element.parent().hasClass('uploader') || element.parents().hasClass('input-group')) {
                    error.appendTo( element.parent().parent() );
                }

                else {
                    error.insertAfter(element);
                }
            },
            validClass: "validation-valid-label",
            rules: {
                name: {
                    required: true
                },
                email: {
                    required: true,
                    email: true
                },
                type: {
                    required: true
                },
                area: {
                    required: true
                }
            }
          });
        });
    });
  </script>

  @if(session('warning'))
      <script type="text/javascript">
          document.addEventListener('DOMContentLoaded', function() {
              new Noty({
                  theme: 'limitless',
                  layout: 'topCenter',
                  text: '{{session("warning")}}',
                  type: 'warning',
                  timeout: 2500
              }).show();
          });
      </script>
  @endif
  @if(session('success'))
      <script type="text/javascript">
          document.addEventListener('DOMContentLoaded', function() {
              new Noty({
                  theme: 'limitless',
                  layout: 'topCenter',
                  text: '{{session("success")}}',
                  type: 'success',
                  timeout: 2500
              }).show();
          });
      </script>
  @endif
  @if(session('info'))
      <script type="text/javascript">
          document.addEventListener('DOMContentLoaded', function() {
              new Noty({
                  theme: 'limitless',
                  layout: 'topCenter',
                  text: '{{session("info")}}',
                  type: 'info',
                  timeout: 2500
              }).show();
          });
      </script>
  @endif


  <!-- Page header -->
  <div class="page-header">
    <div class="page-header-content">
      <div class="page-title">
        <h4>
          <i class="icon-arrow-left52 position-left"></i>
          <span class="text-semibold">User</span>
        </h4>
      </div>
    </div>
  </div>
  <!-- /page header -->


  <!-- Page container -->
  <div class="page-container">

    <!-- Page content -->
    <div class="page-content">

      <!-- Main content -->
      <div class="content-wrapper">

        <!-- Top right menu -->
        <ul class="fab-menu fab-menu-absolute fab-menu-top-right update_modal" data-fab-toggle="hover" id="fab-menu-affixed-demo-right">
          <li>
            <a class="fab-menu-btn btn bg-blue btn-float btn-rounded btn-icon">
              <i class="fab-icon-open icon-plus3"></i>
              <i class="fab-icon-close icon-plus3"></i>
            </a>
          </li>
        </ul>
        <!-- /top right menu -->

        <!-- Insert modal -->
        <div id="modal_add" class="modal fade" >
          <div class="modal-dialog modal-sm">
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h5 class="modal-title">Add Form</h5>
              </div>
              <legend></legend>
              <div class="modal-body">
                <form action="{{ url('user') }}" method="POST" id="add_form">
                  @csrf
                  <fieldset>
                    <div class="row">
                      <div class="col-md-12">
                        <div class="form-group">
                          <label>Name</label>
                          <input name="name" type="text" placeholder="Name" class="form-control" required>
                        </div>
                      </div>
                      <div class="col-md-12">
                        <div class="form-group">
                          <label>Email</label>
                          <input name="email" type="text" placeholder="Email" class="form-control" required>
                        </div>
                      </div>
                      <div class="col-md-12">
                        <div class="form-group">
                          <label>Area</label>
                          <select name="area" data-placeholder="Select Area..." class="select-search select_valid" required>
                            <option></option>
                            <option value="0">ALL</option>
                            @foreach($area as $val)
                            <option value="{{$val->id}}">{{$val->name}}</option>
                            @endforeach
                          </select>
                        </div>
                      </div>
                      <div class="col-xs-6">
                        <div class="form-group">
                          <label>Type</label>
                          <select name="type" data-placeholder="Select type..." class="select-search select_valid" required>
                            <option></option>
                            <option value="ADMIN">ADMIN</option>
                            <option value="ADMIN AREA">ADMIN AREA</option>
                            <option value="ACCOUNTING">ACCOUNTING</option>
                            <option value="SUPER USER">SUPER USER</option>
                          </select>
                        </div>
                      </div>
                      <div class="col-xs-6">
                        <div class="form-group">
                          <label>Status</label>
                          <select name="status" data-placeholder="Select status..." class="select select_valid" required>
                            <option value="ACTIVE" selected>ACTIVE</option>
                            <option value="NOT ACTIVE">NOT ACTIVE</option>
                          </select>
                        </div>
                      </div>
                    </div>
                  </fieldset>
                  <legend></legend>
                  <fieldset>
                    <div class="row">
                      <div class="col-md-12">
                        <div class="text-right">
                          <button type="submit" class="btn btn-primary">Submit form <i class="icon-arrow-right14 position-right"></i></button>
                        </div>
                      </div>
                    </div>
                  </fieldset>
                </form>
              </div>
            </div>
          </div>
        </div>
        <!-- /Insert modal -->

        <!-- Update modal -->
        <div id="modal_edit" class="modal fade" >
          <div class="modal-dialog modal-sm">
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h5 class="modal-title">Update Form</h5>
              </div>
              <legend></legend>
              <div class="modal-body">
                <form method="POST" id="edit_form">
                  @csrf
                  @method('PUT')
                  <fieldset>
                    <div class="row">
                      <div class="col-md-12">
                        <div class="form-group">
                          <label>Name</label>
                          <input name="name" type="text" placeholder="Name" class="form-control" required>
                        </div>
                      </div>
                      <div class="col-md-12">
                        <div class="form-group">
                          <label>Email</label>
                          <input name="email" type="text" placeholder="Email" class="form-control" required>
                        </div>
                      </div>
                      <div class="col-md-12">
                        <div class="form-group">
                          <label>Area</label>
                          <select name="area" data-placeholder="Select Area..." class="select-search select_valid" required>
                            <option></option>
                            <option value="0">ALL</option>
                            @foreach($area as $val)
                            <option value="{{$val->id}}">{{$val->name}}</option>
                            @endforeach
                          </select>
                        </div>
                      </div>
                      <div class="col-xs-6">
                        <div class="form-group">
                          <label>Type</label>
                          <select name="type" data-placeholder="Select type..." class="select-search select_valid" required>
                            <option></option>
                            <option value="ADMIN">ADMIN</option>
                            <option value="ADMIN AREA">ADMIN AREA</option>
                            <option value="ACCOUNTING">ACCOUNTING</option>
                            <option value="SUPER USER">SUPER USER</option>
                          </select>
                        </div>
                      </div>
                      <div class="col-xs-6">
                        <div class="form-group">
                          <label>Status</label>
                          <select name="status" data-placeholder="Select status..." class="select select_valid" required>
                            <option value="ACTIVE" selected>ACTIVE</option>
                            <option value="NOT ACTIVE">NOT ACTIVE</option>
                          </select>
                        </div>
                      </div>
                    </div>
                  </fieldset>
                  <legend></legend>
                  <fieldset>
                    <div class="row">
                      <div class="col-md-12">
                        <div class="text-right">
                          <button type="submit" class="btn btn-primary">Submit form <i class="icon-arrow-right14 position-right"></i></button>
                        </div>
                      </div>
                    </div>
                  </fieldset>
                </form>
              </div>
            </div>
          </div>
        </div>
        <!-- /Update modal -->

        <!-- Reset modal -->
        <div id="modal_reset" class="modal fade" >
          <div class="modal-dialog modal-sm">
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h5 class="modal-title">Reset Form</h5>
              </div>
              <legend></legend>
              <div class="modal-body">
                <form method="POST" id="reset_form">
                  @csrf
                  @method('PUT')
                  <center>
                    <h5 class="desc"></h5>
                  </center>
                  <legend></legend>
                  <fieldset>
                    <div class="row">
                      <div class="col-md-12">
                        <div class="text-right">
                          <input type="hidden" name="resetid" value="-">
                          <button type="submit" class="btn btn-primary">Submit form <i class="icon-arrow-right14 position-right"></i></button>
                        </div>
                      </div>
                    </div>
                  </fieldset>
                </form>
              </div>
            </div>
          </div>
        </div>
        <!-- /Reset modal -->

        <div class="row">
          <div class="col-lg-12">
            <div class="panel panel-flat">
              <table class="table datatable-responsive">
                <thead>
                  <tr>
                    <th></th>
                    <th>Name</th>
                    <th>Email</th>
                    <th>Type</th>
                    <th>Status</th>
                    <th>Area</th>
                  </tr>
                </thead>
                <tbody>
                </tbody>
              </table>
            </div>
          </div>
        </div>
        <!-- /dashboard content -->
      </div>
      <!-- /main content -->
    </div>
    <!-- /page content -->
  </div>
  <!-- /page container -->