<!DOCTYPE html>
<html lang="en">
@include('component.header')
<body class="az-light az-body">
@include('component.navbar')
@include($body)
@include('component.footer')
</body>
</html>
