<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <title>
            DCMS | Login
        </title>
        <meta name="description" content="Login">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no, user-scalable=no, minimal-ui">
        <meta name="apple-mobile-web-app-capable" content="yes" />
        <meta name="msapplication-tap-highlight" content="no">
        <link rel="stylesheet" media="screen, print" href="css/vendors.bundle.css">
        <link rel="stylesheet" media="screen, print" href="css/app.bundle.css">
        <link rel="apple-touch-icon" sizes="180x180" href="img/favicon/apple-touch-icon.png">
        <link rel="icon" type="image/png" sizes="32x32" href="img/favicon/favicon-32x32.png">
        <link rel="mask-icon" href="img/favicon/safari-pinned-tab.svg" color="#5bbad5">
        <link rel="stylesheet" media="screen, print" href="css/page-login.css">
        <link rel="stylesheet" media="screen, print" href="css/notifications/toastr/toastr.css">
    </head>
    <body>
        <div class="blankpage-form-field">
            <div class="page-logo m-0 w-100 align-items-center justify-content-center rounded border-bottom-left-radius-0 border-bottom-right-radius-0 px-4">
                <center>
                    <a href="#" class="page-logo-link press-scale-down d-flex align-items-center">
                        <img src="img/dcms-mini.png" alt="SmartAdmin WebApp" aria-roledescription="logo">
                    </a>
                </center>
            </div>
            <div class="card p-4 border-top-left-radius-0 border-top-right-radius-0">
                <form action="{{ url('login') }}" method="POST" class="">
                    @csrf
                    <div class="form-group">
                        <label class="form-label" for="username">Username / Email</label>
                        <input type="email" name="email" class="form-control" placeholder="your username or email" value="{{ old('email') }}{{ session('email') }}" required="required">
                        <span class="help-block">
                            Your unique username or email
                        </span>
                    </div>
                    <div class="form-group">
                        <label class="form-label" for="password">Password</label>
                        <input type="password" name="password" class="form-control" placeholder="password" value="" required="required">
                        <span class="help-block">
                            Your password
                        </span>
                    </div>
                    <div class="form-group text-left">
                        <div class="custom-control custom-checkbox">
                            <input type="checkbox" class="custom-control-input" id="rememberme">
                            <label class="custom-control-label" for="rememberme"> Remember me</label>
                        </div>
                    </div>
                    <button type="submit" class="btn btn-default btn-block">Login</button>
                </form>
            </div>
            <div class="blankpage-footer text-center">
                <a href="#"><strong>Recover Password</strong></a> | <a href="#"><strong>Register Account</strong></a>
            </div>
        </div>
        <video poster="img/backgrounds/clouds.png" id="bgvid" playsinline autoplay muted loop>
            <source src="media/video/cc.webm" type="video/webm">
            <source src="media/video/cc.mp4" type="video/mp4">
        </video>
        <script src="js/vendors.bundle.js"></script>
        <script src="js/app.bundle.js"></script>
        <script src="js/notifications/toastr/toastr.js"></script>
        @if(session('success'))
            <script type="text/javascript">
                toastr.options = {"positionClass": "toast-top-center","progressBar": true}
                toastr.success('{{session("success")}}')
            </script>
        @endif
        @if(session('info'))
            <script type="text/javascript">
                toastr.options = {"positionClass": "toast-top-center","progressBar": true}
                toastr.info('{{session("info")}}')
            </script>
        @endif
        @if(session('warning'))
            <script type="text/javascript">
                toastr.options = {"positionClass": "toast-top-center","progressBar": true}
                toastr.warning('{{session("warning")}}')
            </script>
        @endif
        @if(session('danger'))
            <script type="text/javascript">
                toastr.options = {"positionClass": "toast-top-center","progressBar": true}
                toastr.danger('{{session("danger")}}')
            </script>
        @endif
    </body>
</html>