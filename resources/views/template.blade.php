<!DOCTYPE html>
<html lang="en">
    @include('part.header')
    <body class="mod-bg-1 mod-hide-info-card">
        @include('part.navbar')
        @include($body)
        @include('part.footer')
    </body>
</html>
