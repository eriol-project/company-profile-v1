    <div class="az-content">
      <div class="container">
        <div class="az-content az-content-dashboard-four">
          <div class="media media-dashboard">
            <div class="media-body">
              <div class="az-content-header">
                <div>
                  <h6 class="az-content-title tx-18 mg-b-5">Brand</h6>
                  <p class="az-content-text tx-13 mg-b-0">Master Data Brand</p>
                </div>
              </div><!-- az-content-header -->

              <div class="card card-dashboard-twelve mg-b-20">
                <div class="card-header">
                  <div class="">
                    <div class="row row-sm">
                      <!-- NOTIFICATION -->
                      @if(session('success'))
                        <div class="col-sm-12">
                          <div class="alert alert-outline-success" role="alert" style="">
                              <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                              <span aria-hidden="true">×</span>
                              </button>
                              <strong>Success</strong> {!!session('success')!!}
                          </div> 
                        </div>
                      @endif
                      @if(session('info'))
                        <div class="col-sm-12">
                          <div class="alert alert-outline-info" role="alert" style="">
                              <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                              <span aria-hidden="true">×</span>
                              </button>
                              <strong>Info</strong> {!!session('info')!!}
                          </div> 
                        </div>
                      @endif
                      @if(session('danger'))
                        <div class="col-sm-12">
                          <div class="alert alert-outline-danger" role="alert" style="">
                              <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                              <span aria-hidden="true">×</span>
                              </button>
                              <strong>Info</strong> {!!session('danger')!!}
                          </div> 
                        </div>
                      @endif
                      <div class="col-sm-12 mg-t-20">
                        <button name="add"  data-toggle='modal' data-target='#modaladd' class="btn btn-success btn-block">New Brand</button>
                      </div>
                    </div><!-- row -->
                  </div>
                </div><!-- card-header -->
                
                <div class="card-body">
                  <div class="">
                    <table class="datatable display responsive nowrap">
                      <thead>
                        <tr>
                          <th style="width:85px !important;">No</th>
                          <th>Brand Name</th>
                          <th style="width:85px !important;">Action</th>
                        </tr>
                      </thead>
                      <tbody>
                      </tbody>
                    </table>
                  </div>
                </div><!-- card-body -->
              </div><!-- card -->

            </div><!-- media-body -->

          </div><!-- media -->

        </div><!-- az-content -->
      </div>
    </div><!-- az-content -->

    <!-- ADD MODAL -->
    <div id="modaladd" class="modal">
      <div class="modal-dialog" role="document">
        <div class="modal-content modal-content-demo">
          <div class="modal-header">
            <h6 class="modal-title">Add Form</h6>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <form class="row-fluid" method="POST" action="{{ url('brand') }}" data-parsley-validate>
          <div class="modal-body">
            @csrf
              <div class="form-group mg-b-0">
                <label class="form-label">Brand Name <span class="tx-danger">*</span></label>
                <input type="text" name="brand" class="form-control wd-450" placeholder="Enter Brand Name" required>
              </div><!-- form-group -->
            </div>
          <div class="modal-footer">
            <button type="submit" class="btn btn-success">Save changes</button>
            <button type="button" class="btn btn-outline-light" data-dismiss="modal">Close</button>
          </div>
          </form>
        </div>
      </div><!-- modal-dialog -->
    </div><!-- modal -->

    <!-- EDIT MODAL -->
    <div id="modaledit" class="modal">
      <div class="modal-dialog" role="document">
        <div class="modal-content modal-content-demo">
          <div class="modal-header">
            <h6 class="modal-title">Edit Form</h6>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <form id="edit_form" class="row-fluid" method="POST" data-parsley-validate>
          <div class="modal-body">
						@method('PUT')
						@csrf
              <div class="form-group mg-b-0">
                <label class="form-label">Brand Name <span class="tx-danger">*</span></label>
                <input type="hidden" name="id">
                <input type="text" name="brand" class="form-control wd-450" placeholder="Enter Brand Name" required>
              </div><!-- form-group -->
            </div>
          <div class="modal-footer">
            <button type="submit" class="btn btn-info">Save changes</button>
            <button type="button" class="btn btn-outline-light" data-dismiss="modal">Close</button>
          </div>
          </form>
        </div>
      </div><!-- modal-dialog -->
    </div><!-- modal -->

    <!-- DELETE MODAL -->
    <div id="modaldelete" class="modal">
      <div class="modal-dialog" role="document">
        <div class="modal-content modal-content-demo">
          <div class="modal-header">
            <h6 class="modal-title">Delete Confirmation</h6>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <form id="delete_form" class="row-fluid" method="POST">
          <div class="modal-body">
            @method('PUT')
            @csrf
            <h6></h6>
            <input type="hidden" name="deleteid">
            <input type="hidden" name="brand">
          </div>
          <div class="modal-footer">
            <button type="submit" class="btn btn-danger">Delete</button>
            <button type="button" class="btn btn-outline-light" data-dismiss="modal">Close</button>
          </div>
          </form>
        </div>
      </div><!-- modal-dialog -->
    </div><!-- modal -->