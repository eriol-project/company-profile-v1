<?php
Route::resource('/login', 'LoginController');
Route::get('/logout', 'LoginController@logout');
Route::get('/check', 'LoginController@check');
Route::get('process-upload', 'UploadController@upload_process');
Route::get('/testemail', 'HomeController@testEmail');
Route::get('test', function () {
    return view('test');
});
Route::get('/ddd', function (Codedge\Fpdf\Fpdf\Fpdf $fpdf) {

    $fpdf->AddPage();
    $fpdf->SetFont('Courier', 'B', 18);
    $fpdf->Cell(50, 25, 'Hello World!');
    $fpdf->Output();

});
Route::group(['middleware' => ['auth_session']], function ()
{
    Route::resource('/', 'HomeController'); 
    Route::get('/dashboard', 'HomeController@dashboard'); 
    Route::resource('/claim', 'ClaimController'); 
    Route::get('/claim-table', 'ClaimController@table'); 
    Route::get('/claim-detail-table', 'ClaimController@table_detail'); 
    Route::get('/claim-detail/{id}', 'ClaimController@detail'); 
    Route::get('/claim-export-excel/{distributor}/{status}/{area}', 'ClaimController@create'); 
    Route::get('/claim-export-pdf/{distributor}/{status}/{area}', 'ClaimController@create_pdf'); 
    Route::resource('/user', 'UserController'); 
    Route::resource('/distributor', 'DistributorController'); 
    Route::resource('/area', 'AreaController'); 
    Route::resource('/ppn', 'PpnController'); 
    Route::resource('/pph', 'PphController'); 
    Route::resource('/pending', 'PendingController'); 
    Route::resource('/status', 'StatusController'); 

    Route::post('/editpassword', 'LoginController@editpassword');
    Route::post('/sendemail', 'HomeController@sendEmail');
    Route::resource('/upload', 'UploadController'); 
    Route::resource('/brand', 'BrandController'); 
    Route::resource('/category', 'CategoryController'); 
    Route::resource('/customer', 'CustomerController'); 
    Route::resource('/material', 'MaterialController'); 
    Route::post('file-upload', 'UploadController@fileUploadPost');
    Route::get('/cancel-process', 'UploadController@process_cancel');
    Route::post('/check-upload-calculate', 'UploadController@check_process_calculate');
    Route::post('/check-upload', 'UploadController@check_process');
});
